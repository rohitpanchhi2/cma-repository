@isTest
public class CMA_ParametercreationTest {
@isTest
    public static void parametercreationInput(){
        test.startTest();
        
        List<CMA_Parametercreation.DataWrapper> finalList = new List<CMA_Parametercreation.DataWrapper>();
        CMA_Parametercreation.DataWrapper  obj = new CMA_Parametercreation.DataWrapper();
        CMA_DecisionTable__c decisionObj = new CMA_DecisionTable__c();
        decisionObj.CMA_SourceObject__c = 'Case';
        decisionObj.CMA_Primary_Object__c = 'CMA_Call_Rule__c';
        insert decisionObj;
        
        obj.recordId = decisionObj.Id;
        obj.isTestClass = false;
        obj.inputString = '([[{"indexVal":0,"fieldName":"Id","useAs":"INPUT","operator":"Does Not Exists In"}]])';
        finalList.add(obj);
          CMA_Parametercreation.processPR(finalList);
        test.stopTest();
    }
@isTest
    public static void parametercreationOutput(){
        test.startTest();
        List<CMA_Parametercreation.DataWrapper> finalList = new List<CMA_Parametercreation.DataWrapper>();
        CMA_Parametercreation.DataWrapper  obj = new CMA_Parametercreation.DataWrapper();
        CMA_DecisionTable__c decisionObj = new CMA_DecisionTable__c();
        decisionObj.CMA_SourceObject__c = 'Case';
        decisionObj.CMA_Primary_Object__c = 'CMA_Call_Rule__c';
        insert decisionObj;
        
        obj.recordId = decisionObj.Id;
        obj.inputString = '([[{"indexVal":0,"fieldName":"Id","useAs":"Output"}]])';
        finalList.add(obj);
          CMA_Parametercreation.processPR(finalList);
        test.stopTest();
    }
}