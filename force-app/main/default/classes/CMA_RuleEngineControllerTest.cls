/**
*Description : Test Class for CMA_InvokeRuleEngine, CMA_RuleEngineController           
*Author : KPMG
*Code Created Date : 27th September 2021
**/
@isTest
public class CMA_RuleEngineControllerTest {
    
    /*
    *@Description: Test data setup method
    *@Developer: santoshrebello@KPMG-KGS 
    */
    @testSetup 
    public static void setupData(){
        
        CMA_Call_Rule__c ruleObj = new CMA_Call_Rule__c();
        ruleObj.CMA_Event_Type__c = 'Care Management';
        ruleObj.CMA_Call_Attempt_Number__c = 1;
        ruleObj.CMA_Days_for_Next__c = 10;
        ruleObj.CMA_Active_Member__c = 'Yes';
        insert ruleObj;
        
        CMA_DecisionTable__c decTabObj = new CMA_DecisionTable__c();
        decTabObj.CMA_API_Name__c = 'Unsuccessful_Call__c';
        decTabObj.CMA_ConditionType__c = 'ALL';
        decTabObj.CMA_SourceObject__c = 'CMA_Call_Rule__c';        
        decTabObj.CMA_Primary_Object__c = 'Task';
        decTabObj.CMA_Status__c = 'Active';
        insert decTabObj;
        
        CMA_Decision_Table_Parameter__c decTabParamInp1 = CMA_DataProviderClass.createDecisionTblParameter('CMA_Event_Type__c','INPUT','Equals',1);
        decTabParamInp1.CMA_DecisionTableId__c = decTabObj.Id;
        
        CMA_Decision_Table_Parameter__c decTabParamInp2 = CMA_DataProviderClass.createDecisionTblParameter('CMA_Call_Attempt_Number__c','INPUT','Equals',2);
        decTabParamInp2.CMA_DecisionTableId__c = decTabObj.Id;
        
        CMA_Decision_Table_Parameter__c decTabParamInp3 = CMA_DataProviderClass.createDecisionTblParameter('CMA_Active_Member__c','INPUT','Equals',3);
        decTabParamInp3.CMA_DecisionTableId__c = decTabObj.Id;
        
        CMA_Decision_Table_Parameter__c decTabParamInp4 = CMA_DataProviderClass.createDecisionTblParameter('CreatedDate','INPUT','Equals',4);
        decTabParamInp4.CMA_DecisionTableId__c = decTabObj.Id;
        
        CMA_Decision_Table_Parameter__c decTabParamOup1 = CMA_DataProviderClass.createDecisionTblParameter('CMA_Days_for_Next__c','OUTPUT','Equals',null);
        decTabParamOup1.CMA_DecisionTableId__c = decTabObj.Id;
        
        CMA_Decision_Table_Parameter__c decTabParamOup2 = CMA_DataProviderClass.createDecisionTblParameter('Name','OUTPUT','Equals',null);
        decTabParamOup2.CMA_DecisionTableId__c = decTabObj.Id;
        
        CMA_Decision_Table_Parameter__c decTabParamOup3 = CMA_DataProviderClass.createDecisionTblParameter('CreatedDate','OUTPUT','Equals',null);
        decTabParamOup3.CMA_DecisionTableId__c = decTabObj.Id;
        
        insert new List<CMA_Decision_Table_Parameter__c>{decTabParamInp1, decTabParamInp2, decTabParamInp3, decTabParamInp4, decTabParamOup1, decTabParamOup2, decTabParamOup3};
            
        CMA_DecisionTableDataSetLink__c dataSetLink = CMA_DataProviderClass.createDecisionTblDataSetLink();
        dataSetLink.CMA_DecisionTableId__c = decTabObj.Id;
        insert dataSetLink;
        
        CMA_DecisionTblDatasetParameter__c dtSetLinkParam1 = new CMA_DecisionTblDatasetParameter__c();
        dtSetLinkParam1.CMA_DatasetFieldName__c = 'CMA_Event_Type__c';
        dtSetLinkParam1.CMA_DatasetSourceObject__c = 'Task';
        dtSetLinkParam1.CMA_DecisionTableDatasetLinkId__c = dataSetLink.Id;
        dtSetLinkParam1.DecisionTableParameterId__c =  decTabParamInp1.Id;
        
        CMA_DecisionTblDatasetParameter__c dtSetLinkParam2 = new CMA_DecisionTblDatasetParameter__c();
        dtSetLinkParam2.CMA_DatasetFieldName__c = 'CMA_Call_Attempt_Number__c';
        dtSetLinkParam2.CMA_DatasetSourceObject__c = 'Case';
        dtSetLinkParam2.CMA_DecisionTableDatasetLinkId__c = dataSetLink.Id;
        dtSetLinkParam2.DecisionTableParameterId__c =  decTabParamInp2.Id;
        
        CMA_DecisionTblDatasetParameter__c dtSetLinkParam3 = new CMA_DecisionTblDatasetParameter__c();
        dtSetLinkParam3.CMA_DatasetFieldName__c = 'Active__c';
        dtSetLinkParam3.CMA_DatasetSourceObject__c = 'Account';
        dtSetLinkParam3.CMA_DecisionTableDatasetLinkId__c = dataSetLink.Id;
        dtSetLinkParam3.DecisionTableParameterId__c =  decTabParamInp3.Id;
        
        CMA_DecisionTblDatasetParameter__c dtSetLinkParam4 = new CMA_DecisionTblDatasetParameter__c();
        dtSetLinkParam4.CMA_DatasetFieldName__c = 'CreatedDate';
        dtSetLinkParam4.CMA_DatasetSourceObject__c = 'Task';
        dtSetLinkParam4.CMA_DecisionTableDatasetLinkId__c = dataSetLink.Id;
        dtSetLinkParam4.DecisionTableParameterId__c =  decTabParamInp4.Id;
        
        insert new List<CMA_DecisionTblDatasetParameter__c>{dtSetLinkParam1, dtSetLinkParam2, dtSetLinkParam3, dtSetLinkParam4};
            
        Account accObj = new Account(Name ='Test Person Account', Active__c ='Yes');
        insert accObj;
        
        Case caseData =  new Case();
        caseData.AccountId = accObj.id;
        caseData.Subject = 'Test Care Plan';
        caseData.CMA_Call_Attempt_Number__c = 1;
        caseData.Status = 'Active';
        insert caseData;
        
        Task taskObj = new Task();
        taskObj.Description = 'Call Notes';
        taskObj.Status = 'Not Started';
        taskObj.CMA_Call_Status__c = 'New';
        taskObj.CMA_Event_Type__c = 'Care Management';
        taskObj.WhatId = caseData.Id;
        taskObj.Subject = 'Call';
        
        Task taskObj2 = new Task();
        taskObj2.Description = 'Call Notes';
        taskObj2.Status = 'Not Started';
        taskObj2.CMA_Call_Status__c = 'New';
        taskObj2.CMA_Event_Type__c = 'Post Discharge';
        taskObj2.WhatId = caseData.Id;
        taskObj2.Subject = 'Call';
        
        insert new List<Task>{taskObj, taskObj2};
    }
    
    /*
    * @Description: test method for RuleEngine method for Condition Criteria as ALL 
    * @Developer: santoshrebello@KPMG-KGS 
    */
    @isTest
    public static void ruleEngineTestforALL(){       
        List<Task> taskList = [SELECT Id, Description, Subject, CMA_Event_Type__c, WhatId, CreatedDate FROM Task WHERE Subject ='Call'];        
        Test.startTest();
        List<RuleEngineOutputWrap> outputWrap = new List<RuleEngineOutputWrap> ();
        outputWrap= CMA_RuleEngineController.invokeDTwDataSet(taskList,'Unsuccessful_Call__c');
        Test.stopTest();
    }
    
    /*
    * @Description: test method for RuleEngine method for Condition Criteria as ANY
    * @Developer: santoshrebello@KPMG-KGS 
    */
    @isTest
    public static void ruleEngineTestforANY(){
        CMA_DecisionTable__c decTabObj = [SELECT Id,CMA_ConditionCriteria__c FROM CMA_DecisionTable__c WHERE CMA_API_Name__c='Unsuccessful_Call__c'];
        decTabObj.CMA_ConditionType__c = 'ANY';
        update decTabObj;        
        List<Task> taskList = [SELECT Id, Description, Subject, CMA_Event_Type__c, WhatId, CreatedDate FROM Task WHERE Subject ='Call'];
        
        Test.startTest();
        CMA_RuleEngineController.invokeDTwDataSet(taskList,'Unsuccessful_Call__c');
        Test.stopTest();
    }
    
    /*
    * @Description: test method for RuleEngine method for Condition Criteria as CUSTOM
    * @Developer: santoshrebello@KPMG-KGS 
    */
    @isTest
    public static void ruleEngineTestforCustom(){
        CMA_DecisionTable__c decTabObj = [SELECT Id,CMA_ConditionCriteria__c FROM CMA_DecisionTable__c WHERE CMA_API_Name__c='Unsuccessful_Call__c'];
        decTabObj.CMA_ConditionType__c = 'CUSTOM';
        decTabObj.CMA_ConditionCriteria__c = '(1 OR 2 AND (3 OR 4) OR (3 AND 4))';
        update decTabObj;        
        List<Task> taskList = [SELECT Id, Description, Subject, CMA_Event_Type__c, WhatId, CreatedDate FROM Task WHERE Subject ='Call'];
        
        Test.startTest();
        CMA_RuleEngineController.invokeDTwDataSet(taskList,'Unsuccessful_Call__c');
        Test.stopTest();
    }
    
    /*
    * @Description: test method for RuleEngine method invoking from Flow
    * @Developer: santoshrebello@KPMG-KGS 
    */
    @isTest
    public static void ruleEngineTestfromFlow(){        
        List<Task> taskList = [SELECT Id, Status, Description, Subject, CMA_Event_Type__c, WhatId, CreatedDate FROM Task WHERE Subject ='Call' LIMIT 1];
        taskList[0].Status = 'In Progress';
        Test.startTest();
        update taskList;
        Test.stopTest();
    }
}