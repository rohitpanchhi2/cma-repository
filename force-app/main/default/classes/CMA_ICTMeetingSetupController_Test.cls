@isTest
public class CMA_ICTMeetingSetupController_Test {
    
    @isTest
    public static void getMeetingDetailsTest(){
        test.startTest();
        Case c = CMA_DataProviderClass.getCase();
        insert c;
        Contact con = CMA_DataProviderClass.getContact();
        insert con;
        Account acc = CMA_DataProviderClass.getAccount();
        insert acc;
        CMA_ICTMeetingSetupController.getMeetingDetails(c.Id);
        CMA_ICTMeetingSetupController.getCaseTeamRoles();
        CMA_ICTMeetingSetupController.getCaseTeamMembers(c.Id);
        CMA_ICTMeetingSetupController.getRecordName(acc.Id);
        Campaign campaign = new Campaign();
        campaign.CMA_Care_Plan__c = c.Id;
        campaign.CMA_Account__c = acc.Id;
        campaign.Name = 'Test';
        insert campaign;
        CMA_Care_Plan_Campaign_Member__c camp = new CMA_Care_Plan_Campaign_Member__c();
        camp.CMA_Contact__c = con.Id;
		camp.CMA_Reason__c = 'Invited';
        camp.CMA_Role__c = 'Nurse';
        camp.CMA_Type__c = 'Email';
        camp.CMA_Campaign__c = campaign.Id;
        List<CMA_Care_Plan_Campaign_Member__c> campList = new List<CMA_Care_Plan_Campaign_Member__c>();
        campList.add(camp);
        system.debug('campList -> '+campList);
        CMA_ICTMeetingSetupController.insertCampaignMembersCP(campList);
        test.stopTest();
    }

}