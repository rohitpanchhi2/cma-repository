/******************************************************************************
* Author: Dhanik Lal Sahni
* Date: Feb 14, 2021
* Descpription: Knowledge Search

*/

public with sharing class CMA_knowledgeSearchController {
    
    //Get knowledge articles which contains input text
    @AuraEnabled(cacheable=true)
    public static List<Knowledge__kav> getKnowledgeArticles(String searchText) {
        system.debug('searchText -> '+searchText);
        List<Knowledge__kav> knowledgeArticlesList = new List<Knowledge__kav>();
        if (searchText != '' && searchText != null) {
            for(Knowledge__kav kav : [SELECT Id, Title, Summary,Information__c,CreatedDate,ArticleCaseAttachCount,ArticleTotalViewCount,KnowledgeArticleId FROM Knowledge__kav where PublishStatus='Online']) {
                if(kav.Title.contains(searchText) || kav.Information__c.contains(searchText))
                    knowledgeArticlesList.add(kav);
            }
            return knowledgeArticlesList;
        }
        return null;
    }
    
    
    //Assign articles to case
    /*@AuraEnabled
    public static boolean assignArticleToCase(List<CaseArticle> articles) {
        if(articles!=null)
        {
            upsert articles;
        }
        return true;
    }*/
    
	@AuraEnabled(cacheable=true)
    public static List<Knowledge__kav> getTitle(String articleId){
        List<Knowledge__kav> knowledgeList = [SELECT Id, Title, Summary,Information__c,CreatedDate,ArticleCaseAttachCount,
                                    ArticleTotalViewCount,KnowledgeArticleId FROM Knowledge__kav WHERE Id =: articleId];
        if(knowledgeList != null){
            return knowledgeList;
        }
        else{
            return null;
        }
    }    
}