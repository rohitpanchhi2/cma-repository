/**
Description : Class for CMA_AddDataSetLinkTable           
*Author : KPMG
*Code Created Date : 27th September 2021
**/
public class CMA_FetchObjectName {
    
    /*
* @Description: method to get all objects
* @param name : NA
* @return type: List of String
* @Developer: Aishwarya 
*/
    @AuraEnabled
    public static list<string> getAllObjects(){
        List<String> objectList = new List<String>();
        for(Schema.SObjectType objType : Schema.getGlobalDescribe().Values())
        {
            Schema.DescribeSObjectResult result = objType.getDescribe();
            string objName = objType.getDescribe().getName();
            objectList.add(objName);
        }
        system.debug('object are : '+objectList);
        return objectList;
    }
    
    @AuraEnabled
    public static list<String> getFieldsOfDecisionTblPara(String recordId){
        List<CMA_Decision_Table_Parameter__c> fieldList = [select CMA_FieldName__c from CMA_Decision_Table_Parameter__c where CMA_Usage__c = 'INPUT' and CMA_DecisionTableId__c =:recordId];
        system.debug('fieldList are : '+fieldList);
        List<String> nameList = new List<String>();
        for(Integer i=0; i<=fieldList.size() - 1; i++){
            nameList.add(fieldList[i].CMA_FieldName__c);
        }
        return nameList; 
    }
    
    @auraEnabled 
    public static Map<String, List<String>> getObjectFields(string selectedObj) {
        Map<String, List<String>> fieldNames = new Map<String, List<String>>();
        List<String> namesOfField = new List<String>();
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Schema.SObjectType sobjType = gd.get(selectedObj); 
        Schema.DescribeSObjectResult describeResult = sobjType.getDescribe(); 
        Map<String,Schema.SObjectField> fieldsMap = describeResult.fields.getMap();
        System.debug('fieldsMap '+fieldsMap.keySet());
        for(Schema.SObjectField field : fieldsMap.values()){
            //fieldNames.put(selectedObj, new List<String>{String.valueOf(field)});
            namesOfField.add(String.valueOf(field));
        }
        if(!namesOfField.isEmpty()){
            fieldNames.put(selectedObj, namesOfField);
        }
        system.debug('fieldNames   '+fieldNames);
        return fieldNames;
    }  
    
    public class DataWrapper {
        @InvocableVariable(label='Record Id' required=false)
        public List<Id> decisionTblDatasetParaId;
        @InvocableVariable(label='JSON String' required=false)
        public String inputString;      
    }
    
    @InvocableMethod
    public static void updatedecisionTblDatasetPara(List<DataWrapper>  passedData){
        
        try{
            Set<Id> decisionTblDatasetParaIdSet = new Set<Id>();
            
            for(DataWrapper wrapper : passedData){
                decisionTblDatasetParaIdSet.addAll(wrapper.decisionTblDatasetParaId);
            }
            system.debug('decisionTblDatasetParaIdSet is : '+decisionTblDatasetParaIdSet);
            
            List<CMA_DecisionTblDatasetParameter__c> decisonList = [select id,Name,CMA_DatasetFieldName__c,CMA_DatasetSourceObject__c from CMA_DecisionTblDatasetParameter__c where id in : decisionTblDatasetParaIdSet];
            system.debug('decisonList is : '+decisonList);
            List<CMA_DecisionTblDatasetParameter__c> decisonListUpdated = new List<CMA_DecisionTblDatasetParameter__c>();
            
            for (DataWrapper dw: passedData) {
                String strList  = String.valueOf(dw.inputString);
                strList =  strList.replace('\\','').replace('[{', '').replace('}]', ';').replace('"', '').replace('},{', ';').replace('(', '').replace(')', '').replace('fieldname','').replace('obj','').replace('fields','').replace(':','').replace('"]','').replace('["',''); 
                strList = strList.removeStart('[');
                strList = strList.removeEnd(']');
                system.debug('strList--'+strList);
                //strList = strList.replace(';,', ';');
                //system.debug('strList--'+strList);
                
                List<String> stringList = strList.split(';');
                system.debug('stringList--'+stringList.size());
                system.debug('stringList--'+stringList);
                
                for(CMA_DecisionTblDatasetParameter__c record : decisonList){
                    for(String fstring : stringList){
                        List<String> finalString = fstring.split(',');
                        system.debug('finalString--'+finalString.size());
                        system.debug('finalString--'+finalString);
                        if(finalString.size() > 2){
                            if(record.Name == finalString[0]){
                                record.CMA_DatasetSourceObject__c = finalString[1];
                                record.CMA_DatasetFieldName__c  = finalString[2];
                                decisonListUpdated.add(record);
                            }
                        }
                    }
                    
                }
                
            } 
            system.debug('decisonList updated is : '+decisonListUpdated);
            update decisonListUpdated;
        }
        
        catch(Exception e){
            SystemLogUtils.insertSystemLogs(e);
        }
        
    }
    
    
}