@isTest
public class CMA_CreateNewDemoUser_Test {
    
    @isTest
    public static void recordTypeOfAccountTest(){
        test.startTest();
        CMA_CreateNewDemoUser.recordTypeOfAccount();
        List<String> checkboxList = new List<String>();
        checkboxList.add('Address');
        checkboxList.add('Phone');
        checkboxList.add('Allergy');
        checkboxList.add('Diagnosis');
        checkboxList.add('Immunization');
        CMA_CreateNewDemoUser.updateInsertedRecord('Bob','BB','Payer',checkboxList);
        CMA_CreateNewDemoUser.updateInsertedRecord('Bob','BBI','Provider',checkboxList);
        test.stopTest();
    }

}