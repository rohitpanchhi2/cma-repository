/**
*Description : Test Class for CMA_GenericDataViewController           
*Author : KPMG
*Last Modified By/Date : santoshrebello@kpmg-kgs/14-Dec,2021;
**/
@isTest
public class CMA_GenericDataViewControllerTest {
    @testSetup 
    public static void setupData(){
        Account accObj = new Account(Name ='Test Person Account', Active__c ='Yes');
        insert accObj;
        
        Case caseData =  new Case();
        caseData.AccountId = accObj.id;
        caseData.Subject = 'Test Care Plan';
        caseData.CMA_Call_Attempt_Number__c = 1;
        caseData.Status = 'Active';
        insert caseData;
        
        CMA_Alert__c alertData = new CMA_Alert__c();
        alertData.CMA_Alert__c = 'Member Hospitalized';
        alertData.CMA_StartDate__c = System.today().addDays(-2);
        alertData.CMA_End_Date__c =System.today().addDays(4);
        alertData.CMA_Priority__c = 2;
        alertData.CMA_Account__c = accObj.Id;
        alertData.CMA_Case__c = caseData.Id;
        
        CMA_Alert__c alertData2 = new CMA_Alert__c();
        alertData2.CMA_Alert__c = 'Member Discharged';
        alertData2.CMA_StartDate__c = System.today().addDays(-3);
        alertData2.CMA_End_Date__c =System.today().addDays(5);
        alertData2.CMA_Priority__c = 1;
        alertData2.CMA_Account__c = accObj.Id;
        alertData2.CMA_Case__c = caseData.Id;
        
        insert new List<CMA_Alert__c>{alertData,alertData2};
    }
    
    @isTest
    public static void getFieldsAndRecordsTest(){
        Test.startTest();
        CMA_GenericDataViewController.getFieldsAndRecords('','CMA_Alert__c','Alert_Display','');
        Test.stopTest();
    }
    
    @isTest
    public static void updateObjectDataTest(){
        Test.startTest();
        List<CMA_Alert__c> alertDataList = [Select Id, Name, CMA_StartDate__c,CMA_Priority__c FROM CMA_Alert__c];
        alertDataList[0].CMA_Priority__c = 4;
        CMA_GenericDataViewController.updateObjectData(alertDataList,'CMA_Alert__c');
        Test.stopTest();
    }
}