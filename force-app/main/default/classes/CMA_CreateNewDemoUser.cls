/*
Description : Class for creating demo user person account           
*Author : KPMG
*Code Created Date : 10th December 2021
*/
public class CMA_CreateNewDemoUser {
    /*
* @Description: method for fetching Payer and Provider record types of Account 
* @param name : NA
* @return type: List of recordtype
* @Developer: Aishwarya 
*/
    @AuraEnabled
    public static List<String> recordTypeOfAccount(){
        List<String> recordTypeList = new List<String>();
        String recordTypeDevName = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payer').getName();
        recordTypeList.add(recordTypeDevName);
        String recordTypeDevName1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Provider').getName();
        recordTypeList.add(recordTypeDevName1);
        if(recordTypeList != null){
            return recordTypeList;
        }
        else{
            return null;
        }
    }
    
    
    /*
* @Description: method for inserting person account 
* @param name : FirstName,LastName,RecordType, List of string containing objects to create there records.
* @return type: Account
* @Developer: Aishwarya 
*/
    @AuraEnabled
    public static Account updateInsertedRecord(String accFName, String accSName,String recordtypeId,List<String> selectedValue){
        system.debug('accName = '+accFName);
        system.debug('accName = '+accSName);
        system.debug('selectedValue -> '+selectedValue);
        
        Id recordTypeIds;
        if(recordtypeId == 'Payer'){
            recordTypeIds = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
        }
        else if(recordtypeId == 'Provider'){
            recordTypeIds = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        }
        
        List<CMA_Account_Dummy_Data__mdt> recordsMDT = [SELECT Alternate_City__c,Alternate_Personal_Phone__c,
                                                        Alternate_State__c,Alternate_Street__c,Alternate_ZIP_Code__c,
                                                        Member_Id__c,Primary_City__c,Primary_Home_Phone__c,Immunization__c,
                                                        Primary_State__c,Primary_Street__c,Primary_ZIP_Code__c,SSN__c,
                                                        Allergy__c,Allergy1__c,Diagnosis__c,Diagnosis1__c,birthdate__c,
                                                        email__c,Gender__c,prefered_address__c,prefered_contact_channel__c,
                                                        prefered_language__c,prefered_phone__c,preferred_contact_time__c
                                                        FROM CMA_Account_Dummy_Data__mdt];
        system.debug('recordsMDT -> '+recordsMDT);
        
        Account account = new Account();
        account.Active__c = 'Yes';
        account.FirstName = accFName;
        account.LastName = accSName;
        account.RecordTypeId = recordTypeIds;
        Long SSN = (Math.random()*100000).round();
        account.vlocity_ins__SocialSecurityNumber__pc = String.valueof(SSN);
        if(recordsMDT != null){
            account.PersonBirthdate = recordsMDT[0].birthdate__c;
            account.PersonEmail = recordsMDT[0].email__c;
            account.HealthCloudGA__Gender__pc = recordsMDT[0].Gender__c;
            account.vlocity_ins__Gender__pc = recordsMDT[0].Gender__c;
            account.CMA_PreferredAddress__c = recordsMDT[0].prefered_address__c;
            account.CMA_Preferred_Contact_Time__c = recordsMDT[0].preferred_contact_time__c;
            account.CMA_Preferred_Contact_Channel__c = recordsMDT[0].prefered_contact_channel__c;
            account.CMA_PreferredLanguage__c = recordsMDT[0].prefered_language__c;
            account.CMA_PreferredPhone__c = recordsMDT[0].prefered_phone__c;
            account.CMA_MemberId__c = recordsMDT[0].Member_Id__c + (Math.random()*100).round();
        }
        system.debug('Account -> '+account);
        try{
            insert account;
        }
        catch(Exception e){ 
            SystemLogUtils.insertSystemLogs(e); 
        }
        
        
        if(recordsMDT != null){
            if(selectedValue != null && selectedValue.contains('Address')){
                
                Schema.Location loc = new Schema.Location();
                loc.Name = 'Members Location';
                loc.LocationType = 'Primary Location';
                loc.Account__c= account.id;
                insert loc;
                
                Schema.Address accPriAddress = new Schema.Address();
                Schema.Address accAltAddress = new Schema.Address();
                List<Schema.Address> addressList = new List<Schema.Address>();
                accPriAddress.Account__c = account.id;
                accPriAddress.CMA_Primary_Address__c = true;
                accPriAddress.street = recordsMDT[0].Primary_Street__c;
                accPriAddress.state = recordsMDT[0].Primary_State__c;
                accPriAddress.city = recordsMDT[0].Primary_City__c;
                accPriAddress.PostalCode = recordsMDT[0].Primary_ZIP_Code__c;
                accPriAddress.LocationType = 'Primary Address';
                accPriAddress.ParentId = loc.id;
                addressList.add(accPriAddress);
                
                accAltAddress.Account__c = account.id;
                accAltAddress.CMA_Alternative_Address__c = true;
                accAltAddress.street = recordsMDT[0].Alternate_Street__c;
                accAltAddress.state = recordsMDT[0].Alternate_State__c;
                accAltAddress.city = recordsMDT[0].Alternate_City__c;
                accAltAddress.PostalCode = recordsMDT[0].Alternate_ZIP_Code__c;
                accAltAddress.LocationType = 'Alternative Address';
                accAltAddress.ParentId = loc.id;
                addressList.add(accAltAddress);
                
                if(addressList != null){
                    insert addressList;
                }
                else{
                    return null;
                }
                
            }
            
            if(selectedValue != null && selectedValue.contains('Phone')){
                CMA_Phone__c priPhone = new CMA_Phone__c();
                CMA_Phone__c altPhone = new CMA_Phone__c();
                List<CMA_Phone__c> phoneList = new List<CMA_Phone__c>();
                priPhone.CMA_Account__c = account.id;
                priPhone.CMA_Phone_Number__c = recordsMDT[0].Primary_Home_Phone__c;
                priPhone.CMA_Phone_Type__c = 'Home';
                priPhone.CMA_Phone_Preference__c = 'Primary';
                phoneList.add(priPhone);
                
                altPhone.CMA_Account__c = account.id;
                altPhone.CMA_Phone_Number__c = recordsMDT[0].Alternate_Personal_Phone__c;
                altPhone.CMA_Phone_Type__c = 'Personal';
                altPhone.CMA_Phone_Preference__c = 'Alternate';
                phoneList.add(altPhone);
                
                if(phoneList != null){
                    insert phoneList;
                }
                else{
                    return null;
                }
            }
            if(selectedValue != null && selectedValue.contains('Diagnosis')){
                List<CMA_Diagnosis__c> diagnosisList = new List<CMA_Diagnosis__c>();
                CMA_Diagnosis__c diagnosis = new CMA_Diagnosis__c();
                diagnosis.CMAAccount__c = account.Id;
                diagnosis.Name = recordsMDT[0].Diagnosis__c;
                diagnosisList.add(diagnosis);
                
                CMA_Diagnosis__c diagnosis1 = new CMA_Diagnosis__c();
                diagnosis1.CMAAccount__c = account.Id;
                diagnosis1.Name = recordsMDT[0].Diagnosis1__c;
                diagnosisList.add(diagnosis1);
                
                if(diagnosisList != null){
                    insert diagnosisList;
                }
                else{
                    return null;
                }
            }
            
            if(selectedValue != null && selectedValue.contains('Allergy')){
                List<HealthCloudGA__EhrAllergyIntolerance__c> allergyList = new List<HealthCloudGA__EhrAllergyIntolerance__c>();
                HealthCloudGA__EhrAllergyIntolerance__c allergy = new HealthCloudGA__EhrAllergyIntolerance__c();
                allergy.HealthCloudGA__Account__c = account.Id;
                allergy.HealthCloudGA__SubstanceLong__c = recordsMDT[0].Allergy__c;
                allergy.HealthCloudGA__Substance255__c = recordsMDT[0].Allergy__c;
                allergyList.add(allergy);
                
                HealthCloudGA__EhrAllergyIntolerance__c allergy1 = new HealthCloudGA__EhrAllergyIntolerance__c();
                allergy1.HealthCloudGA__Account__c = account.Id;
                allergy1.HealthCloudGA__SubstanceLong__c = recordsMDT[0].Allergy1__c;
                allergy1.HealthCloudGA__Substance255__c = recordsMDT[0].Allergy1__c;
                allergyList.add(allergy1);
                
                if(allergyList != null){
                    insert allergyList;
                }
                else{
                    return null;
                }
            }
            
            if(selectedValue != null && selectedValue.contains('Immunization')){
                List<HealthCloudGA__EhrImmunization__c> immunizationList = new List<HealthCloudGA__EhrImmunization__c>();
                HealthCloudGA__EhrImmunization__c immunization = new HealthCloudGA__EhrImmunization__c();
                immunization.HealthCloudGA__Account__c = account.Id;
                immunization.HealthCloudGA__VaccineTypeLabel__c = recordsMDT[0].Immunization__c;
                immunizationList.add(immunization);
                
                if(immunizationList != null){
                    insert immunizationList;
                }
                else{
                    return null;
                }
            }
        }
        return account; 
    }
}