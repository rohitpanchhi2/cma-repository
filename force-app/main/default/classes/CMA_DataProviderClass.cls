/**
Description : Utility Class           
*Author : KPMG
*Code Created Date : 27th September 2021
**/

public class CMA_DataProviderClass {
    
    /*
        * @Description: utility method for task
        * @param name : NA
        * @return type: List of sobjects
        * @Developer: Aishwarya 
    */
    public static list<sObject> getListOfSobjects(){
        
        List<sObject> recordList = new List<sObject>();
        List<Account> accountList = new List<Account>();
        List<Case> caseList = new List<Case>();
        List<Task> taskList = new List<Task>();
        
        try{
            for(Integer i=0; i<=5; i++){
                Account a = new Account();
                a.Name = 'Sam'+i;
                accountList.add(a);
            }
            
            insert accountList;
            
            for(Account acc : accountList){
                Case c =  new Case();
                c.AccountId = acc.id;
                caseList.add(c);
            }
            
            insert caseList;
            
            for(Case c : caseList){
                Task t = new Task();
                t.Status = 'Not Started';
                t.CMA_Call_Status__c = 'New';
                t.CMA_Event_Type__c = 'Care Management';
                t.WhatId = c.id;
                taskList.add(t);
            }
            
            insert taskList;
        }
        catch(Exception e){
            SystemLogUtils.insertSystemLogs(e);
        }
        return taskList;
    }
    
    
    /*
        * @Description: utility method for getting verification records from Account
        * @param name : NA
        * @return type: CMA_Verification__c record
        * @Developer: Aishwarya 
    */
    public static CMA_Verification__c getVerificationRecords(){
        Account a = new Account();
        a.Name = 'Sam';
        CMA_Verification__c verificationRecord = new CMA_Verification__c();
        try{
            insert a; 
            verificationRecord.CMA_Member_Id__c = a.id;
            //verificationRecord.CMA_Authorized_Party__r.Name = 'Musa';
            verificationRecord.CMA_Communication_Type__c = 'Inbound';
            verificationRecord.CMA_Primary_Number__c = '12345678';
            
            insert verificationRecord;
        }
        catch(Exception e){
            SystemLogUtils.insertSystemLogs(e);
        }
        return verificationRecord;
    }
    
    
    /*
        * @Description: utility method for creating CMA_DecisionTable__c
        * @param name : NA
        * @return type: CMA_DecisionTable__c record
        * @Developer: Aishwarya 
    */
    public static CMA_DecisionTable__c createDecisionTable(){
        CMA_DecisionTable__c decisionObj = new CMA_DecisionTable__c();
        decisionObj.CMA_SourceObject__c = '';
        decisionObj.CMA_Primary_Object__c = 'Task';
        return decisionObj;
    }
    
    
    /*
        * @Description: utility method for creating CMA_Decision_Table_Parameter__c
        * @param name : NA
        * @return type: CMA_Decision_Table_Parameter__c record
        * @Developer: Aishwarya 
    */
    public static CMA_Decision_Table_Parameter__c createDecisionTblParameter(String fieldName, String Type, String Operator, Decimal Sequence){
        CMA_Decision_Table_Parameter__c decTabParam = new CMA_Decision_Table_Parameter__c();
        decTabParam.CMA_FieldName__c = fieldName;
        decTabParam.CMA_Usage__c = Type;
        decTabParam.CMA_Operator__c = Operator;
        decTabParam.CMA_Sequence__c = Sequence;
        return decTabParam;
    }
    
    
    /*
        * @Description: utility method for creating CMA_DecisionTableDataSetLink__c
        * @param name : NA
        * @return type: CMA_DecisionTableDataSetLink__c record
        * @Developer: Aishwarya 
    */
    public static CMA_DecisionTableDataSetLink__c createDecisionTblDataSetLink(){
        CMA_DecisionTableDataSetLink__c dataset = new CMA_DecisionTableDataSetLink__c();
        return dataset;
    }
    
    
    /*
        * @Description: utility method for creating CMA_DecisionTblDatasetParameter__c
        * @param name : NA
        * @return type: CMA_DecisionTblDatasetParameter__c record
        * @Developer: Aishwarya 
    */
    public static CMA_DecisionTblDatasetParameter__c createDecisionTblDataSetPara(){
        CMA_DecisionTblDatasetParameter__c decisionTblDatasetpara = new CMA_DecisionTblDatasetParameter__c();
        decisionTblDatasetpara.CMA_DatasetSourceObject__c = 'Case';
        return decisionTblDatasetpara;
    }
    
    /*
        * @Description: utility method for creating Account
        * @param name : NA
        * @return type: Account
        * @Developer: Aishwarya 
    */
    public static Account getAccount(){
        Account account = new Account();
        account.Name = 'Arena';
        return account;
    }
    
     /*
        * @Description: utility method for creating Case
        * @param name : NA
        * @return type: Case
        * @Developer: Aishwarya 
    */
    public static Case getCase(){
        Case c = new Case();
        c.status = 'Active';
        return c;
    }
    
    /*
        * @Description: utility method for creating Contact
        * @param name : NA
        * @return type: Contact
        * @Developer: Aishwarya 
    */
    public static Contact getContact(){
        Contact contact = new Contact();
        contact.LastName = 'Frank';
        return contact;
    }
    
    /*
        * @Description: utility method for creating Knowledge__kav
        * @param name : NA
        * @return type: Knowledge__kav
        * @Developer: Aishwarya 
    */
    public static Knowledge__kav getKnowledge(){
        Knowledge__kav knowledge = new Knowledge__kav();
        knowledge.Title='Fetal Heart';
        knowledge.UrlName = 'Fetal-Heart';
        knowledge.Information__c = 'WHAT IS FETAL HEART RATE MONITORING? Fetal heart rate monitors are often used for measuring the rhythm and heart rate of your baby. Therefore, allowing your doctor or OB/GYN to see how your baby is doing.WHY IS FETAL HEART RATE MONITORING DONE?';
        knowledge.Summary = 'Salesforce Cloud CRM';
		knowledge.Language = 'en_US';
        insert knowledge;
        return knowledge;
    }

    
}