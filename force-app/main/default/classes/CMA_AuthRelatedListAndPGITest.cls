@isTest
public class CMA_AuthRelatedListAndPGITest {
    @isTest
    public static void getAllTest(){
       
        Account a = CMA_DataProviderClass.getAccount();
        insert a;
        
        Case c1 = new Case(AccountId = a.id,Subject = 'Test Case1');
        insert c1;
        
        Contact contact = CMA_DataProviderClass.getContact();
        contact.AccountId = a.id;
        insert contact;
        
        Task t1 = new Task(WhoId = contact.id,Subject = 'Call Notes1',Status = 'Not Started',Type ='Intervention');
        insert t1;

        test.startTest();

        CMA_AccAccConConRelatedListController.getAccountRecord(a.Id,4);
        CMA_AccAccConConRelatedListController.getContactRecord(a.Id,4);
        CMA_RelatedInterventionTask.getAssessmentDetails(a.Id);
        CMA_RelatedInterventionTask.getTaskBarrierDetails();
        CMA_RelatedInterventionTask.getCaseDetails(a.Id);
        CMA_RelatedInterventionTask.getPGIDetails(c1.Id);
        CMA_RelatedInterventionTask.getPGIDetailsOnInitialLoad(a.id);
       
        test.stopTest();
    }
    
}