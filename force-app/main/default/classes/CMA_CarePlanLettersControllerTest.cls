@isTest
public class CMA_CarePlanLettersControllerTest {
    /*
        * @Description: test method for getEmailIdOfAccount,getLetterTemplate,getAttachmentRecord
        * @param name : NA
        * @return type: NA
        * @Developer: Aishwarya 
    */
    /*
        * @Description: test method for getEmailIdOfAccount,getLetterTemplate,getAttachmentRecord
        * @Modified By: Brijesh
        * @Modified Date: 28/10/2021 
    */
    @isTest
    public static void getEmailIdOfAccountTest(){
        Id accRecId = Label.PersonAccount;
        test.startTest();
        
        //member record
        Account a = new Account();
        //a.Name = 'Sam';
        a.LastName = 'Sam';
        a.RecordTypeId = accRecId;
        a.PersonEmail = 'brijeshkumar4@kpmg.com';
        insert a;
        //nephew        
        Account c = new Account();
        c.LastName =  'Wills';
        c.RecordTypeId = accRecId;
        c.PersonEmail = 'aishwaryag3@kpmg.com';
        insert c;
        //caregiver
        Account cm = new Account();
        cm.LastName = 'care man';
        cm.RecordTypeId = accRecId;
        cm.PersonEmail = 'rohitpanchhi@kpmg.com';
        insert cm;
        
        //member location
        Schema.Location l = new Schema.Location();
        l.Name = 'Members Location';
        l.LocationType = 'Primary Location';
        l.Account__c= a.id;
        insert l;
        
        Schema.Location l2 = new Schema.Location();
        l2.Name = 'Nephews Location';
        l2.LocationType = 'Primary Location';
        l2.Account__c= a.id;
        insert l2;
        
        Schema.Location l3 = new Schema.Location();
        l3.Name = 'Care Man Location';
        l3.LocationType = 'Primary Location';
        l3.Account__c= a.id;        
        insert l3;
        
        Schema.Address ad = new Schema.Address();
        ad.ParentId = l.id;
        ad.Account__c =a.id;
        ad.Street = '107 ems nagar';
        ad.City = 'aaa';
        ad.State = 'bbb';
        ad.PostalCode = '160055';
        ad.LocationType  = 'Primary Address';
        ad.AddressType = 'Mailing';
        ad.CMA_Primary_Address__c = true;
        insert ad;
        
        Schema.Address ad2 = new Schema.Address();
        ad2.ParentId = l2.id;
        ad2.Account__c =c.id;
        ad2.Street = '107 ems nagar';
        ad2.City = 'aaa';
        ad2.State = 'bbb';
        ad2.PostalCode = '160055';
        ad2.LocationType  = 'Primary Address';
        ad2.AddressType = 'Mailing';
        ad2.CMA_Primary_Address__c = true;
        insert ad2;
        
        Schema.Address ad3 = new Schema.Address();
        ad3.ParentId = l3.id;
        ad3.Account__c =cm.id;
        ad3.Street = '107 ems nagar';
        ad3.City = 'aaa';
        ad3.State = 'bbb';
        ad3.PostalCode = '160055';
        ad3.LocationType  = 'Primary Address';
        ad3.AddressType = 'Mailing';
        ad3.CMA_Primary_Address__c = true;
        insert ad3;
        
        //relationship records
        HealthCloudGA__ReciprocalRole__c memberRole = new HealthCloudGA__ReciprocalRole__c();
        memberRole.HealthCloudGA__InverseRole__c = 'Caregiver';
        memberRole.Name = 'Member';
        memberRole.RecordTypeId = Label.AccountRolRecTypeId;
        insert memberRole; 
        
        HealthCloudGA__ReciprocalRole__c son = new HealthCloudGA__ReciprocalRole__c();
        son.HealthCloudGA__InverseRole__c = 'Aunt';
        son.Name = 'Nephew';
        son.RecordTypeId = Label.ContactRolRecTypeId;
        insert son;
        
        //account account relation record
        HealthCloudGA__AccountAccountRelation__c aar = new HealthCloudGA__AccountAccountRelation__c();
        aar.HealthCloudGA__Account__c = a.Id;
        aar.HealthCloudGA__RelatedAccount__c = cm.Id;
        aar.HealthCloudGA__Role__c = memberRole.Id;
        aar.HealthCloudGA__Active__c = true;
        insert aar;
        
        /*HealthCloudGA__ContactContactRelation__c ccr = new HealthCloudGA__ContactContactRelation__c();
        ccr.HealthCloudGA__Contact__c = c.PersonContactId;
        ccr.HealthCloudGA__RelatedContact__c = a.PersonContactId;
        ccr.HealthCloudGA__Active__c = true;
        ccr.HealthCloudGA__Role__c = son.Id;
        insert ccr;
        */
        
        CMA_Verification__c verify = new CMA_Verification__c();
        verify.CMA_Member_Id__c = a.id;
        insert verify;
        
        
        CMA_CarePlanLettersController.getLetterTemplate('Letter');
        CMA_CarePlanLettersController.getAttachmentRecord('test');
        CMA_CarePlanLettersController.addressMethod(a.id);
        List<String> addr = new List<String>();
        addr.add('107 ems nagar aaa kkk 1231 ooo');
        CMA_CarePlanLettersController.createCommReq(a.id, 'Email', '5', '[["108"]]', '["aishwaryag3@kpmg.com"]', '[{"primaryId":"'+a.id+'","recipient":"brijeshkumar4@kpmg.com","disabletocheckbox":true}]', addr);

        CMA_CarePlanLettersController.createCommReq(a.id, 'Letter', '5', '[["108"]]', '[{"ccId":"'+c.id+'","ccEmail":"aishwaryag3@kpmg.com","ccAddress":"13th cross Bangalore Karnataka 560088 INDIA"}]', '[{"primaryId":"'+a.id+'","recipient":"brijeshkumar4@kpmg.com","disabletocheckbox":true,"selectedtoaddress":"abc area pune mh 923 India"}]', addr);

        CMA_CarePlanLettersController.fetchComRequest(a.id);
        test.stopTest();
    }
}