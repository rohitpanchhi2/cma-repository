global class CMA_RelatedTaskCreation {
    global class DataWrapper {
        @InvocableVariable(label='MemberNote Id' required=false)
        public String MemberNoteId;
        @InvocableVariable(label='JSON String' required=false)
        public String inputString; 
    }
    @InvocableMethod
    public static void insertRelatedTask(List<DataWrapper> passedData)
    {
     try{
        List<CMA_Related_Task__c> rtList = new List<CMA_Related_Task__c>();
        for (DataWrapper dw: passedData) {
            List<Object> fieldList = (List<Object>)JSON.deserializeUntyped(String.valueOf(dw.inputString));
            
            for(Object fld : fieldList){    
                Map<String,Object> data = (Map<String,Object>)fld;
                
                CMA_Related_Task__c rt = new CMA_Related_Task__c();
                rt.CMA_Member_Note__c = dw.MemberNoteId;
                rt.CMA_Intervention_ID__c = string.valueOf(data.get('target')) ;
                rt.CMA_Included__c = boolean.valueOf(data.get('included'));
                
                if(data.get('closed') == null){
                    rt.CMA_Closed__c =  false;                  
                }
                else{
                    rt.CMA_Closed__c =  boolean.valueOf(data.get('closed'));
                }                        
                rtList.add(rt);  
            }
            insert rtList; 
        }
    }
        catch(Exception e){
            SystemLogUtils.insertSystemLogs(e);
        }

    }
}