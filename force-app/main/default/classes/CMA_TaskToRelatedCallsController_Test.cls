/*
Description : Test Class for CMA_TaskToRelatedCallsController           
*Author : KPMG
*Code Created Date : 29th October 2021
*/
@isTest
public class CMA_TaskToRelatedCallsController_Test {
    
    /*
		* @Description: test method for getPGIDetails and getCallNotesTasks
		* @param name : NA
		* @return type: NA
		* @Developer: Aishwarya 
	*/
	@isTest
    public static void getPGIDetails_Test(){
        test.startTest();
        Account account = CMA_DataProviderClass.getAccount();
        insert account;
        
        Case c = CMA_DataProviderClass.getCase();
        c.AccountId = account.id;
        insert c;
        
        Contact contact = CMA_DataProviderClass.getContact();
        contact.AccountId = account.id;
        insert contact;
        
        CMA_TaskToRelatedCallsController.getPGIDetails(account.id);
        CMA_TaskToRelatedCallsController.getCallNotesTasks(account.id);
        test.stopTest();
    }
    
    /*
		* @Description: test method for getCallNotesDetails and getInterventionDetails
		* @param name : NA
		* @return type: NA
		* @Developer: Aishwarya 
	*/
    @isTest
    public static void getCallNotesDetails_Test(){
        test.startTest();
        Account account = CMA_DataProviderClass.getAccount();
        insert account;
        
        Case c = CMA_DataProviderClass.getCase();
        c.AccountId = account.id;
        insert c;
        
        Contact contact = CMA_DataProviderClass.getContact();
        contact.AccountId = account.id;
        insert contact;
        
        Task task = new Task();
        task.WhoId = contact.id;
        task.WhatId = c.Id;
        task.Subject = 'Call Notes';
        task.Type = 'Call';
        insert task;
        
        CMA_Related_Task__c relatedTask = new CMA_Related_Task__c();
		relatedTask.CMA_Note_ID__c = task.Id;
		relatedTask.CMA_Intervention_ID__c = task.Id;  
        
        CMA_TaskToRelatedCallsController.getCallNotesDetails(task.id);
        CMA_TaskToRelatedCallsController.getInterventionDetails(task.id);
        test.stopTest();
    }
}