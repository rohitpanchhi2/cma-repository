@isTest
public class CMA_RelatedTaskCreationtest {
    @isTest
    public static void createTaskRecordTest(){
        test.startTest();
        
        List<CMA_RelatedTaskCreation.DataWrapper> finalList = new List<CMA_RelatedTaskCreation.DataWrapper>();
        CMA_RelatedTaskCreation.DataWrapper  obj = new CMA_RelatedTaskCreation.DataWrapper();
        
        Member_Note__c memObj = new Member_Note__c();
        memObj.Category__c = 'Outreach';
        insert memObj;
        
        obj.MemberNoteId = memObj.Id;
        obj.inputString = '[{"target":"00T5f000008JBRHEA4","included":true},{"target":"00T5f000008JBQsEAO","included":true,"closed":true}]';

        finalList.add(obj);
        CMA_RelatedTaskCreation.insertRelatedTask(finalList);
        
        test.stopTest();
    } 
}