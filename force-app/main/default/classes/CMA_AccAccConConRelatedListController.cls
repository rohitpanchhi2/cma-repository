public class CMA_AccAccConConRelatedListController {
        @AuraEnabled
        public static List<HealthCloudGA__AccountAccountRelation__c> getAccountRecord(String RecId, Integer intLimit)    
        {
            return new List<HealthCloudGA__AccountAccountRelation__c>([Select Id,Name,HealthCloudGA__Account__c,HealthCloudGA__Account__r.Name,CMA_Account_Name__c,HealthCloudGA__Active__c,HealthCloudGA__EndDate__c,HealthCloudGA__ExternalId__c,HealthCloudGA__InverseRelationship__c,HealthCloudGA__RelatedAccount__c,HealthCloudGA__Role__r.Name,HealthCloudGA__StartDate__c,CMA_Status__c,OwnerId from HealthCloudGA__AccountAccountRelation__c WHERE HealthCloudGA__Account__c =:RecId ORDER BY CreatedDate DESC LIMIT : Integer.valueOf(intLimit)]); 
        }  
    
        @AuraEnabled
        public static List<HealthCloudGA__ContactContactRelation__c> getContactRecord(String RecId, Integer intLimit)    
        {
            return new List<HealthCloudGA__ContactContactRelation__c>([Select Id,Name,HealthCloudGA__Contact__c,HealthCloudGA__Contact__r.Name,CMA_Contact_Name__c,HealthCloudGA__Active__c,HealthCloudGA__EndDate__c,HealthCloudGA__ExternalId__c,HealthCloudGA__InverseRelationship__c,HealthCloudGA__RelatedContact__c,HealthCloudGA__Role__r.Name,HealthCloudGA__StartDate__c,CMA_Status__c,OwnerId from HealthCloudGA__ContactContactRelation__c WHERE Account__c =:RecId ORDER BY CreatedDate DESC LIMIT : Integer.valueOf(intLimit)]); 
        } 
}