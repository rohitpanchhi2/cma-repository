public class CMA_DisplayLastWorkedMemberController {

    @AuraEnabled    
    public static  Map<String , list<Sobject>>  getMemberDetails() {     		
        Map<String,list<Sobject>> accFinalMap = new Map<String,list<Sobject>>();       
        Id currentUserId = UserInfo.getUserId();
        
        List<Task> TaskList = [SELECT Id, Subject, Account.Name, AccountId,LastModifiedDate FROM Task WHERE LastModifiedDate = LAST_N_DAYS:7 AND Status != 'Completed' AND  LastModifiedById =: currentUserId AND AccountId != null];
        for(Task tlst : TaskList)
        {            
            if (accFinalMap.containsKey(tlst.Account.Name)) {
                accFinalMap.get(tlst.Account.Name).add(tlst);
            }
            else{
                accFinalMap.put(tlst.Account.Name,new List<sobject>{tlst});
            }
        }
       
        List<Case> CaseList = [SELECT Id, Subject, Account.Name, AccountId,LastModifiedDate FROM Case WHERE LastModifiedDate = LAST_N_DAYS:7 AND Status != 'Completed' AND LastModifiedById =: currentUserId AND AccountId != null]; 
        for(Case cslst : CaseList) { 

            if(accFinalMap.containsKey(cslst.Account.Name)) {
                accFinalMap.get(cslst.Account.Name).add(cslst);
            }
            else{
                accFinalMap.put(cslst.Account.Name,new List<sobject>{cslst});
            }
        }

        List<Member_Note__c> MemList = [SELECT Id, Name, Account__r.Name, Account__c,LastModifiedDate FROM Member_Note__c WHERE LastModifiedDate = LAST_N_DAYS:7 AND LastModifiedById =: currentUserId AND Account__c != null]; 
        for(Member_Note__c mmlst : MemList) {            
            if(accFinalMap.containsKey(mmlst.Account__r.Name)) {
                accFinalMap.get(mmlst.Account__r.Name).add(mmlst);
            }
            else{
                accFinalMap.put(mmlst.Account__r.Name,new List<sobject>{mmlst});
            }
        }
           
		system.debug('accFinalMap--'+accFinalMap);
        return accFinalMap;     
        
    }
}