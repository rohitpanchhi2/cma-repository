/*
Description : Remote Class for displaying related Assessment Records on Omniscript through LWC and task creation done after entering details on Call Notes Screen           
*Author : KPMG
*Code Created Date : 21th October 2021
*/
global with sharing class CMA_AssessmentDetailsSection implements vlocity_ins.VlocityOpenInterface{
    
    public static final String subject = 'Call Notes';
    public static final String taskType = 'Call';
    public static final String decTabApiName = 'Unsuccessful_Call__c';
    
    public Boolean invokeMethod(String methodName, Map<String,Object> input, Map<String,Object> outMap, Map<String,Object> options){
        Boolean result = true;
        try {
            if(methodName.equals('getAssessment')){
                getAssessment(input, outmap, options);
            }
            else if(methodName.equals('getAccountPhone')){
                getAccountPhone(input, outmap, options);
            }
            else if(methodName.equals('updateAccountPhone')){
                updateAccountPhone(input, outmap, options);
            }
            else if(methodName.equals('createTaskRecord')){
                createTaskRecord(input, outmap, options);
            }
        }
        
        catch(Exception e) 
        {
            System.debug('getAssessment : invokeMethod -> exception: '+e.getStackTraceString());
            system.debug('exception :'+e.getMessage());
            result = false;
        }
        return result;
    }
    
    /*
* @Description: method for fetching assessment record
* @param name : input from omniscript
* @return type: NA
* @Developer: Aishwarya 
*/
    private void getAccountPhone(Map<String, Object> input, Map<String, Object> outMap, Map<String, Object> options){
        system.debug('input map ->'+input);
        String accountId = (String)input.get('AccountId');
        List<CMA_Phone__c> phoneList = [SELECT Name,CMA_Account__c,CMA_Phone_Number__c,CMA_Phone_Preference__c,
                                        CMA_Phone_Type__c FROM CMA_Phone__c WHERE CMA_Account__c =: accountId
                                        AND CMA_Phone_Preference__c != 'Do Not Call'];
        if(phoneList != null)
            outMap.put('data',phoneList);
    }
    private void updateAccountPhone(Map<String, Object> input, Map<String, Object> outMap, Map<String, Object> options){
        system.debug('input map ->'+input);
        String accountId = (String)input.get('accId');
        System.debug('accountId: '+accountId);
        
        List<CMA_Phone__c> phoneList = [SELECT Name,CMA_Account__c,CMA_Phone_Number__c,CMA_Phone_Preference__c,
                                        CMA_Phone_Type__c FROM CMA_Phone__c WHERE CMA_Account__c =: accountId];
        
        List<Object> arguments = (List<object>)input.get('recordsToUpdate');
        system.debug('arguments-> '+arguments);
        for(Object fld : arguments){    
            Map<String,Object> data = (Map<String,Object>)fld;
            system.debug('data-> '+data);
            for(CMA_Phone__c phoneRecord : phoneList){
                if(phoneRecord.Id == string.valueOf(data.get('Id'))){
                    phoneRecord.CMA_Phone_Preference__c = string.valueOf(data.get('CMA_Phone_Preference__c'));
                    phoneRecord.CMA_Phone_Type__c = string.valueOf(data.get('CMA_Phone_Type__c'));
                }
            }
        } 
        system.debug('phoneList -> '+phoneList);
        update phoneList;
        if(phoneList != null)
            outMap.put('dataPhone',phoneList);
    }
    
    private void getAssessment(Map<String, Object> input, Map<String, Object> outMap, Map<String, Object> options){
        for(String str: input.keyset()){
            System.debug('Key: '+str);
            System.debug('Value: '+input.get(str));
        }
        String accountId = (String)input.get('AccountId');
        List<vlocity_ins__Assessment__c> assessmentList = [SELECT Name, vlocity_ins__CompletedDate__c  FROM vlocity_ins__Assessment__c where    CMA_Account__c =: accountId];
        if(assessmentList != null)
            outMap.put('data',assessmentList);
    }
    
    /*
* @Description: method for creating task records based on details entered on Call Notes Screen
* @param name : input from omniscript
* @return type: NA
* @Developer: Aishwarya 
*/
    private void createTaskRecord(Map<String, Object> input, Map<String, Object> outMap, Map<String, Object> options){
        for(String str: input.keyset()){
            System.debug('Key: '+str);
            System.debug('Value: '+input.get(str));
        }
        String accountId = (String)input.get('Formula');
        String accId = (String)input.get('ContxtId');
        String enrollmentStatus = (String)input.get('Enrollment Status');
        String callType = (String)input.get('CallType');
        String callResults = (String)input.get('Call Results');
        String CallNotesTextArea = (String)input.get('CallNotesTextArea');
        
        List<Member_Note__c> insertTaskList = new List<Member_Note__c>();
        List<Case> caseList = [SELECT Subject,AccountId FROM Case WHERE AccountId =:accId AND Status ='Active'];
        
        Contact contact = [SELECT Name FROM Contact WHERE AccountId =: accountId LIMIT 1];
        
      /*  Task task = new Task();
        task.CMA_Call_Status__c = callResults;
        task.CMA_Event_Type__c = callType;
        task.ActivityDate = Date.valueof(system.now());
        task.Description = CallNotesTextArea;
        task.Type = taskType;
        task.TaskSubtype = taskType;
        if(enrollmentStatus != null){
        task.CMA_Enrollment_Status__c = enrollmentStatus;
        }
        if(contact != null){
        task.WhoId = contact.id;
        }
        if(caseList != null){
        task.WhatId = caseList[0].Id;
        }
        task.Subject = subject;
        
        insertTaskList.add(task);        
        insert insertTaskList; */
        
        Member_Note__c task = new Member_Note__c();
        task.Account__c = accId;
        if(caseList != null){
            task.Case__c = caseList[0].Id;
        }
        task.Notes__c = CallNotesTextArea;
        task.Type__c = taskType;
        insertTaskList.add(task);        
        insert insertTaskList;
        outMap.put('IPResult',task);
        
        /*Invoking Rule Engine for created Call Notes and passing back Output for Reminder*/
        System.debug('Remote Call invokeRuleEngine');        
        List<RuleEngineOutputWrap> outputWrapList = new List<RuleEngineOutputWrap>();
        //CMA_RuleEngineController.invokeDTwDataSet(insertTaskList, decTabApiName);
        Decimal outputInteger;
        String outputString;
        for(RuleEngineOutputWrap objData :outputWrapList){
            for(RuleEngineOutputData objValue: objData.outputValues){
                if(objValue.outputInt != null){
                    if(outputInteger == null){
                        outputInteger = objValue.outputInt;
                        outputString = objValue.outputString;
                    }
                    else if(outputInteger < objValue.outputInt){
                        outputInteger = objValue.outputInt;
                        outputString = objValue.outputString;
                    }
                }
            }
        }
        System.debug('OutputRuleEngine:'+outputInteger);
        Date outputvalue = System.today().addDays(Integer.valueof(outputInteger));
        outMap.put('OutputRuleEngineDate',outputvalue);
        outMap.put('OutputRuleEngineSubject',outputString);
    }     
    
}