/*
Description : Test class for CMA_GetAccountPhoneData       
*Author : KPMG
*Code Created Date : 17th October 2021
*/
@isTest
public class CMA_GetAccountPhoneData_Test {
	
    /*
		* @Description: test method for getAccountPhone,getPickListValuesOfPhoneType,getPickListValuesofPhonePreference,savePhone,insertPhone
		* @param name : NA
		* @return type: NA
        * @Developer: Aishwarya 
	*/
    @isTest
    public static void getAccountPhoneTest(){
        test.startTest();
        Account a = CMA_DataProviderClass.getAccount();
        insert a;
        
        CMA_Phone__c phone = new CMA_Phone__c();
        phone.CMA_Account__c = a.id;
        phone.CMA_Phone_Number__c = '8676747674';
        phone.CMA_Phone_Type__c = 'Work';
        phone.CMA_Phone_Preference__c = 'Primary';
        
        List<CMA_Phone__c> phoneList = new List<CMA_Phone__c>();
        phoneList.add(phone);
        insert phoneList;
        
        CMA_GetAccountPhoneData.getAccountPhone(a.id);
        CMA_GetAccountPhoneData.getPickListValuesOfPhoneType();
        CMA_GetAccountPhoneData.getPickListValuesofPhonePreference();  
        CMA_GetAccountPhoneData.savePhone(phoneList);
        CMA_GetAccountPhoneData.insertPhone(phoneList,a.id);
        test.stopTest();
    }
}