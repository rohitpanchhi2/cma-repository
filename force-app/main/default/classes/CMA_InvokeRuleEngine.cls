/**
@Description : Class contains general logic for Invoking Rule Engine from Flow 
@Author : KPMG - KGS
@Code Created : October 2021
**/
public class CMA_InvokeRuleEngine {
    /**
    @Description : Input Wrapper which accepts Record and Dec.Tab API name passed from flow 
    @Developer: santoshrebello@KPMG-KGS
    **/
	public class InputDataWrapper {
        @InvocableVariable(label='Decision Table API Name' required=false)
        public String decTabApi;
        @InvocableVariable(label='Input Data List' required=true)
        public Sobject inputDataList;      
    }    
    
    /**
	@Description: Method that invokes Rule Engine and returns the formed output wrapper to the Flow
	@param name : dataParam - List of InputDataWrapper passed from Flow ;
	@return type: returns List of RuleEngineOutputWrap 
	@Developer: santoshrebello@KPMG-KGS
	**/
    @InvocableMethod(label='Call Rule Engine')
    public static List<RuleEngineOutputWrap> callRulesExecute(List<InputDataWrapper> dataParam){ 
        System.debug('List of Records: '+ dataParam);        
        List<sObject> inputRecList = new List<sObject>();
        String decTabApiName = dataParam[0].decTabApi ;
        for (InputDataWrapper dw: dataParam) {
            inputRecList.add(dw.inputDataList);
        }        
   	   	
        List<RuleEngineOutputWrap> outputWrapList = CMA_RuleEngineController.invokeDTwDataSet(inputRecList, decTabApiName);
          
        while(dataParam.size() !=  outputWrapList.size())
        {            
            RuleEngineOutputWrap objDummyData = new RuleEngineOutputWrap();
            outputWrapList.add(objDummyData);
            if(dataParam.size() ==  outputWrapList.size())
            {
                system.debug('coming inside check');
                break;                
            }
        }
        
        system.debug('InputParam Size: '+dataParam.size());
        system.debug('OutputParam Size: '+outputWrapList.size());
        System.debug('outputWrapList: '+outputWrapList);
        return outputWrapList;
    }
}