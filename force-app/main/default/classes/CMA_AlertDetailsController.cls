public class CMA_AlertDetailsController {
    @AuraEnabled(cacheable=true)
    public static List<CMA_Alert__c> getAlertList(String recordId,String objApi) {
        if(recordId != null && objApi=='Account'){
            return [ SELECT Id, Name, CMA_Alert__c, CMA_Account__c, CMA_Case__c, CMA_Category__c,CMA_Priority__c FROM CMA_Alert__c
                     WHERE CMA_Account__c =: recordId WITH SECURITY_ENFORCED ORDER BY CreatedDate DESC ];
        }
        else if(recordId != null && objApi=='Account'){
            return [ SELECT Id, Name, CMA_Alert__c, CMA_Account__c, CMA_Case__c, CMA_Category__c,CMA_Priority__c FROM CMA_Alert__c
                     WHERE CMA_Case__c =: recordId WITH SECURITY_ENFORCED ORDER BY CreatedDate DESC];
        }
        else if(recordId == null){
            return [ SELECT Id, Name, CMA_Alert__c, CMA_Account__c, CMA_Case__c, CMA_Category__c,CMA_Priority__c,CMA_StartDate__c,CMA_End_Date__c FROM CMA_Alert__c
                     WHERE CMA_Case__c =: recordId WITH SECURITY_ENFORCED ORDER BY CreatedDate DESC];
        }

        return null;
    }
}