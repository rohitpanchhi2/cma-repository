/*
Description : Class for Letter Generation Screen           
*Author : KPMG
*Code Created Date : 12th October 2021
*/
public class CMA_CarePlanLettersController {
    public class applicationException extends Exception {}
    /*
* @Description: method for fetching letter template
* @param name : Type of template
* @return type: list of CMA_Letter_Templates__c
* @Developer: Aishwarya 
*/
    @AuraEnabled
    public static List<CMA_Letter_Templates__c> getLetterTemplate(string type){
        List<CMA_Letter_Templates__c> letterTemplateList = [SELECT Name,CMA_Template_Id__c,CMA_Attachment__c, CMA_Template_Relations__c, CMA_Type__c, UniqueID__c FROM CMA_Letter_Templates__c WHERE CMA_Type__c =: type];
        if(letterTemplateList != null)
            return letterTemplateList;
        else
            return null;
    }
    
    
    /*
* @Description: method for fetching attachments
* @param name : template id
* @return type: map of template id and attachment id
* @Developer: Aishwarya 
*/
    @AuraEnabled
    public static List<String> getAttachmentRecord(String templateId){
        System.debug('tempid '+ templateId);
        List<String> templateVSAttachmentMap = new List<String>();
        List<CMA_Template_Relations__c> idList = [SELECT CMA_Attachment_Id__c, CMA_Template_Id__c,Name from CMA_Template_Relations__c WHERE CMA_Template_Id__c =: templateId];
        List<String> templateIdList = new List<String>();
        if(idList != null){
            for(CMA_Template_Relations__c templateIds : idList){
                templateIdList.add(templateIds.CMA_Attachment_Id__c);
                templateVSAttachmentMap.add(templateIds.CMA_Attachment_Id__c);
            }
        }
        return templateVSAttachmentMap;
    }
    
    /*
* @Description: method for Accounts and addrese
* @param name : account id
* @return type: list of Accounts
* @Developer: Brijesh 
*/
    @AuraEnabled
    public static  List<Account> addressMethod(String accountId){
        Map<String,List<String>> nameVsAddressMap = new Map<String,List<String>>();
        
        Set<String> IdSet = new Set<String>();
        IdSet.add(accountId);
        
        List<CMA_Verification__c> authPartyAddress = [SELECT CMA_Member_Id__c,CMA_Authorized_Party__c,CMA_AuthPartyAddress__c FROM CMA_Verification__c WHERE CMA_Member_Id__c =: accountId];
        if(authPartyAddress != null){
            for(CMA_Verification__c authpartyadd : authPartyAddress){  
                IdSet.add(authpartyadd.CMA_Authorized_Party__c);
            }  
        }
        
        List<HealthCloudGA__AccountAccountRelation__c> accountAccountRelationList = [SELECT CMA_Account_Name__c, CMA_Status__c,HealthCloudGA__RelatedAccount__c from HealthCloudGA__AccountAccountRelation__c where HealthCloudGA__Account__c =:accountId AND CMA_Status__c = 'Active'];
        if(accountAccountRelationList != null){
            for(HealthCloudGA__AccountAccountRelation__c record : accountAccountRelationList){
                IdSet.add(record.HealthCloudGA__RelatedAccount__c);
            }
            
        }
        
        Set<Id> conId = new Set<Id>();
        List<HealthCloudGA__ContactContactRelation__c> contactContactRelationList = [SELECT id,CMA_Contact_Name__c, HealthCloudGA__Contact__c FROM HealthCloudGA__ContactContactRelation__c WHERE Account__c =: accountId];
        if(contactContactRelationList != null){
            for(HealthCloudGA__ContactContactRelation__c contactRecord : contactContactRelationList){
                conId.add(contactRecord.HealthCloudGA__Contact__c);
            }
            
        }
        
        List<Contact> con = [Select Id, isPersonAccount, AccountId from Contact where id in : conId];
        if(!con.isEmpty()){
            for(Contact c : con){
                if(c.accountId <> null){
                    IdSet.add(c.accountId);
                }
            }   
        }
        
        IdSet.remove(null);
        
        List<Account> accList = [Select Id, Name, PersonEmail, CMA_Role__c, (SELECT id, Name, Account__c,CMA_FullAddress__c, Account__r.Name, Account__r.PersonEmail, Address, Address.city, Address.street, Address.postalCode, Address.state, Address.country,AddressType FROM Addresses__r) From Account where id in : IdSet];
        if(!accList.isEmpty()){
            return accList;
        }
        else{
            return null;
        }
    }
    
    /*
* @Description: method to create communication request record
* @param name : account id, email, address, communication type
* @return type: boolean.
* @Developer: Brijesh 
*/
    
    @AuraEnabled
    public static Boolean createCommReq(String recId, String comType, String comTempId, String selAtt, String ccMail, String toMail, List<String> toAdd){
        System.debug(recId);
        System.debug(comType);
        System.debug(comTempId);
        System.debug(selAtt);
        System.debug(ccMail);
        System.debug(toMail);
        System.debug(toAdd);
        ToListRecords tList = new ToListRecords();
        List<Communication_Request__c> cr = new List<Communication_Request__c>();
        string tempList = '{"toList":'+toMail+'}';
        tList = (ToListRecords)System.JSON.deserialize(tempList, ToListRecords.class);
        
        for(ToWrapper tw: tList.toList){
            Communication_Request__c c = new Communication_Request__c();
            String temp = selAtt.replace('"','');
            temp = temp.replace('[[','');
            temp = temp.replace(']]',',');
            temp = temp.removeEnd(',');
            String ccEmails = ccMail <> null ? string.valueOf(string.valueOf(ccMail.replace('[','')).replace(']','')).replaceAll('"','') : '';
            c.CMA_Primary_Member__c = tw.primaryId.substring(0, 18);
            c.CMA_Member__c = recId;
            c.CMA_Status__c = 'New';
            c.CMA_Template_Id__c = comTempId;
            c.CMA_Attachment_Id__c = temp;
            c.CMA_Type__c =comType;
            c.CMA_To_Emails__c = tw.recipient;
            c.CMA_Address__c = tw.selectedtoaddress;
            c.CMA_CC_Email__c = comType == 'Email' ? ccEmails : ccMail;
            cr.add(c);
        }
        if(!cr.isEmpty()){
            try{
                insert cr;
            }
            catch(Exception e){
                SystemLogUtils.insertSystemLogs(e);
            }
            
            if(comType == 'Letter'){
                List<sObject> ret = createCCRecords(cr);
            }
            return true;
            
        }else{
            return false;
        }
    }
    
    /*
* @Description: method to fetch communication request records
* @param name : account id
* @return type: list of Communication_Request__c.
* @Developer: Brijesh 
*/
    
    @AuraEnabled
    public static List<Communication_Request__c> fetchComRequest(String recordId){
        return [Select Id, Name, CreatedDate, CMA_Status__c, CMA_Send_Date__c, CMA_Member__c, CMA_Template_Id__c, CMA_Attachment_Id__c, CMA_Type__c, CMA_To_Emails__c, CMA_Address__c, CMA_CC_Email__c from Communication_Request__c where CMA_Member__c =: recordId];
    }
    
    /*
* @Description: method to create CMA_Communication_CC_Members__c records
* @param name : List of Communication_Request__c
* @return type: list of CMA_Communication_CC_Members__c.
* @Developer: Brijesh 
*/
    
    @AuraEnabled
    public static List<CMA_Communication_CC_Members__c> createCCRecords(List<Communication_Request__c> comReq){
        CCListRecords cList = new CCListRecords();
        List<CMA_Communication_CC_Members__c> ccMemberList = new List<CMA_Communication_CC_Members__c>();
        for(Communication_Request__c cor: comReq){
            if(cor.CMA_CC_Email__c <> null || cor.CMA_CC_Email__c <> ''){
                string temp = '{"ccList":'+cor.CMA_CC_Email__c+'}';
                
                cList = (CCListRecords)System.JSON.deserialize(temp, CCListRecords.class);
                
                for(CCWrapper c: cList.ccList){
                    CMA_Communication_CC_Members__c ccMembers = new CMA_Communication_CC_Members__c();
                    ccMembers.CMA_Account__c = c.ccId.substring(0, 18);
                    ccMembers.CMA_Communication_Request__c = cor.id;
                    ccMembers.CMA_Address__c = c.ccAddress;
                    ccMemberList.add(ccMembers);
                }  
            }
        }
        if(!ccMemberList.isEmpty()){
            try{
                insert ccMemberList;
            }
            catch(Exception e){
                SystemLogUtils.insertSystemLogs(e);
            }
            
            return ccMemberList;
        }else{
            return null;
        }
        
    }
    
    /*
* @Description: method to Send email alert
* @param name : List of Communication_Request__c
* @return type: null
* @Developer: Brijesh 
*/
    
    public static void sendEmailAlert(List<Communication_Request__c> comReq){
        
        EmailTemplate emailTemplate = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE Name = 'EmailTemplateForCommunicationRequest']; //Email Template's ID
        String htmlBody = emailTemplate.HtmlValue;
        String plainBody = emailTemplate.Body;
        
        Set<Id> comId = new Set<Id>();
        List<Communication_Request__c> comReqList = new List<Communication_Request__c>();
        List<Communication_Request__c> comReqListUpdate = new List<Communication_Request__c>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        for(Communication_Request__c cor : comReq){
            comId.add(cor.id);
        }
        if(!comId.isEmpty()){
            comReqList = [Select id, CMA_Primary_Member__r.Name, CMA_Type__c, CMA_Primary_Member__c, CMA_To_Emails__c, CMA_CC_Email__c from Communication_Request__c where id in: comId];    
        }
        
        for(Communication_Request__c cr : comReqList){
            Communication_Request__c cUpdate = new Communication_Request__c();
            if(cr.CMA_Type__c == 'Email'){
                system.debug('check----'+cr.CMA_Primary_Member__r.Name);
                plainBody = plainBody.replace('{!Communication_Request__c.00N5f00000FLxTc}', cr.CMA_Primary_Member__r.Name);
                system.debug('plainBody 212----'+plainBody);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setSenderDisplayName('Care Manager'); //Sender's Display name
                mail.setUseSignature(false);
                mail.setBccSender(false);
                mail.setSaveAsActivity(false);
                mail.setSubject(emailTemplate.Subject);
                //mail.setHtmlBody(htmlBody);
                mail.setPlainTextBody(plainBody);
                List<String> sendTo = new List<String>();
                List<String> sendCC = new List<String>();
                sendTo.add(cr.CMA_To_Emails__c);
                mail.setToAddresses(sendTo);
                if(cr.CMA_CC_Email__c <> null){
                    sendCC.add(cr.CMA_CC_Email__c);
                    mail.setCcAddresses(sendCC);   
                }
                mails.add(mail);
                cUpdate.id = cr.id;
                cUpdate.CMA_Send_Date__c = System.now();
                cUpdate.CMA_Status__c = 'Delivered';
                comReqListUpdate.add(cUpdate);
            }
            
        }
        try{
            if(!mails.isEmpty()){
                Messaging.sendEmail(mails);
                if(!comReqListUpdate.isEmpty()){
                    update comReqListUpdate;
                }
            }
        }
        
        catch(Exception e){
            SystemLogUtils.insertSystemLogs(e);
            throw new applicationException('Error Occurred!!!');
        }
    }
    
    
    /*
* @Description: Wrapper classes to create communication request records and communication cc request
* @param name : 
* @return type: list of ccwrapper and towrapper for communications.
* @Developer: Brijesh 
*/
    
    public class CCListRecords
    {
        public List<CCWrapper> ccList;
    }
    public class ToListRecords
    {
        public List<ToWrapper> toList;
    }
    public class CCWrapper
    {
        public string ccId { get; set; }
        public string ccEmail { get; set; }
        public string ccAddress { get; set; }
    }
    public class ToWrapper
    {
        public string primaryId { get; set; }
        public string recipient { get; set; }
        public string selectedtoaddress { get; set; }
    }
    
    
}