/*
Description : Class for getting Phone Records from Account, Insert new Phone record, Update Phone record          
*Author : KPMG
*Code Created Date : 9th October 2021
*/
public class CMA_GetAccountPhoneData {
    
    /*
		* @Description: method for fetching Phone record
		* @param name : account Id
		* @return type: List of CMA_Phone__c
        * @Developer: Aishwarya 
	*/

    @AuraEnabled
    public static List<CMA_Phone__c> getAccountPhone(String accountId){
        List<CMA_Phone__c> phoneList = [SELECT Name,CMA_Account__c,CMA_Phone_Number__c,CMA_Phone_Preference__c,
                                        CMA_Phone_Type__c FROM CMA_Phone__c WHERE CMA_Account__c =: accountId
                                        AND CMA_Phone_Preference__c != 'Do Not Call'];
        system.debug('phoneList -> **'+phoneList);
        if(phoneList != null){
            return phoneList;
        }
        else{
            return null;
        }
    }
    
     /*
		* @Description: method for fetching picklist values of phone type
		* @param name : NA
		* @return type: List of String
        * @Developer: Aishwarya 
	*/

    @AuraEnabled
    public static List<String> getPickListValuesOfPhoneType(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = CMA_Phone__c.CMA_Phone_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }   
        system.debug('pickListValuesList -> '+pickListValuesList);
        return pickListValuesList;
    }
    
     /*
		* @Description: method for fetching picklist values of phone preference
		* @param name : NA
		* @return type: List of String
        * @Developer: Aishwarya 
	*/
    @AuraEnabled
    public static List<String> getPickListValuesofPhonePreference(){
        List<String> pickListValuesList1= new List<String>();
        Schema.DescribeFieldResult fieldResult = CMA_Phone__c.CMA_Phone_Preference__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList1.add(pickListVal.getLabel());
        }  
        system.debug('pickListValuesList -> '+pickListValuesList1);
        return pickListValuesList1;
    }
    
     /*
		* @Description: method for updating Phone records
		* @param name : List of CMA_Phone__c
		* @return type: List of CMA_Phone__c
        * @Developer: Aishwarya 
	*/
    @AuraEnabled
    public static List <CMA_Phone__c> savePhone(List<CMA_Phone__c> lstPhone) {
        system.debug('****'+lstPhone);
        try{   
            update lstPhone;
        }catch(Exception e){
            system.debug('******'+e.getMessage());
        }
        return lstPhone;
    }
    
    /*
		* @Description: method for inserting Phone records
		* @param name : List of CMA_Phone__c and accountId
		* @return type: List of CMA_Phone__c
        * @Developer: Aishwarya 
	*/
    @AuraEnabled
    public static List <CMA_Phone__c> insertPhone(List<CMA_Phone__c> listPhone, String accountId) {
        system.debug('****'+listPhone);
        List<CMA_Phone__c> listPhoneReversed = new List<CMA_Phone__c>();
        List<CMA_Phone__c> listPhoneAdded = new List<CMA_Phone__c>();
        if(listPhone != null){
            for(Integer i = listPhone.size() - 1; i >= 0; i--){
                listPhoneReversed.add(listPhone[i]);
            }
        }
        if(listPhoneReversed != null){
            for(Integer j=0 ; j<= listPhoneReversed.size()-1 ;j++){
                listPhoneAdded.add(listPhoneReversed[j]);
                break;
            }
        }
        system.debug('listPhoneAdded ->'+listPhoneAdded);
        if(listPhoneAdded != null){
            CMA_Phone__c phone = new CMA_Phone__c();
            phone.CMA_Account__c = accountId;
            phone.CMA_Phone_Number__c = listPhoneAdded[0].CMA_Phone_Number__c;
            phone.CMA_Phone_Type__c = listPhoneAdded[0].CMA_Phone_Type__c;
            phone.CMA_Phone_Preference__c = listPhoneAdded[0].CMA_Phone_Preference__c;
            try{
                insert phone;
            }
            catch(Exception e){
                SystemLogUtils.insertSystemLogs(e);
            }
        }
        
        return listPhone;
    }
}