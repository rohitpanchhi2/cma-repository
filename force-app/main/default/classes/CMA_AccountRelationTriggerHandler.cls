/*
Description : Class for Letter Generation Screen           
*Author : KPMG
*Code Created Date : 26th October 2021
*/
public class CMA_AccountRelationTriggerHandler {
    
    /*
* @Description: method for updating account role on account record
* @param name : list of account account raltion
* @return type: void
* @Developer: Brijesh Kumar 
*/
    public static void updateAccRelation(List<HealthCloudGA__AccountAccountRelation__c> accRelationList){
        try{
            List<Account> updateList = new List<Account>();
            List<HealthCloudGA__AccountAccountRelation__c> accR = new List<HealthCloudGA__AccountAccountRelation__c>();
            Set<Id> accId = new Set<Id>();
            for(HealthCloudGA__AccountAccountRelation__c accRel : accRelationList){
                accId.add(accRel.id);
            }
            if(!accId.isEmpty()){
                accR = [Select id, HealthCloudGA__RelatedAccount__c, HealthCloudGA__Role__c, HealthCloudGA__Role__r.Name from HealthCloudGA__AccountAccountRelation__c where id in: accId];
            }
            
            if(!accR.isEmpty()){
                for(HealthCloudGA__AccountAccountRelation__c accRel : accR){
                    Account acc = new Account();
                    acc.Id = accRel.HealthCloudGA__RelatedAccount__c;
                    acc.CMA_Role__c = accRel.HealthCloudGA__Role__r.Name;
                    updateList.add(acc);
                }    
            }
            if(!updateList.isEmpty()){
                update updateList;
            }
        }
        catch(Exception e){
            SystemLogUtils.insertSystemLogs(e);
        }
        
    }
    
    /*
* @Description: method for updating account role on account record
* @param name : list of Contact contact raltion
* @return type: void
* @Developer: Brijesh Kumar 
*/    
    public static void updateConRelation(List<HealthCloudGA__ContactContactRelation__c> conRelationList){
        try{
            List<Account> updateList = new List<Account>();
            List<HealthCloudGA__ContactContactRelation__c> conR = new List<HealthCloudGA__ContactContactRelation__c>();
            Set<Id> conId = new Set<Id>();
            for(HealthCloudGA__ContactContactRelation__c conRel : conRelationList){
                conId.add(conRel.id);
                Account acc = new Account();
            }
            if(!conId.isEmpty()){
                conR = [Select id, HealthCloudGA__Contact__r.AccountId, HealthCloudGA__Contact__c, HealthCloudGA__Role__r.Name,  HealthCloudGA__Role__c from HealthCloudGA__ContactContactRelation__c where id in: conId];
            }
            
            if(!conR.isEmpty()){
                for(HealthCloudGA__ContactContactRelation__c conRelation : conR){
                    Account acc = new Account();
                    acc.Id = conRelation.HealthCloudGA__Contact__r.AccountId;
                    acc.CMA_Role__c = conRelation.HealthCloudGA__Role__r.Name;
                    updateList.add(acc);
                }
            }
            if(!updateList.isEmpty()){
                update updateList;
            }
        }
        catch(Exception e){
            SystemLogUtils.insertSystemLogs(e);
        }
        
    }
}