public class CMA_SearchForSourceObject {
    @AuraEnabled
    public static List<String> getAllCustomSObjects()
    {
        List<String> sObjectList = new List<String>();
        for(Schema.SObjectType objTyp : Schema.getGlobalDescribe().Values())
        {
            Schema.DescribeSObjectResult describeSObjectResultObj = objTyp.getDescribe();
            if(describeSObjectResultObj.isCustom())
            {
                String name = objTyp.getDescribe().getLabel();
                if(name.containsignorecase('Rule'))
                {
                    SobjectList.add(name);
                }
            }
        }
        system.debug('SObjectList****' + SObjectList);
        return sObjectList;
    }
    @AuraEnabled
    public static List<String> getAllPrimarySObjects()
    {
        List<String> pObjectList = new List<String>();
        for(Schema.SObjectType objTyp : Schema.getGlobalDescribe().Values())
        {
            Schema.DescribeSObjectResult describeSObjectResultObj = objTyp.getDescribe();
                String name = objTyp.getDescribe().getLabel();
                    pObjectList.add(name);              
        }
        system.debug('pObjectList****' + pObjectList);
        return pObjectList;
    }  
}