public class CMA_RelatedInterventionTask {
    
    @AuraEnabled
    public static List<Map<String, String>> getCaseDetails(String accountId) {      
        List<Map<String, String>> items = new List<Map<String, String>>();
        List<Case> caselist = [Select Id,Subject from Case where AccountId =:accountId AND Status != 'Completed' order by CreatedDate desc];
        for(Case cs : caselist)
        {
            items.add(new Map<String, String>{'value' => cs.Id, 'label' => cs.Subject});
        }
        return items;
    }
      
    @AuraEnabled
    public static List<Task> getPGIDetails(String caseId){
      //  List<Case> caselist = [Select Id from Case where AccountId =:accountId AND Status != 'Completed' order by CreatedDate desc];
        List<Task> TaskList = [SELECT Id,Status,ActivityDate,Subject,HealthCloudGA__CarePlanProblem__r.Name,HealthCloudGA__CarePlanGoal__r.Name,
                               HealthCloudGA__CarePlanGoal__r.HealthCloudGA__CarePlanProblem__r.Name
                               FROM Task WHERE  Type ='Intervention' AND Status != 'Completed'                           
                               AND whatId =: caseId]; 
        
        return TaskList;
    }
    
    @AuraEnabled
    public static List<Task> getPGIDetailsOnInitialLoad(String accountId){
        List<Case> caselist = [Select Id from Case where AccountId =:accountId AND Status != 'Completed' order by CreatedDate desc];
        List<Task> TaskList = [SELECT Id,Status,ActivityDate,Subject,HealthCloudGA__CarePlanProblem__r.Name,HealthCloudGA__CarePlanGoal__r.Name,
                               HealthCloudGA__CarePlanGoal__r.HealthCloudGA__CarePlanProblem__r.Name
                               FROM Task WHERE  Type ='Intervention' AND Status != 'Completed'                           
                               AND whatId =: caselist[0].Id]; 
        
        return TaskList;
    }
    
    @AuraEnabled
    public static List<vlocity_ins__Assessment__c> getAssessmentDetails(String accountId){
        List<vlocity_ins__Assessment__c> assessmentList = [SELECT Id, Name, vlocity_ins__CompletedDate__c 
                                                           FROM vlocity_ins__Assessment__c 
                                                           WHERE  CMA_Account__c =: accountId];       
        return assessmentList;
    }

    @AuraEnabled
    public static List<Task> getTaskBarrierDetails(){
        Id currentUserId = UserInfo.getUserId();
        List<Task> TskBrList = [SELECT Id,Status,ActivityDate,Subject,who.Name,what.Name 
                                FROM Task 
                                WHERE Type ='Intervention' AND Status != 'Completed' AND whatId IN ( SELECT Id From CareBarrier) AND OwnerId =: currentUserId]; 
        system.debug(TskBrList);
        return TskBrList;
        
    }

}