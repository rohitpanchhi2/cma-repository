/*
    Description : Class for Letter Generation Screen           
    *Author : KPMG
    *Code Created Date : 10th November 2021
    */
public class CMA_NewMemberVerificationScreenCntrl {
  
    /*
* @Description: method for fetching Phone Records
* @param name : Type of recordId
* @return type: list of CMA_Phone__c
* @Developer: Brijesh
*/
    @AuraEnabled
    public static list<CMA_Phone__c> getPhoneRecords(String recordId){
        return [Select id, name, CMA_Account__c, CMA_Phone_Number__c, CMA_Phone_Preference__c, CMA_Phone_Type__c from CMA_Phone__c 
                WHERE CMA_Account__c =: recordId AND CMA_Phone_Preference__c != 'Do Not Call'];
    }

    /*
* @Description: method for fetching Account Records
* @param name : Type of recordId
* @return type: list of Account
* @Developer: Brijesh
*/    
    @AuraEnabled
    public static list<Account> getContactPerson(String recordId){
        Set<String> IdSet = new Set<String>();
        IdSet.add(recordId);
        
        List<HealthCloudGA__AccountAccountRelation__c> accountAccountRelationList = [SELECT CMA_Account_Name__c, CMA_Status__c,HealthCloudGA__RelatedAccount__c from HealthCloudGA__AccountAccountRelation__c where HealthCloudGA__Account__c =:recordId AND CMA_Status__c = 'Active'];
        if(accountAccountRelationList != null){
            for(HealthCloudGA__AccountAccountRelation__c record : accountAccountRelationList){
                IdSet.add(record.HealthCloudGA__RelatedAccount__c);
            }
            
        }
        
        Set<Id> conId = new Set<Id>();
        List<HealthCloudGA__ContactContactRelation__c> contactContactRelationList = [SELECT id,CMA_Contact_Name__c, HealthCloudGA__Contact__c FROM HealthCloudGA__ContactContactRelation__c WHERE Account__c =: recordId];
        if(contactContactRelationList != null){
            for(HealthCloudGA__ContactContactRelation__c contactRecord : contactContactRelationList){
                conId.add(contactRecord.HealthCloudGA__Contact__c);
            }
            
        }
        
        List<Contact> con = [Select Id, isPersonAccount, AccountId from Contact where id in : conId];
        if(!con.isEmpty()){
            for(Contact c : con){
                if(c.accountId <> null){
                    IdSet.add(c.accountId);
                }
            }   
        }
        
        IdSet.remove(null);
        
        List<Account> accList = [Select Id, Name, PersonEmail, CMA_Role__c From Account where id in : IdSet];
        
        if(!accList.isEmpty()){
            return accList;    
        }
        else{
            return null;
        }
        
    }

    /*
* @Description: method for fetching Case Records
* @param name : Type of recordId
* @return type: list of Case
* @Developer: Santosh
*/    
    @AuraEnabled
    public static list<Case> getCaseCareProgram(String recordId){
        return [Select Id, Subject, AccountId, Account.Name, CMA_Care_Program__c, CMA_Care_Program__r.Name, Status from case where AccountId =:recordId AND Status='Active'];
    }

    /*
* @Description: method for fetching CareProgram Records
* @param name : Type of recordId
* @return type: list of CareProgram
* @Developer: Brijesh
*/    
    @AuraEnabled
    public static list<CareProgram> getCareProgram(String recordId){
        return [Select Id, Name FROM CareProgram];
    }
    
    /*
* @Description: class for account records along with role 
* @param name : null
* @return type: null
* @Developer: Santosh
*/    
    public class memberWrapper{
       @AuraEnabled public string memberId;
       @AuraEnabled public string memberName;
       @AuraEnabled public string memberRole;
    }

    /*
* @Description: method to form records
* @param name : Type of recordId
* @return type: list of memberWrapper
* @Developer: Santosh
*/    
    @AuraEnabled
    public static list<memberWrapper> getContactPersonWrapp(String recordId){
        Set<String> IdSet = new Set<String>();
        IdSet.add(recordId);
        
        list<memberWrapper> wrapperList = new list<memberWrapper>();
        Account account = [Select Id, Name From Account where Id =: recordId];
        
        if(account != null){
            memberWrapper wrapper4 = new memberWrapper();
            wrapper4.memberId = account.Id;
            wrapper4.memberName = account.Name;
            wrapper4.memberRole = 'Member';
            system.debug('account wrapper => '+json.serializePretty(wrapper4));
            wrapperList.add(wrapper4);
        }
        
        List<HealthCloudGA__AccountAccountRelation__c> accountAccountRelationList = [SELECT CMA_Account_Name__c, CMA_Status__c,HealthCloudGA__Role__r.Name,HealthCloudGA__RelatedAccount__c from HealthCloudGA__AccountAccountRelation__c where HealthCloudGA__Account__c =:recordId AND CMA_Status__c = 'Active'];
        if(accountAccountRelationList != null){
            for(HealthCloudGA__AccountAccountRelation__c record : accountAccountRelationList){
                memberWrapper wrapper1 = new memberWrapper();
                IdSet.add(record.HealthCloudGA__RelatedAccount__c);
                wrapper1.memberId = record.HealthCloudGA__RelatedAccount__c;
                wrapper1.memberName = record.CMA_Account_Name__c;
                wrapper1.memberRole = record.HealthCloudGA__Role__r.Name;
                wrapperList.add(wrapper1);
            }
          //  system.debug('accountAccountRelationList wrapper => '+JSON.serializePretty(wrapperList));
        }
        
        Set<Id> conId = new Set<Id>();
        List<HealthCloudGA__ContactContactRelation__c> contactContactRelationList = [SELECT id,CMA_Contact_Name__c,HealthCloudGA__Role__r.Name,HealthCloudGA__Contact__r.AccountId, HealthCloudGA__Contact__c FROM HealthCloudGA__ContactContactRelation__c WHERE Account__c =: recordId];
        if(contactContactRelationList != null){
            for(HealthCloudGA__ContactContactRelation__c contactRecord : contactContactRelationList){
                 memberWrapper wrapper2 = new memberWrapper();
                conId.add(contactRecord.HealthCloudGA__Contact__c);
                wrapper2.memberId = contactRecord.HealthCloudGA__Contact__r.AccountId;
                wrapper2.memberName = contactRecord.CMA_Contact_Name__c;
                wrapper2.memberRole = contactRecord.HealthCloudGA__Role__r.Name;
                wrapperList.add(wrapper2);
            }
         //   system.debug('contactContactRelationList wrapper => '+json.serializePretty(wrapperList));
        }
        
        if(wrapperList != null){
            system.debug('final wrapper => '+json.serializePretty(wrapperList));
            return wrapperList;
        }
        else{
            return null;
        }
        
    }
}