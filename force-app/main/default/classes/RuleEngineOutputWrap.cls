/**
@Description : Output Wrapper which contains Record and its output values passed from Rule Engine 
@Author : KPMG - KGS
@Code Created : October 2021
**/
public class RuleEngineOutputWrap {
    @AuraEnabled @InvocableVariable(label='sObject Output from Input Data List')
    public sObject objectData;
    @AuraEnabled @InvocableVariable(label='Output Wrapper with Values') 
    public List<RuleEngineOutputData> outputValues;
}