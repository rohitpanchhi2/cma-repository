/*
Description : Class for Letter Generation Screen           
*Author : KPMG
*Code Created Date : 23th September 2021
*/

public class CMA_IntractionHistoryController {
    
    /*@auraEnabled
public static list<CMA_Verification__c> getAllVerificationRecord(string recordId){
return [Select Id, Name, CMA_Authorized_Party__c, CMA_Communication_Type__c, CMA_Member_Id__c, CMA_Member_Id__r.Name, CMA_Role__c, CMA_Status__c, createddate from CMA_Verification__c where CMA_Member_Id__c =: recordId];
}*/

    /*
* @Description: method for fetching Verification records
* @param name : account id, search key, dates
* @return type: List of Verification record
* @Developer: Brijesh Kumar
*
* Updated 11/30/2021 by John Szurley - Optimized Code
*/    
    @auraEnabled
    public static list<CMA_Verification__c> getAllVerificationRecordOnSearch(string recordId, String keyword, Date startDate, Date endDate){
        
        String sSOQL = 'Select Id, Name, CMA_Authorized_Party__c, CMA_Communication_Type__c, CMA_Member_Id__c, CMA_Member_Id__r.Name, CMA_Role__c, CMA_Status__c, createddate from CMA_Verification__c where CMA_Member_Id__c =: recordId';

        system.debug('testttt----  '+recordId + ' '+ keyword + ' ' + startDate + ' ' + endDate );
        
        if(keyword != 'All'){
            sSOQL += ' and CMA_Communication_Type__c =:keyword';
        }
        if(endDate <> null && startDate == null){
            system.debug('in end date');
            sSOQL += ' and CreatedDate<=:endDate';
        }
        if(endDate == null && startDate <> null){
            system.debug('in start date');
            sSOQL += ' and CreatedDate>=:startDate';
        }
        if(endDate <> null && startDate <> null){
            system.debug('in start and end date');
            sSOQL += ' and CreatedDate>=:startDate and CreatedDate<=:endDate';
        }

        return Database.query(sSOQL);
    }
}