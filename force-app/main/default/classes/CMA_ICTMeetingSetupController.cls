/**
@Description : Controller for ICT Meeting Setup LWC Component
@Author : KPMG - KGS
@Code Created Date : Dec 2021
@Test Class: 
**/
public class CMA_ICTMeetingSetupController {    
    
    @AuraEnabled(cacheable = true)
    public static String getMeetingDetails(String recordId){
        Map<String, String> returnMap = new Map<String,String>(); 
        Case caseData = [SELECT Id, Subject, CaseNumber, AccountId, Account.Name FROM Case WHERE Id=: recordId LIMIT 1];
        
        List<Campaign> campaignList = [SELECT Id, Name, CMA_Meeting_Date_Time__c, (SELECT Id, Name, CreatedDate, CMA_Care_Team_Member__r.Name, CMA_Contact__r.Name, CMA_Role__c, CMA_Type__c, CMA_Status__c FROM Care_Plan_Campaign_Members__r) 
                                       FROM Campaign WHERE CMA_Care_Plan__c =: recordId AND IsActive = true];
        
        List<PickListWrap> pickListCaseRoles = getCaseTeamRoles();
        returnMap.put('CaseObj', JSON.serialize(caseData));
        returnMap.put('CampaignList', JSON.serialize(campaignList));
        returnMap.put('CaseTeamRoles', JSON.serialize(pickListCaseRoles));
        return JSON.serialize(returnMap);   
    }
    
    public static List<PickListWrap> getCaseTeamRoles(){
        List<PickListWrap> pickListData = new List<PickListWrap>();
        for(CaseTeamRole roleData: [Select Id, Name FROM CaseTeamRole]){
            PickListWrap obj = new PickListWrap();
            obj.label = roleData.Name;
            obj.value = roleData.Name;
            pickListData.add(obj);
        }
        return pickListData;
    }
    
    @AuraEnabled
    public static List<CaseTeamMember> getCaseTeamMembers(String caseId){
        return [Select Id, MemberId, Member.Name, ParentId, TeamRole.Name FROM CaseTeamMember WHERE ParentId =:caseId];
    }
    
    @AuraEnabled
    public static sObject getRecordName(Id recordId){
        try{
            String sObjectName =   recordId.getSObjectType().getDescribe().getName();
            String querySOQL = 'SELECT Name FROM '+sObjectName+' WHERE Id =:recordId';
            return Database.query(querySOQL);
        } catch(Exception e) {
            System.debug(e.getMessage());
            throw new AuraHandledException('Exception Occured: '+e.getMessage());
        } 
    }
    
    @AuraEnabled
    public static Boolean insertCampaignMembersCP(List<CMA_Care_Plan_Campaign_Member__c> campaignMemCPList){
        System.debug('campaignMemCPLis'+campaignMemCPList);
        try{
            insert campaignMemCPList;
            return true;   
        } catch (Exception e) {
            System.debug(e.getMessage());
          	//SystemLogUtils.insertSystemLogs(e);
            throw new AuraHandledException('Exception Occured: '+e.getMessage());
        }        
    }
    
    public class PickListWrap {
        String label;
        String value;        
    }                                                                                                     
    
}