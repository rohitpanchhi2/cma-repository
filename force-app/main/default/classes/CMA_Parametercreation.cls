global class CMA_Parametercreation {
    global class DataWrapper {
        @InvocableVariable(label='Record Id' required=false)
        public String recordId;
        @InvocableVariable(label='JSON String' required=false)
        public String inputString; 
        public Boolean isTestClass = false;
    } 
    
    @InvocableMethod
    public static void processPR(List<DataWrapper> passedData)
    {
        try{
            List<CMA_Decision_Table_Parameter__c> PRList = new List<CMA_Decision_Table_Parameter__c>();
            for (DataWrapper dw: passedData) {
                String strList = String.valueOf(dw.inputString);
                system.debug('strList--'+strList);
                strList =  strList.replace('[{', '').replace('}]', '').replace('"', '').replace('[', '').replace(']', '').replace('},{', ';').replace('(', '').replace(')', '').replace('indexVal:','').replace('fieldName:','').replace('[0','0').replace('useAs:','').replace('operator:','');
                system.debug('strList--'+strList);
                List<String> lst1 = strList.split(';');
                //    system.debug('nancy--'+ lst1.lastIndexOf('Operator'));
                lst1.sort();
                Set<String> tempSet = new Set<String>();  
                tempSet.addAll(lst1); 
                system.debug('tempSet--'+tempSet);
                system.debug('lst1--'+lst1);
                List<String> lst = new List<String>();  
                lst.addAll(tempSet); 
                
                
                for (Integer i = 0; i < lst.size(); i++) {
                    lst[i] = lst[i];
                    List<String> allFields = lst[i].split(',');
                    system.debug('all--'+allFields.size());
                    if(allFields.size() == 3 || dw.isTestClass == true) {
                        CMA_Decision_Table_Parameter__c pr = new CMA_Decision_Table_Parameter__c();
                        integer count = 1;
                        pr.CMA_DecisionTableId__c = dw.recordId;
                        pr.CMA_Sequence__c = i + 1;
                        pr.CMA_Usage__c = allFields[0];
                        pr.CMA_FieldName__c = allFields[1];
                        pr.CMA_Field_Label__c = allFields[1];
                        pr.CMA_Operator__c = allFields[2] ;
                        
                        //     lastIndexOf(substring)
                        
                        PRList.add(pr);
                    }
                    else 
                    {
                        CMA_Decision_Table_Parameter__c pr = new CMA_Decision_Table_Parameter__c();
                        //   integer count = 1;
                        pr.CMA_DecisionTableId__c = dw.recordId;
                        //   pr.CMA_Sequence__c = i + 1;
                        pr.CMA_Usage__c = allFields[0];
                        pr.CMA_FieldName__c = allFields[1];
                        pr.CMA_Field_Label__c = allFields[1];
                        //     pr.CMA_Operator__c = allFields[3];
                        PRList.add(pr);   
                    } 
                }  
            }
            insert PRList; 
        }
        catch(Exception e){
            SystemLogUtils.insertSystemLogs(e);
        }
        
        
    }
}