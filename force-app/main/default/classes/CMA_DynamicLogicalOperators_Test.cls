/*
Description : Test class for CMA_DynamicLogicalOperators           
*Author : KPMG
*Code Created Date : 2nd November 2021
*/
@isTest
public class CMA_DynamicLogicalOperators_Test {
    
    /*
		* @Description: test method for evalOperation
		* @param name : NA
		* @return type: NA
		* @Developer: Aishwarya 
	*/
    @isTest
    public static void evalOperationTest(){
        test.startTest();
        String value1 = 'a';
        String value2 = 'b';
        CMA_DynamicLogicalOperators.evalOperation(value1,'Equals',value2);
        CMA_DynamicLogicalOperators.evalOperation(value1,'Not Equals',value2);
        CMA_DynamicLogicalOperators.evalOperation(value1,'Greater Than',value2);
        CMA_DynamicLogicalOperators.evalOperation(value1,'Less Or Equal',value2);
        CMA_DynamicLogicalOperators.evalOperation(value1,'Greater Or Equal',value2);
        CMA_DynamicLogicalOperators.evalOperation(value1,'Less Than',value2);
        CMA_DynamicLogicalOperators.evalOperation(value1,'ExistsIn',value2);
        CMA_DynamicLogicalOperators.evalOperation(value1,'Does Not Exists In',value2);
        
        Decimal intValue1 = 1;
        Decimal intValue2 = 2;
        CMA_DynamicLogicalOperators.evalOperation(intValue1,'Equals',intValue2);
        CMA_DynamicLogicalOperators.evalOperation(intValue1,'Not Equals',intValue2);
        CMA_DynamicLogicalOperators.evalOperation(intValue1,'Greater Than',intValue2);
        CMA_DynamicLogicalOperators.evalOperation(intValue1,'Less Or Equal',intValue2);
        CMA_DynamicLogicalOperators.evalOperation(intValue1,'Greater Or Equal',intValue2);
        CMA_DynamicLogicalOperators.evalOperation(intValue1,'Less Than',intValue2);
        CMA_DynamicLogicalOperators.evalOperation(intValue1,'ExistsIn',intValue2);
        CMA_DynamicLogicalOperators.evalOperation(intValue1,'Does Not Exists In',intValue2);
        
        DateTime dateValue1 = system.now();
        DateTime dateValue2 = system.now()+2;
        CMA_DynamicLogicalOperators.evalOperation(dateValue1,'Equals',dateValue2);
        CMA_DynamicLogicalOperators.evalOperation(dateValue1,'Not Equals',dateValue2);
        CMA_DynamicLogicalOperators.evalOperation(dateValue1,'Greater Than',dateValue2);
        CMA_DynamicLogicalOperators.evalOperation(dateValue1,'Less Or Equal',dateValue2);
        CMA_DynamicLogicalOperators.evalOperation(dateValue1,'Greater Or Equal',dateValue2);
        CMA_DynamicLogicalOperators.evalOperation(dateValue1,'Less Than',dateValue2);
        CMA_DynamicLogicalOperators.evalOperation(dateValue1,'ExistsIn',dateValue2);
        CMA_DynamicLogicalOperators.evalOperation(dateValue1,'Does Not Exists In',dateValue2);
        test.stopTest();
    }

}