/**
@Description : Class for Rule Engine Input Data to create Record Set
@Author : KPMG - KGS
@Code Created Date : September 2021
@Test Class: CMA_RuleEngineControllerTest
**/
public class CMA_RuleEngineInputData{    
    
    public static Boolean secondLevelFlag = false;
    public static Boolean thirdLevelFlag = false;
    
    /**
	@Description: Method to fetch related parent records and group data records to form a record set 
	@param name : inputDataList - Input Data set from Primary Obj; datasetParameters - DTset Params Records;
	@return type: returns List<RuleEngineDataWrapper> record set on primary rec with related rec
	@Developer: 
	**/
    public static Map<sObject, List<sObject>> fetchAllInputRecords(List<sObject> inputDataList, List<CMA_DecisionTblDatasetParameter__c> datasetParameters, String decisionTableAPI){
        
        CMA_DecisionEngineRelations__mdt[] dtoMetadata = [SELECT DeveloperName, QualifiedApiName, FieldName__c, ParentObj__c, CurrentObj__c, Decision_Table_API_Name__c, Relationship_level__c FROM CMA_DecisionEngineRelations__mdt
                                                       		WHERE Decision_Table_API_Name__c=: decisionTableAPI ];
        List<sObject> relatedDataList = new List<sObject>();
        
        Map<String, Set<String>> objFieldParams = new Map<String,Set<String>>();
        
        for(CMA_DecisionTblDatasetParameter__c datasetParam :datasetParameters){            
            if(objFieldParams.keyset().contains(datasetParam.CMA_DatasetSourceObject__c)){
                objFieldParams.get(datasetParam.CMA_DatasetSourceObject__c).add(datasetParam.CMA_DatasetFieldName__c);
            }
            else{
                objFieldParams.put(datasetParam.CMA_DatasetSourceObject__c, new Set<String>{datasetParam.CMA_DatasetFieldName__c});
            }
        }        
        System.debug('objFieldParams--> '+objFieldParams);
        
        Map<Schema.SObjectType,Set<String>> sourceObjwFields = new Map<Schema.SObjectType,Set<String>>();        
        Decimal maxlevel= 0;
        
        if(!dtoMetadata.isEmpty()){   
            for(CMA_DecisionEngineRelations__mdt decisionMeta: dtoMetadata){            
                Schema.SObjectType sObjType = CMA_RuleEngineController.getGlobalDesValues.get(decisionMeta.CurrentObj__c);
                if(sourceObjwFields.keyset().contains(sObjType)){
                    sourceObjwFields.get(sObjType).add(decisionMeta.FieldName__c);
                }else{
                    sourceObjwFields.put(sObjType, new Set<String>{decisionMeta.FieldName__c});
                }
                
                if(decisionMeta.Relationship_level__c != null && decisionMeta.Relationship_level__c> maxlevel){
                    maxlevel = decisionMeta.Relationship_level__c;
                }
            }       
            System.debug('sourceObjwFields: '+sourceObjwFields);
            System.debug('maxLevel: '+maxlevel);
        }
        
        if(maxlevel == 2){
            secondLevelFlag = true;
        }
        else if(maxlevel == 3){
            secondLevelFlag = true;
            thirdLevelFlag = true;
        }
        
        Map<Id,sObject> secondLevelRecordSet = new Map<Id,sObject>();
        if(secondLevelFlag){
            secondLevelRecordSet = getLevelUpRecords(inputDataList, objFieldParams, dtoMetadata);
        }
        
        Map<Id,sObject> thirdLevelRecordSet = new Map<Id,sObject>();
        if(thirdLevelFlag){
            thirdLevelRecordSet = getLevelUpRecords(secondLevelRecordSet.values(), objFieldParams, dtoMetadata);
        }
        
        /*Map of Input Object with its related Objects*/
        Map<sObject,List<sObject>> mapPrimaryWRelated = new Map<sObject,List<sObject>>();
        
        for(sObject sObjData: inputDataList){
            if(!secondLevelFlag){
                mapPrimaryWRelated.put(sObjData, new List<sObject>());
            }
            
            if(secondLevelFlag){            
                for(String relatedfield: sourceObjwFields.get(sObjData.getSObjectType())){
                    String objId = (Id)sObjData.get(relatedfield);
                    sObject ParentObj = secondLevelRecordSet.get(objId);
                    if(ParentObj != null){
                        if(mapPrimaryWRelated.keyset().contains(sObjData)){
                            mapPrimaryWRelated.get(sObjData).add(ParentObj);
                        }else{
                            mapPrimaryWRelated.put(sObjData, new List<sObject>{ParentObj});
                        }                    
                    }
                }
                System.debug('Result second Level: '+mapPrimaryWRelated.get(sObjData));
                
                if(!mapPrimaryWRelated.isEmpty() && thirdLevelFlag == true ){
                    List<sObject> secondlevelRecList = new List<sObject>(mapPrimaryWRelated.get(sObjData));
                    for(sObject secondLevel: secondlevelRecList){
                        System.debug('secondLevel: '+secondLevel);
                        Set<String> relatedfields = sourceObjwFields.get(secondLevel.getSObjectType());
                        if(relatedfields != null){
                            for(String relatedfield: relatedfields){
                                String objId = (Id)secondLevel.get(relatedfield);
                                sObject secParentObj = thirdLevelRecordSet.get(objId);
                                if(secParentObj != null){
                                    if(mapPrimaryWRelated.keyset().contains(sObjData)){
                                        mapPrimaryWRelated.get(sObjData).add(secParentObj);
                                    }else{
                                        mapPrimaryWRelated.put(sObjData, new List<sObject>{secParentObj});
                                    }
                                    System.debug('Result third level: '+mapPrimaryWRelated.get(sObjData));
                                }
                            }
                        }
                    }
                }
            }
        }
        
        for(sObject primaryData: mapPrimaryWRelated.keyset()){
            System.debug('PrimaryData: '+primaryData);
            System.debug('RelatedData: '+mapPrimaryWRelated.get(primaryData));          
            System.debug('<------------------------------->');
        }
        
        return mapPrimaryWRelated;
    }    
    
    public static Map<Id,sObject> getLevelUpRecords(List<sObject> secondInputData, Map<String,Set<String>> objFieldParams, List<CMA_DecisionEngineRelations__mdt> metaDataRecords){
        
        Set<Id> setOfParentRecordId = new Set<Id>();
        if(!secondInputData.isEmpty()){
            for(sObject sobRec : secondInputData){
               // System.debug('sobRecords '+sobRec);
                for(CMA_DecisionEngineRelations__mdt dtObject : metaDataRecords){
                    if(sobRec.Id.getSObjectType().getDescribe().getName() == dtObject.CurrentObj__c){
                        Map<String, Schema.SObjectField> M_Objects_Schema_Field = CMA_RuleEngineController.getGlobalDesValues.get(dtObject.CurrentObj__c).getDescribe().fields.getMap();
                        for(String a_Field_Name : M_Objects_Schema_Field.keySet() ) {
                            Schema.DescribeFieldResult a_Field_Description = M_Objects_Schema_Field.get( a_Field_Name ).getDescribe();
                            if(dtObject.FieldName__c == a_Field_Description.getName()){
                               // System.debug('field Name: ' +a_Field_Description.getName() +' '+ sobRec.get(a_Field_Description.getName()));
                                if(string.valueOf(sobRec.get(a_Field_Description.getName())) <> null){
                                    setOfParentRecordId.add(string.valueOf(sobRec.get(a_Field_Description.getName())));
                                }
                            }
                        }                        
                    }                    
                }             
            }
        }
        System.debug('setOfParentRecordId: '+setOfParentRecordId);
        
        Map<String,Id> uniqueObjIds = new Map<String,Id>();
        if(!setOfParentRecordId.isEmpty()){
            for(Id recordId: setOfParentRecordId){
                if(! uniqueObjIds.containskey(recordId.getSObjectType().getDescribe().getName())){
                    uniqueObjIds.put(recordId.getSObjectType().getDescribe().getName(),recordId);
                }
            }
        }   
        System.debug('uniqueObjIds: '+uniqueObjIds);
        
        Map<Id,sObject> parentRecordMap = new Map<Id,sObject>();
        List<String> parentQueryList = new List<String>();
        
        if(!setOfParentRecordId.isEmpty()){
            for(Id pId: uniqueObjIds.values()){
                for(CMA_DecisionEngineRelations__mdt dtObject : metaDataRecords){
                    if(pId.getSObjectType().getDescribe().getName() == dtObject.ParentObj__c){
                        for(String strObjName: objFieldParams.keyset()){
                            if(strObjName ==  dtObject.ParentObj__c){                                
                                for(CMA_DecisionEngineRelations__mdt dtObject2 : metaDataRecords){
                                    if(strObjName ==  dtObject2.CurrentObj__c){
                                        objFieldParams.get(strObjName).add(dtObject2.FieldName__c);
                                    }
                                }                                
                                String fieldParamString = String.join(new List<String>(objFieldParams.get(strObjName)),',');
                                String queryForObj = 'SELECT Id,'+fieldParamString+' FROM '+strObjName+' WHERE Id IN: setOfParentRecordId' ;
                                parentQueryList.add(queryForObj);
                            }
                        }
                    }                    
                }
                if(!parentQueryList.isEmpty()){
                    for(String query: parentQueryList){
                        system.debug('query: '+query);
                        List<sobject> currentData = Database.query(query); 
                        if(!currentData.isEmpty()){                         
							System.debug('QueryDataSize: '+currentData.size());
                            parentRecordMap.putAll(new Map<Id,sObject>(currentData));
                            System.debug('parentRecordMap: '+parentRecordMap.size()+' '+parentRecordMap);
                        }
                    }
                    parentQueryList.clear();
                }
            }
        }        
        
        return parentRecordMap;
    }
}