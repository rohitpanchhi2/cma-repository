/**
Description : Test Class for CMA_IntractionHistoryController           
*Author : KPMG
*Code Created Date : 30th September 2021
**/
@isTest
public class CMA_InteractionHistoryControllerTest {
    /*
     * @Description: test method for getAllVerificationRecordOnSearch method
	 * @param name : NA
	 * @return type: NA
	 * @Developer: Aishwarya 
	*/
	@isTest
    public static void testInteractionHistory(){
        test.startTest();
        CMA_Verification__c record = CMA_DataProviderClass.getVerificationRecords();
        String recordId = record.id;
        String keyword = 'All';
        Date startDate = Date.newInstance(2021, 09, 21);
        Date endDate = Date.newInstance(2021, 09, 29);
        CMA_IntractionHistoryController.getAllVerificationRecordOnSearch(recordId, keyword, startDate, endDate);
        String keyword1 = 'key';
        CMA_IntractionHistoryController.getAllVerificationRecordOnSearch(recordId, keyword1, startDate, endDate);
        test.stopTest();
    }    
    
    /*
     * @Description: test method for getAllVerificationRecordOnSearch method
	 * @param name : NA
	 * @return type: NA
	 * @Developer: Aishwarya 
	*/
    @isTest
    public static void testInteractionHistoryWhenStrDateIsNull(){
        test.startTest();
        CMA_Verification__c record = CMA_DataProviderClass.getVerificationRecords();
        String recordId = record.id;
        String keyword = 'All';
        Date startDate = null;
        Date endDate = Date.newInstance(2021, 09, 29);
        CMA_IntractionHistoryController.getAllVerificationRecordOnSearch(recordId, keyword, startDate, endDate);
        String keyword1 = 'key';
        CMA_IntractionHistoryController.getAllVerificationRecordOnSearch(recordId, keyword1, startDate, endDate);
        test.stopTest();
    }
    
    /*
     * @Description: test method for getAllVerificationRecordOnSearch method
	 * @param name : NA
	 * @return type: NA
	 * @Developer: Aishwarya 
	*/
    @isTest
    public static void testInteractionHistoryWhenEndDateIsNull(){
        test.startTest();
        CMA_Verification__c record = CMA_DataProviderClass.getVerificationRecords();
        String recordId = record.id;
        String keyword = 'All';
        Date startDate = Date.newInstance(2021, 09, 29);
        Date endDate = null;
        CMA_IntractionHistoryController.getAllVerificationRecordOnSearch(recordId, keyword, startDate, endDate);
        String keyword1 = 'key';
        CMA_IntractionHistoryController.getAllVerificationRecordOnSearch(recordId, keyword1, startDate, endDate);
        test.stopTest();
    }
    
    /*
     * @Description: test method for getAllVerificationRecordOnSearch method
	 * @param name : NA
	 * @return type: NA
	 * @Developer: Aishwarya 
	*/
    @isTest
    public static void testInteractionHistoryWhenDateIsNull(){
        test.startTest();
        CMA_Verification__c record = CMA_DataProviderClass.getVerificationRecords();
        String recordId = record.id;
        String keyword = 'All';
        Date startDate = null;
        Date endDate = null;
        CMA_IntractionHistoryController.getAllVerificationRecordOnSearch(recordId, keyword, startDate, endDate);
        String keyword1 = 'key';
        CMA_IntractionHistoryController.getAllVerificationRecordOnSearch(recordId, keyword1, startDate, endDate);
        test.stopTest();
    }


}