public class CMA_DTcontroller {
        
        @AuraEnabled
        public static List<String> getAllCustomSObjects(String objName)
         {
            List<String> sourceObjFieldsList = new List<String>();
            List<String> sobjectList = new List<String>();
          //   String objName = 'Account';
            if(objName != null){
               List<FieldDefinition> sourceObjFieldsList1 = [SELECT QualifiedApiName,Label FROM FieldDefinition WHERE EntityDefinition.Label = :objName AND PublisherId != 'System']; 
               // List<FieldDefinition> sourceObjFieldsList1 = [SELECT QualifiedApiName FROM FieldDefinition];
                for(FieldDefinition Fd : sourceObjFieldsList1){
                   sourceObjFieldsList.add(Fd.Label);
                }
            }
             return sourceObjFieldsList; 
        }
        
        @AuraEnabled
        public static List<String> getPickListValuesIntoList(){
            List<String> pickListValuesList= new List<String>();
            Schema.DescribeFieldResult fieldResult = CMA_Decision_Table_Parameter__c.CMA_Usage__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                pickListValuesList.add(pickListVal.getLabel());
            }    
            return pickListValuesList;
        }
        
        @AuraEnabled
        public static List<String> getPickListValuesIntoList1(){
            List<String> pickListValuesList1= new List<String>();
            Schema.DescribeFieldResult fieldResult = CMA_Decision_Table_Parameter__c.CMA_Operator__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                pickListValuesList1.add(pickListVal.getLabel());
            }    
            return pickListValuesList1;
        }
        @auraEnabled 
        public static Map<String, List<String>> getObjectFields(string selectedObj) {
            Map<String, List<String>> fieldNames = new Map<String, List<String>>();
            List<String> pickListValuesList2= new List<String>();
            Schema.DescribeFieldResult fieldResult = CMA_Decision_Table_Parameter__c.CMA_Operator__c.getDescribe();
            if(selectedObj == 'INPUT'){
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                pickListValuesList2.add(pickListVal.getLabel());
            }
                    if(!pickListValuesList2.isEmpty()){
                fieldNames.put(selectedObj, pickListValuesList2);
            }
            }
            system.debug('fieldNames   '+fieldNames);
            return fieldNames;
        }  
    }