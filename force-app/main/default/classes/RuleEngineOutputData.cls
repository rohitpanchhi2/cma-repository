public class RuleEngineOutputData {
    @AuraEnabled @InvocableVariable public Double outputInt;
    @AuraEnabled @InvocableVariable public String outputString;
    @AuraEnabled @InvocableVariable public Boolean outputBoolean;
    @AuraEnabled @InvocableVariable public Datetime outputDateTime;
}