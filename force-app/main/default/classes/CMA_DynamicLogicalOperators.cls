public class CMA_DynamicLogicalOperators {
    
    public static Boolean evalOperation(String value1, String operator, String value2){
        switch on operator{
            when 'Equals'{
                if(value1 == value2) return true;
            }
            when 'Not Equals'{
                if(value1 != value2) return true;
            }
            when 'Greater Or Equal'{
                if(value1 >= value2) return true;    
            }
            when 'Greater Than'{
                if(value1 > value2) return true;   
            }
            when 'Less Or Equal'{
                if(value1 <= value2) return true;   
            }
            when 'Less Than'{
                if(value1 < value2) return true;
            }
            when 'ExistsIn'{
                if(value2.containsIgnoreCase(value1)) return true;
            }
            when 'Does Not Exists In'{
                if(!value2.containsIgnoreCase(value1)) return true;
            }
        }
        return false;
    }
    
    public static Boolean evalOperation(Decimal value1, String operator, Decimal value2){
        switch on operator{
            when 'Equals'{
                if(value1 == value2) return true;
            }
            when 'Not Equals'{
                if(value1 != value2) return true;
            }
            when 'Greater Or Equal'{
                if(value1 >= value2) return true;    
            }
            when 'Greater Than'{
                if(value1 > value2) return true;   
            }
            when 'Less Or Equal'{
                if(value1 <= value2) return true;   
            }
            when 'Less Than'{
                if(value1 < value2) return true;
            }
        }
        return false;
    }
    
    public static Boolean evalOperation(DateTime value1, String operator, DateTime value2){
        switch on operator{
            when 'Equals'{
                if(value1 == value2) return true;
            }
            when 'Not Equals'{
                if(value1 != value2) return true;
            }
            when 'Greater Or Equal'{
                if(value1 >= value2) return true;    
            }
            when 'Greater Than'{
                if(value1 > value2) return true;   
            }
            when 'Less Or Equal'{
                if(value1 <= value2) return true;   
            }
            when 'Less Than'{
                if(value1 < value2) return true;
            }
        }
        return false;
    }

}