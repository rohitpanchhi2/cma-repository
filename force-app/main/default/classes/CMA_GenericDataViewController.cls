/**
@Description : Controller for Generic Data Table LWC Component
@Author : KPMG - KGS
@Code Created Date : Nov 2021
@Test Class: CMA_GenericDataViewControllerTest
**/
public with sharing class CMA_GenericDataViewController {
    public static final Map<String, Schema.SObjectType> getGlobalDesValues = Schema.getGlobalDescribe();
    public static final Map<String,String> excepObjNames = new Map<String,String>{'Case'=>'CaseNumber','Task'=>'Subject'};
    
    /**
    @Description: Method to fetch Fields from Field Set passed.
    @param name : ObjectName - sObject API Name; fieldSetName - Field set API Name from the sObject;
    @return type: List of fields along with its metadata from the Field set.
    @Developer: santoshrebello@KPMG-KGS
    **/
    public static List<Schema.FieldSetMember> getFieldsFromFieldset(String ObjectName,String fieldSetName) {
        try{
            Schema.DescribeSObjectResult DescribeSObjectResultObj = getGlobalDesValues.get(ObjectName).getDescribe();
            List<Schema.FieldSetMember> listdata = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName).getFields();
            return listdata;
        } catch(Exception ex){
            System.debug(ex.getMessage());
            throw new AuraHandledException('Invalid Object API/Field Set Input');
        }
    }
    
    /**
    @Description: Method to fetch Fields from Field Set passed.
    @param name : strObjectApiName - sObject API Name; strfieldSetName - Field set API Name from the sObject; 
				  recordId - recordId if present on any Record Detail; criteriaField - WHERE clause for the Query;
    @return type: JSON Object consists of Field Metadata, Record List.
    @Developer: santoshrebello@KPMG-KGS
    **/
    @AuraEnabled
    public static String getFieldsAndRecords(String recordId, String strObjectApiName, String strfieldSetName, String criteriaField){
        Map<String, String> returnMap = new Map<String,String>();
        
        List<String> fieldList = new List<String>();
        Map<String,String> fieldAndLabelMap = new Map<String,String>();
       	Map<String,String> fieldAndRefMap = new Map<String,String>();
        Map<String,Boolean> fieldAndEditMap = new Map<String,Boolean>();
        
        if(!String.isEmpty(strObjectApiName) && !String.isEmpty(strfieldSetName)){
            List<Schema.FieldSetMember> lstFSMember = getFieldsFromFieldset(strObjectApiName.trim(), strfieldSetName.trim());
            
            for(Schema.FieldSetMember fieldSetMemObj : lstFSMember){
                System.debug('fieldSetMemObj: '+fieldSetMemObj);
                fieldList.add(fieldSetMemObj.getFieldPath());
                fieldAndLabelMap.put(fieldSetMemObj.getFieldPath(), fieldSetMemObj.getLabel());
                
                if( fieldSetMemObj.getType()== Schema.DisplayType.Reference){
                    String refField = null;
                    Schema.DescribeFieldResult fieldData = fieldSetMemObj.getSObjectField().getDescribe();
                    refField= fieldData.getRelationshipName();
                    fieldAndRefMap.put(fieldSetMemObj.getFieldPath(),refField);
                    List<Schema.sObjectType> parentObj = fieldData.getReferenceTo();
                  	String objName = parentObj[0].getDescribe().getName();
                    List<String> objectNames = new List<String>(excepObjNames.keySet());
                    if(excepObjNames.keySet().contains(objName)){
                        refField += '.'+ excepObjNames.get(objName);
                    } else {
                        refField += '.Name';
                    }
                    System.debug('Reference Field'+refField);
                    fieldList.add(refField);
                } 
                else{
                    Schema.DescribeFieldResult fieldData = fieldSetMemObj.getSObjectField().getDescribe();
                    fieldAndEditMap.put(fieldData.getName(),fieldData.isUpdateable());
                }
            }
            
			String fieldParamString = String.join(fieldList,',');
            String query;
            String userId = UserInfo.getUserId();
            if(String.isEmpty(criteriaField) || String.isBlank(criteriaField)){
                query = 'SELECT '+fieldParamString+' FROM '+strObjectApiName;
            }else {
                query = 'SELECT '+fieldParamString+' FROM '+strObjectApiName+' WHERE '+criteriaField;
            }            
            System.debug('Query -->'+query);
            List<SObject> lstRecords = Database.query(query);
            
         	returnMap.put('Field_List', JSON.serialize(lstFSMember));
            returnMap.put('Record_List', JSON.serialize(lstRecords));
            returnMap.put('Field_Ref_Map', JSON.serialize(fieldAndRefMap));
            returnMap.put('Exception_Objs_Map', JSON.serialize(excepObjNames));
            returnMap.put('Field_Editable_Map', JSON.serialize(fieldAndEditMap));
            return JSON.serialize(returnMap);
        }
        return null;
    }
    
    /**
    @Description: Method to Update the Records updated in the Data Table
    @param name : listUpdate - list of updated records; strObjectApiName - sObject API Name;
    @return type: Success/Error Message
    @Developer: santoshrebello@KPMG-KGS
    **/
    @AuraEnabled
    public static string updateObjectData(List<sObject> listUpdate, String strObjectApiName){
        System.debug('listUpdate'+listUpdate);
       try {
            update listUpdate;
            return 'Success: contacts updated successfully';
        }
        catch (Exception e) {
            System.debug(e.getMessage());
            throw new AuraHandledException('Exception Occured: '+e.getMessage());
        }
    } 
}