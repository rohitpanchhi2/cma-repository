/**
@Description : Controller for Rule Engine which contains logic for forming the Input Data and processes it to provide output 
@Author : KPMG - KGS
@Code Created Date : September 2021
@Test Class: CMA_RuleEngineControllerTest
**/
public class CMA_RuleEngineController{
    
    public static final Map<String, Schema.SObjectType> getGlobalDesValues = Schema.getGlobalDescribe();
    public static final String customValue = 'CUSTOM';
    
    /**
    @Description: Method to Process the Input params and evaluate with the Rules Data and return Output set
    @param name : recordList - Primary Set of records; decisionTableAPI - decision table API name to be evaluated;
    @return type: returns Map of Id from Primary set and all Output fields with values
    @Developer: santoshrebello@KPMG-KGS
    **/
    public static List<RuleEngineOutputWrap> invokeDTwDataSet(List<sObject> recordList, String decisionTableAPI){
        
        CMA_DecisionTable__c decisionTable = [Select Id, CMA_ConditionCriteria__c, CMA_ConditionType__c, CMA_SourceObject__c, CMA_API_Name__c, CMA_Status__c FROM CMA_DecisionTable__c 
                                                WHERE CMA_API_Name__c =: decisionTableAPI LIMIT 1];
        
        System.debug('Rule Object API: '+decisionTable.CMA_SourceObject__c);
        System.debug('Condition Type: '+decisionTable.CMA_ConditionType__c);
        System.debug('Criteria: '+decisionTable.CMA_ConditionCriteria__c);
        
        /**Fetching API name for Rules obj from DT record and forming dynamic query**/
        Schema.SObjectType ruleObj = getGlobalDesValues.get(decisionTable.CMA_SourceObject__c);
        String ruleObjQuery = formRulesObjQuery(ruleObj);
        System.debug('ruleObjQuery--> '+ruleObjQuery);        
        List<sObject> ruleDataList = Database.query(ruleObjQuery);
        System.debug('Rules Records: '+ruleDataList.size());              
        
        Map<Id,CMA_Decision_Table_Parameter__c> decisionTableParameters = new Map<Id,CMA_Decision_Table_Parameter__c>([SELECT Id, CMA_FieldName__c, CMA_Operator__c, CMA_Sequence__c, CMA_DecisionTableId__c, CMA_Usage__c 
                                                                                                                       FROM CMA_Decision_Table_Parameter__c WHERE CMA_DecisionTableId__c =: decisionTable.Id]);
        Map<String, Schema.SObjectField> ruleObjFieldsMap = ruleObj.getDescribe().fields.getMap();
        List<Schema.DescribeFieldResult> outputFieldParamList = new List<Schema.DescribeFieldResult>();
        Map<String,String> fieldOperatorMap = new Map<String,String>();        
        Map<String,String> fieldSequence = new Map<String,String>();
        
        for(CMA_Decision_Table_Parameter__c decTabParamData: decisionTableParameters.values()){
            //System.debug('Fields: '+decTabParamData.CMA_FieldName__c);
            if (decTabParamData.CMA_Usage__c =='OUTPUT'){
                Schema.DescribeFieldResult fieldResult = ruleObjFieldsMap.get(decTabParamData.CMA_FieldName__c).getDescribe();
                outputFieldParamList.add(fieldResult);
            }
            else if (decTabParamData.CMA_Usage__c =='INPUT'){
                fieldOperatorMap.put(decTabParamData.CMA_FieldName__c, decTabParamData.CMA_Operator__c);
                fieldSequence.put(decTabParamData.CMA_FieldName__c,String.valueOf(decTabParamData.CMA_Sequence__c));
            }
        }
        System.debug('outputFieldParamList: '+outputFieldParamList);
        System.debug('fieldOperatorMap: '+fieldOperatorMap);
        System.debug('fieldSequence: '+fieldSequence);
        
        List<CMA_DecisionTblDatasetParameter__c> datasetParameters = [SELECT Id, CMA_DatasetFieldName__c, CMA_DatasetSourceObject__c, CMA_DecisionTableDatasetLinkId__c, DecisionTableParameterId__c, 
                                                                      DecisionTableParameterId__r.CMA_FieldName__c FROM CMA_DecisionTblDatasetParameter__c 
                                                                      WHERE CMA_DecisionTableDatasetLinkId__r.CMA_DecisionTableId__c =: decisionTable.Id  //WHERE CMA_DecisionTableDatasetLinkId__c = 'aAg5f0000008Yc5CAE'
                                                                      AND DecisionTableParameterId__c =: decisionTableParameters.keySet()];
        
        /*Map of Source Obj with its related fields --- Case =>(Active,Status)*/
        Map<Schema.SObjectType,List<String>> objFieldParams = new Map<Schema.SObjectType,List<String>>();       
        /*List of all Output Fields*/
        List<String> fieldParams = new List<String>();
        /*Output Map of Id-from input recordList with outcome value*/
        Map<Id, List<String>> dataOutputMap = new Map<Id, List<String>>();
        /*Map of Field Names from DT Params & DTset Params*/
        Map<String,String> dtSetDtTableFieldMap = new Map<String,String>();
        
        for(CMA_DecisionTblDatasetParameter__c datasetParam :datasetParameters){
            if(datasetParam.DecisionTableParameterId__c != null){
                dtSetDtTableFieldMap.put(datasetParam.CMA_DatasetFieldName__c, datasetParam.DecisionTableParameterId__r.CMA_FieldName__c);
            }
            
            Schema.SObjectType sObjType = getGlobalDesValues.get(datasetParam.CMA_DatasetSourceObject__c);
            if(objFieldParams.keyset().contains(sObjType)){
                objFieldParams.get(sObjType).add(datasetParam.CMA_DatasetFieldName__c);
            }
            else{
                objFieldParams.put(sObjType, new List<String>{datasetParam.CMA_DatasetFieldName__c});
            }
                        
            fieldParams.add(datasetParam.CMA_DatasetFieldName__c);
        }
        System.debug('dtSetDtTableFieldMap--> '+dtSetDtTableFieldMap);
        System.debug('objFieldParams--> '+objFieldParams);
        System.debug('fieldParams--> '+fieldParams.size());
        
        Map<String,String> resultMap = new Map<String,String>();
        
        /*Map of sObj with all its field APIs --> for all Objs in Source field*/
        Map<Schema.SObjectType, List<String>> objFieldAPI = new Map<Schema.SObjectType, List<String>>();
        Map<Schema.SObjectType, Map<String,Schema.DescribeFieldResult>> objFieldSchema = new Map<Schema.SObjectType, Map<String,Schema.DescribeFieldResult>>();        
        
        for(Schema.SObjectType strObjName: objFieldParams.keyset()){
            Map<String, Schema.SObjectField> sObjFieldsMap = strObjName.getDescribe().fields.getMap();          
            List<String> fieldAPIs = new List<String>();
            for(String sField: sObjFieldsMap.keyset()){
                fieldAPIs.add(String.valueOf(sObjFieldsMap.get(sField)));
            }            
            objFieldAPI.put(strObjName,fieldAPIs);
            
            for(String strFieldParam: objFieldParams.get(strObjName)){
                if(fieldAPIs.contains(strFieldParam)){
                    Schema.DescribeFieldResult FieldType = sObjFieldsMap.get(strFieldParam).getDescribe();
                    if(objFieldSchema.containskey(strObjName)){
                        Map<String,Schema.DescribeFieldResult> getMapData = objFieldSchema.get(strObjName);
                        getMapData.put(strFieldParam, FieldType);
                    }else{
                        objFieldSchema.put(strObjName,new Map<String,Schema.DescribeFieldResult>{strFieldParam => FieldType});
                    }
                }
            }           
        }
        
        /*Fetch related records to form the Input Record Set*/
         Map<sObject, List<sObject>> dataInputMap = CMA_RuleEngineInputData.fetchAllInputRecords(recordList, datasetParameters, decisionTableAPI);
        
        /*Initialize Output Wrapper*/
        List<RuleEngineOutputWrap> outputRuleEngList = new List<RuleEngineOutputWrap>();
        
        /*Iterating over Input Record Set*/
        for(sObject dataInputObj: dataInputMap.keyset()){
            /*Iterating over Rule records*/
            List<RuleEngineOutputData> outputValuesList = new List<RuleEngineOutputData>();
            
            for(sObject ruleData: ruleDataList){
                Integer counterValidate = 0;
                /*Iterating over Map created*/
                for(Schema.SObjectType strObjParam: objFieldParams.keyset()){
                    System.debug('strObjParam--> '+strObjParam);
                    sObject dataObjfromMap;
                    if(dataInputObj.getSObjectType() == strObjParam){
                        dataObjfromMap = dataInputObj;
                    }else{
                        dataObjfromMap = fetchSobjectData(strObjParam, dataInputMap.get(dataInputObj));
                    }
                    
                    for(String strFieldParam: objFieldParams.get(strObjParam)){
                        System.debug('strFieldParam--> '+strFieldParam);
                     // System.debug('Condition Check: '+objFieldAPI.get(strObjParam).contains(strFieldParam));                        
                        /*Logic for Comaprison of Rule rec with Record set rec*/
                        if(objFieldAPI.get(strObjParam).contains(strFieldParam)){            
                            if(dataObjfromMap != null){                            
                                Schema.DescribeFieldResult FieldType = objFieldSchema.get(strObjParam).get(strFieldParam);
                                
                                if(FieldType.getType() == Schema.DisplayType.DOUBLE || FieldType.getType() == Schema.DisplayType.INTEGER || FieldType.getType() == Schema.DisplayType.LONG){
                                    if(CMA_DynamicLogicalOperators.evalOperation((Decimal)ruleData.get(dtSetDtTableFieldMap.get(strFieldParam)), fieldOperatorMap.get(dtSetDtTableFieldMap.get(strFieldParam)), (Decimal)dataObjfromMap.get(strFieldParam))){
                                        counterValidate +=1;
                                        if(decisionTable.CMA_ConditionType__c == customValue){
                                            resultMap.put(fieldSequence.get(dtSetDtTableFieldMap.get(strFieldParam)),'true');
                                        }
                                    }
                                    else{
                                        if(decisionTable.CMA_ConditionType__c == customValue){
                                            resultMap.put(fieldSequence.get(dtSetDtTableFieldMap.get(strFieldParam)) ,'false');
                                        }
                                    }
                                }
                                
                                else if(FieldType.getType() == Schema.DisplayType.DATE || FieldType.getType() == Schema.DisplayType.DATETIME){
                                    if(CMA_DynamicLogicalOperators.evalOperation((DateTime)ruleData.get(dtSetDtTableFieldMap.get(strFieldParam)), fieldOperatorMap.get(dtSetDtTableFieldMap.get(strFieldParam)),(DateTime)dataObjfromMap.get(strFieldParam))){
                                        counterValidate +=1;
                                        if(decisionTable.CMA_ConditionType__c == customValue){
                                            resultMap.put(fieldSequence.get(dtSetDtTableFieldMap.get(strFieldParam)),'true');
                                        }
                                    }
                                    else{
                                        if(decisionTable.CMA_ConditionType__c == customValue){
                                            resultMap.put(fieldSequence.get(dtSetDtTableFieldMap.get(strFieldParam)) ,'false');
                                        }
                                    }
                                }
                                
                                else if(FieldType.getType() == Schema.DisplayType.BOOLEAN){
                                    if(ruleData.get(dtSetDtTableFieldMap.get(strFieldParam)) == dataObjfromMap.get(strFieldParam)){
                                        counterValidate +=1;
                                        if(decisionTable.CMA_ConditionType__c == customValue){
                                            resultMap.put(fieldSequence.get(dtSetDtTableFieldMap.get(strFieldParam)),'true');
                                        }
                                    }
                                    else{
                                        if(decisionTable.CMA_ConditionType__c == customValue){
                                            resultMap.put(fieldSequence.get(dtSetDtTableFieldMap.get(strFieldParam)) ,'false');
                                        }
                                    }
                                }
                                
                                else{                                    
                                    if(CMA_DynamicLogicalOperators.evalOperation((String)ruleData.get(dtSetDtTableFieldMap.get(strFieldParam)), fieldOperatorMap.get(dtSetDtTableFieldMap.get(strFieldParam)),(String)dataObjfromMap.get(strFieldParam))){
                                        counterValidate +=1;
                                        if(decisionTable.CMA_ConditionType__c == customValue){
                                            resultMap.put(fieldSequence.get(dtSetDtTableFieldMap.get(strFieldParam)),'true');
                                        }
                                    }
                                    else{
                                        if(decisionTable.CMA_ConditionType__c == customValue){
                                            resultMap.put(fieldSequence.get(dtSetDtTableFieldMap.get(strFieldParam)) ,'false');
                                        }
                                    }
                                }
                            }
                            System.debug('counterValidate: '+counterValidate);
                            System.debug('resultMap: '+resultMap);
                        }
                    }
                }
                
                /*Custom Criteria Logic*/
                Boolean finalResultExpression = false;
                if(decisionTable.CMA_ConditionType__c ==  customValue){
                    String booleanExpression = decisionTable.CMA_ConditionCriteria__c;                    
                    if(!resultMap.isEmpty()){                               
                        for(String strVar: resultMap.keyset()){
                            booleanExpression = booleanExpression.replace(strVar, resultMap.get(strVar));
                            system.debug('booleanExpression--> '+booleanExpression);  
                        }
                    }
                    
                    if(booleanExpression != null && decisionTable.CMA_ConditionType__c == customValue)
                    {
                        booleanExpression = booleanExpression.toLowerCase().replaceAll( '\\s+', ' ');
                        system.debug('Final Expression: '+booleanExpression);
                        finalResultExpression = CMA_BooleanExpressionCalculation.evaluateExpression(booleanExpression);
                        system.debug('Expression Result: '+finalResultExpression);
                    }
                    resultMap.clear();
                }
                
                /*Criteria Logic and forming the Output Wrapper*/
                RuleEngineOutputData outputValue = new RuleEngineOutputData();
                Boolean outputflag = false;
                if((decisionTable.CMA_ConditionType__c =='ALL' && counterValidate >= fieldParams.size()) ||
                   (decisionTable.CMA_ConditionType__c =='ANY' && counterValidate >= 1) ||
                   (decisionTable.CMA_ConditionType__c == customValue && finalResultExpression == true))
                {
                    for(Schema.DescribeFieldResult outputParam: outputFieldParamList){
                        System.debug(' PASSED -->'+outputParam.getName()+' '+ruleData.get(outputParam.getName()));                    
                        if(outputParam.getType() == Schema.DisplayType.DOUBLE || outputParam.getType() == Schema.DisplayType.INTEGER || outputParam.getType() == Schema.DisplayType.LONG){ 
                            outputValue.outputInt = (DOUBLE)ruleData.get(outputParam.getName());
                            outputflag = true;
                        }
                        else if(outputParam.getType() == Schema.DisplayType.DATE || outputParam.getType() == Schema.DisplayType.DATETIME){
                            outputValue.outputDateTime = (DATETIME)ruleData.get(outputParam.getName());
                            outputflag = true;
                        }
                        else if(outputParam.getType() == Schema.DisplayType.BOOLEAN){
                            outputValue.outputBoolean = (BOOLEAN)ruleData.get(outputParam.getName());
                            outputflag = true;
                        }
                        else{
                            outputValue.outputString = String.valueof(ruleData.get(outputParam.getName()));
                            outputflag = true;
                        }
                    }
                }
                if(outputflag){
                    outputValuesList.add(outputValue);
                }
            }
            
            if(!outputValuesList.isEmpty()){
                RuleEngineOutputWrap outputObj = new RuleEngineOutputWrap();
                outputObj.objectData = dataInputObj;
                outputObj.outputValues = outputValuesList;
                outputRuleEngList.add(outputObj);
            }            
        }        
        System.debug('Output : '+outputRuleEngList);
        return outputRuleEngList;
    }
    
    /*** Without DataSet ***/
    public static void invokeDT(List<sObject> recordList, String decisionTableAPI){        
        //
    }
    
    /**
    @Description: Method to find required sObject record
    @param name : objApiName - required obj rec from record set; dataInput - Input record set;
    @return type: returns sObject record to compare fields
    @Developer: santoshrebello@KPMG-KGS 
    **/
    public static sObject fetchSobjectData(Schema.SObjectType objApi, List<sObject> dataInput){        
        for(sObject objdata: dataInput){
            if(objdata.getSObjectType() == objApi){
                return objdata;
            }
        }        
        return null;
    }    
    
    /**
    @Description: Method to fetch Field API Names for Obj API passed
    @param name : objApiName - Obj API Name; fieldSetType - type of fields to be fetched;
    @return type: returns List<String> which holds field api names
    @Developer: santoshrebello@KPMG-KGS
    **/
    public static List<String> getsObjfields(Schema.SObjectType objApi, String fieldSetType){        
        List<String> objFieldNames = new List<String>();
        Map<String, Schema.SObjectField> sObjFieldsMap = objApi.getDescribe().fields.getMap();
        
        for(String objFieldKey : sObjFieldsMap.keySet()){
            Schema.DescribeFieldResult objFieldResult = sObjFieldsMap.get(objFieldKey).getDescribe();
            //System.debug('#FieldAPI: '+objFieldResult.getName());
            switch on fieldSetType{
                when 'All'{
                    objFieldNames.add(objFieldResult.getName());
                }                
                when 'Custom'{
                    if(objFieldResult.isCustom() || 'Id'.equalsIgnoreCase(objFieldResult.getName())){
                        objFieldNames.add(objFieldResult.getName());
                    }                    
                }
                when 'Relationship'{
                    if(objFieldResult.getRelationshipName() != null){
                        objFieldNames.add(objFieldResult.getName());
                    }
                }
                when null{
                    objFieldNames.add(objFieldKey);
                }
            }                      
        }        
        return objFieldNames;
    }
    
    /**
    @Description: Method to form Dynamic Query for Rule Obj with all its Custom Fields
    @param name : ruleObjApi - Rule Obj API Name;
    @return type: returns String which holds the Dynamic Query
    @Developer: santoshrebello@KPMG-KGS
    **/
    public static String formRulesObjQuery(Schema.SObjectType ruleObjApi){        
        List<String> objCustomFieldsList = getsObjfields(ruleObjApi,'All');
        String customFields = String.join(objCustomFieldsList,',');
        String querydata ='SELECT '+customFields+' FROM '+ruleObjApi;
        return querydata;
    }
}