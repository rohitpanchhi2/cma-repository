@isTest
public class CMA_FetchObjectNameTest {
    @isTest
    public static void getAllObjectsTest(){
        test.startTest();
        CMA_FetchObjectName.getAllObjects();
        CMA_FetchObjectName.getFieldsOfDecisionTblPara('id');
        CMA_FetchObjectName.getObjectFields('Case');
        test.stopTest();
    }
    
    @isTest
    public static void updatedecisionTblDatasetParaTest(){
        test.startTest();
        List<CMA_FetchObjectName.DataWrapper> finalList = new List<CMA_FetchObjectName.DataWrapper>();
        CMA_FetchObjectName.DataWrapper  obj = new CMA_FetchObjectName.DataWrapper();
        
        CMA_DecisionTable__c decisionObj = new CMA_DecisionTable__c();
        decisionObj.CMA_SourceObject__c = 'Case';
        decisionObj.CMA_Primary_Object__c = 'CMA_Call_Rule__c';
        insert decisionObj;
        
        CMA_Decision_Table_Parameter__c decision = new CMA_Decision_Table_Parameter__c();
        decision.CMA_Usage__c = 'OUTPUT';
        decision.CMA_DecisionTableId__c = decisionObj.id;
        decision.CMA_FieldName__c = 'Id';
        insert decision;
        
        CMA_DecisionTableDataSetLink__c dataset = new CMA_DecisionTableDataSetLink__c();
        dataset.CMA_DecisionTableId__c = decisionObj.id;
        insert dataset;
        
        CMA_DecisionTblDatasetParameter__c decisionTblDatasetpara = new CMA_DecisionTblDatasetParameter__c();
        decisionTblDatasetpara.CMA_DecisionTableDatasetLinkId__c = dataset.id;
        decisionTblDatasetpara.DecisionTableParameterId__c = decision.id;
        decisionTblDatasetpara.CMA_DatasetSourceObject__c = 'Case';
        decisionTblDatasetpara.CMA_DatasetFieldName__c = 'CMA_Call_Attempt_Number__c';
        insert decisionTblDatasetpara;
        
        List<Id> decisionTblDatasetParaId = new List<Id>();
        decisionTblDatasetParaId.add(dataset.id);
        obj.decisionTblDatasetParaId = decisionTblDatasetParaId;
        obj.inputString = '["[{\"fieldname\":\"CMA_Call_Attempt_Number__c\",\"obj\":\"CaseStatus\"}]"]';
        finalList.add(obj);
        
        CMA_FetchObjectName.updatedecisionTblDatasetPara(finalList);
     //   CMA_Parametercreation.processPR(finalList);
        test.stopTest();
    }
}