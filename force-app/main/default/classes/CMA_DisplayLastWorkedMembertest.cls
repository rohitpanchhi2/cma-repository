@isTest
public class CMA_DisplayLastWorkedMembertest {
    @isTest
    public static void getAllTest(){
        Account a = CMA_DataProviderClass.getAccount();
        insert a;
        Account acc = new Account(Name='Test2');
        insert acc;
        Account acc1 = new Account(Name='Test3');
        insert acc1;
        Account acc2 = new Account(Name='Test4s');
        insert acc2;
        
        List<Case> caseList = new List<Case>();
        Case c1 = new Case(AccountId = a.id,Subject = 'Test Case1');
        Case c2 = new Case(AccountId = acc.id,Subject = 'Test Case2');
        caseList.add(c1);
        caseList.add(c2);
        insert caseList;
        
        List<Member_Note__c> MemList = new List<Member_Note__c>();
        Member_Note__c m1 = new Member_Note__c(Account__c = a.id,Category__c = 'Outreach');
        Member_Note__c m2 = new Member_Note__c(Account__c = acc1.id,Category__c = 'Outreach');
        MemList.add(m1);
        MemList.add(m2);
        insert MemList;
      
        Contact contact = CMA_DataProviderClass.getContact();
        contact.AccountId = a.id;
        insert contact;
        
        Task t1 = new Task(WhoId = contact.id,Subject = 'Call Notes1');
        insert t1;
         
        test.startTest();            
        CMA_DisplayLastWorkedMemberController.getMemberDetails();
        test.stopTest();
}
}