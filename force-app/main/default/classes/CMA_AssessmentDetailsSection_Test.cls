/*
Description : Test class for CMA_AssessmentDetailsSection           
*Author : KPMG
*Code Created Date : 27th October 2021
*/
@isTest
public class CMA_AssessmentDetailsSection_Test {
	
    /*
		* @Description: test method for getAssessment
		* @param name : NA
		* @return type: NA
		* @Developer: Aishwarya 
	*/
    @isTest
    public static void testInvokeMethod(){
        test.startTest();
        Account account = CMA_DataProviderClass.getAccount();
        insert account;
        Map<String,Object> input = new Map<String,Object>();
        input.put('id', account.id);
        Map<String,Object> output = new Map<String,Object>();
        output.put('id', account.id);
        Map<String,Object> options = new Map<String,Object>();
        options.put('id', account.id);
        CMA_AssessmentDetailsSection obj = new CMA_AssessmentDetailsSection();
        obj.invokeMethod('getAssessment',input,output,options);
        test.stopTest();
    }
     
    /*
		* @Description: test method for createTaskRecord
		* @param name : NA
		* @return type: NA
		* @Developer: Aishwarya 
	*/
    @isTest
    public static void createTaskRecordTest(){
        test.startTest();
        Account account = CMA_DataProviderClass.getAccount();
        insert account;
        
        Case c = CMA_DataProviderClass.getCase();
        c.AccountId = account.id;
        c.Status = 'Active';
        List<Case> caseList = new List<Case>();
        caseList.add(c);
        insert caseList;
        
        system.debug('caseList ->'+caseList);
        
        Contact contact = CMA_DataProviderClass.getContact();
        contact.AccountId = account.id;
        insert contact;
        
        Map<String,Object> input = new Map<String,Object>();
        input.put('Formula', account.id);
        input.put('ContxtId', account.id);
        input.put('Enrollment Status','Accepted');
        input.put('CallType','Outreach');
        input.put('Call Results','Successful');
        input.put('CallNotesTextArea','Tested ok');
        Map<String,Object> output = new Map<String,Object>();
        output.put('Formula', account.id);
        Map<String,Object> options = new Map<String,Object>();
        options.put('Formula', account.id);
        CMA_AssessmentDetailsSection obj = new CMA_AssessmentDetailsSection();
        obj.invokeMethod('createTaskRecord',input,output,options);
        obj.invokeMethod('test',input,output,options);
        test.stopTest();
    }
}