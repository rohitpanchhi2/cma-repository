/*
Description : Class to display PGI details and call notes details           
*Author : KPMG
*Code Created Date : 29th October 2021
*/
public class CMA_TaskToRelatedCallsController {
    
    /*
        * @Description: method for PGI record
        * @param name : accountId
        * @return type: case list
        * @Developer: Aishwarya 
    */
    @AuraEnabled
    public static List<Case> getPGIDetails(String accountId){
        
        if (accountId.left(3) == '500')
        {
            Case c = [select id, accountid, Account.Name from case where id = :accountId limit 1];
            if (c != null) accountId = c.AccountId;
        }
        
        List<Case> caseList = [SELECT Subject,AccountId, Account.Name,
                               (SELECT Id,Subject,Status,ActivityDate,HealthCloudGA__CarePlanProblem__r.Name,HealthCloudGA__CarePlanGoal__r.Name,
                                HealthCloudGA__CarePlanGoal__r.HealthCloudGA__CarePlanProblem__r.Name, createdby.name, createddate
                                FROM Tasks WHERE  Type ='Intervention' AND Status != 'Completed' ORDER By CreatedDate DESC)                           
                               FROM Case WHERE AccountId =:accountId AND Status ='Active']; 
        if(caseList != null){
            return caseList;
        }
        else{
            return null;
        }
    }
    
    /*
        * @Description: method for getting call notes record
        * @param name : taskId
        * @return type: Member_Note__c list
        * @Developer: Aishwarya 
    */
    @AuraEnabled
    public static List<Member_Note__c>getCallNotesDetails(String taskId){
        List<CMA_Related_Task__c> relatedTask = [SELECT Id, CMA_Member_Note__c FROM CMA_Related_Task__c
                                                 WHERE CMA_Intervention_ID__c =: taskId];
        system.debug('relatedTask ->'+relatedTask);
        List<String> relatedTaskIdList = new List<String>();
        if(relatedTask != null){
            for(CMA_Related_Task__c record : relatedTask){
                relatedTaskIdList.add(record.CMA_Member_Note__c);
            }
            system.debug('relatedTaskIdList ->'+relatedTaskIdList);
            List<Member_Note__c> callNotesTaskList = [SELECT ID, Name,Type__c,Notes__c,CMA_Notes_Url__c,CreatedBy.Id,CreatedBy.Name,CreatedDate, Account__c, Account__r.Name FROM Member_Note__c  
                                            WHERE Id IN : relatedTaskIdList ORDER By CreatedDate DESC];
            system.debug('callNotesTaskList ->'+callNotesTaskList);
            return callNotesTaskList;
        }
        else{
            return null;
        }
    }
    
    /*
        * @Description: method for getting related task of Member notes record
        * @param name : accountId
        * @return type: Member_Note__c list
        * @Developer: Aishwarya 
    */
    @AuraEnabled
    public static List<Member_Note__c> getCallNotesTasks(String accountId){
        Contact contact = [SELECT Name, Account.Name FROM Contact WHERE AccountId =: accountId LIMIT 1];
        List<Case> caseList = [SELECT Subject,AccountId, Account.Name FROM Case WHERE AccountId =:accountId AND Status ='Active'];
        if(contact != null && caseList != null){
            List<Member_Note__c> taskList = [SELECT CreatedBy.Name,Name,Type__c,Notes__c,CreatedDate, Account__r.Name,Account__c
                                   FROM Member_Note__c WHERE Account__c =: accountId AND Case__c =: caseList[0].id
                                   ORDER By CreatedDate DESC];
            system.debug('taskList ->'+taskList);
            return taskList;
        }
        else{
            return null;
        }
    }
    
    /*
        * @Description: method for getting Intervention record
        * @param name : member notes id
        * @return type: task list
        * @Developer: Aishwarya 
    */
    @AuraEnabled
    public static List<Task> getInterventionDetails(string callNotesId){
        List<CMA_Related_Task__c> relatedTask = [SELECT CMA_Intervention_ID__c FROM CMA_Related_Task__c
                                                 WHERE CMA_Member_Note__c =: callNotesId];
        
        List<String> interventionIdList = new List<String>();
        if(relatedTask != null){
            for(CMA_Related_Task__c record : relatedTask){
                interventionIdList.add(record.CMA_Intervention_ID__c);
                system.debug('interventionIdList@@@@ ->'+interventionIdList);
            }
        }
        List<Task> relatedTaskList = [SELECT Id,Subject,Status,ActivityDate,CMA_Task_Url__c,CMA_Goal_Url__c,CMA_Problem_Url__c,HealthCloudGA__CarePlanProblem__c,HealthCloudGA__CarePlanProblem__r.Name,HealthCloudGA__CarePlanGoal__c,HealthCloudGA__CarePlanGoal__r.Name,
                                      HealthCloudGA__CarePlanGoal__r.HealthCloudGA__CarePlanProblem__r.Name, createdby.name, CreatedDate
                                      FROM Task WHERE  Type ='Intervention' AND Id IN : interventionIdList ORDER By CreatedDate DESC];
        
        if(relatedTaskList != null){
            system.debug('relatedTaskList ->'+relatedTaskList);
            return relatedTaskList;    
        }
        else{
            return null;
        }
        
    }
}