import { LightningElement, api } from 'lwc';
export default class CMA_ArticleITC extends LightningElement {
		
		@api item;
		@api itemid;
		@api itemcreateddate;
		@api itemcreatedbyname;
		@api itemproblemname;
		@api itemgoalname;
		@api itemsubject;
		@api itemstatus;
		@api itemactivitydate;
		@api accountname;
		sfdcBaseURL;
		prblmurl;
		subjectUrl;
		accountUrl;
		
		connectedCallback(){
				this.sfdcBaseURL = window.location.origin;
				this.prblmurl = this.sfdcBaseURL + '/' +this.itemproblemname.Id;
				this.subjectUrl = this.sfdcBaseURL + '/' +this.itemid;
				this.accountUrl =  this.sfdcBaseURL + '/' +this.accountname.AccountId;
				console.log('Account Name',  JSON.stringify(this.accountname));
		}
		hideArticle=false;
		alertThat(event){
				this.hideArticle = this.hideArticle ==false ? true:false;	
		}
		
}