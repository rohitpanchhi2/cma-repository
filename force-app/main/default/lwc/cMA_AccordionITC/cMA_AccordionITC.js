import { LightningElement, api} from 'lwc';
import getCallNotesDetails from '@salesforce/apex/CMA_TaskToRelatedCallsController.getCallNotesDetails';
export default class CMA_AccordionITC extends LightningElement {

		activeSections = ['B'];
		activeSectionsMessage = '';
		callnoteslist;
		enablecallnotelist;
		flag;
		@api idvalueitc;
		@api item;

		handleSectionToggle(event) {
				const openSections = event.detail.openSections;

				if (openSections.length === 0) {
						this.activeSectionsMessage = 'All sections are closed';
				} else {
						this.activeSectionsMessage =
								'Open sections: ' + openSections.join(', ');
				}
		}

		connectedCallback(){
				//intervention to call apex call
				console.log('id value ', JSON.stringify(this.item));
				getCallNotesDetails({taskId : this.item.Id})
						.then((result) => {
						this.callnoteslist = result;
						this.enablecallnotelist = true;
						console.log('inside '+result);
						let callData = JSON.stringify(this.callnoteslist);
						console.log('inside '+callData);
						if(result.length === 0){
								this.flag = true;
						}
						else{
								this.flag = false;
						}
						console.log('data available :'+this.flag);

				})
						.catch((error) => {
						console.log('call back error....');
						this.error = error;
						this.callnoteslist = undefined;
						console.log('Error is', this.error); 
				});	

		}
}