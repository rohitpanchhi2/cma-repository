import { LightningElement, api, track } from 'lwc';
import getMeetingDetails from '@salesforce/apex/CMA_ICTMeetingController.getMeetingDetails';
import updateCampaign from '@salesforce/apex/CMA_ICTMeetingController.updateMeeting';
import insertNotes from '@salesforce/apex/CMA_ICTMeetingController.addNotes';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import updateObjectData from '@salesforce/apex/CMA_GenericDataViewController.updateObjectData';

export default class CMA_ICTMeetingsScreen extends LightningElement {
		@track likeState = false;
		lastSavedData = [];
		myVal;		
		currentDateTime;
		accountId;
		columns;
		meetingData = [];
		@api recordId;
		hraCriteria;
		carePlanCriteria;
		medicationCriteria;
		sdohCriteria;
		error;
		accountName;
		campaignList;
		activeSections = ['A', 'B', 'C', 'D', 'E', 'F'];
		firstMeeting;

		connectedCallback(){
				this.columns  = [
						{ label: 'Name', fieldName: 'Name' },
						{ label: 'Role', fieldName: 'CMA_Role__c' },
						{ label: 'Date', fieldName: 'CreatedDate', type: 'date' },
						{ label: 'Type', fieldName: 'CMA_Type__c' },
						//{ label: 'Status', fieldName:'CMA_Status__c', editable: true, type:'text'},

						{ label: 'Status', fieldName: 'CMA_Status__c', editable: true, type: 'picklist', typeAttributes: {
										placeholder: 'Choose status', options: [
												{label: 'Attended', value: 'Attended' },
												{label: 'No Show', value: 'No Show' },
												{label: 'Declined', value: 'Declined' },
												{label: 'New', value: 'New' },
												{label: 'Invited', value: 'Invited' },
												{label: 'Accepted', value: 'Accepted' }
										] 
										, value: { fieldName: 'CMA_Status__c' } // default value for picklist
										, context: { fieldName: 'Id' } // binding account Id with context variable to be returned back
								}
						},

						{ label: 'Reason', fieldName:'CMA_Reason__c', type:'text', editable: true },

				];
				
				//this.columns = columnData;
				
				this.getMeeting();
		}
		
		getMeeting(){
				getMeetingDetails({recordId : this.recordId})
						.then((result) => {
						let responseData = JSON.parse(result);
						let caseRec = JSON.parse(responseData.Case);
						this.accountName = caseRec.Account.Name;
						this.accountId =  caseRec.Account.Id;
						this.hraCriteria = 'vlocity_ins__AssessmentQuestionId__r.vlocity_ins__AssessmentId__r.CMA_Account__c = '+"'"+this.accountId+"'"+ ' ORDER BY vlocity_ins__AssessmentQuestionId__r.name ASC NULLS LAST';;
						this.carePlanCriteria = 'whatid = '+"'"+this.recordId+"'"+' AND (HealthCloudGA__CarePlanProblem__c !=null OR HealthCloudGA__CarePlanGoal__c !=null)';
						this.medicationCriteria = 'Account__c = '+"'"+this.accountId+"'" + ' ORDER BY Effective_From__c DESC NULLS LAST';
						this.sdohCriteria = 'whatId IN (SELECT Id From CareBarrier where PatientId = '+"'"+this.accountId+"'"+') AND Status !='+"'"+'Addressed'+"'";
						let campaign = JSON.parse(responseData.Campaign);
						this.campaignList = campaign;
						this.firstMeeting = this.campaignList[0].Id; 
						let campaignDetails =[];
						campaign.forEach(element =>{
								let newObj = { label: element.Name , value: element.Id};
								campaignDetails.push(newObj);
								if(element.Care_Plan_Campaign_Members__r){
										let tempList = element.Care_Plan_Campaign_Members__r.records;
										tempList.forEach( item =>{
												if(item.CMA_Care_Team_Member__c){
														item.Name = item.CMA_Care_Team_Member__r.Name;
												} else if(item.CMA_Contact__c) {
														item.Name = item.CMA_Contact__r.Name;
												}
												this.meetingData= null;
										});
								}
						});											


				})
						.catch((error) => {
						this.error = error; 
				});
		}
		
		selectedCampaign;
		handleCampaign(event){
				let selectedId =  event.target.value;
				this.selectedCampaign=selectedId;
				this.campaignList.forEach(element =>{
						if(selectedId === element.Id){
								if(element.Care_Plan_Campaign_Members__r){
										let tempList = element.Care_Plan_Campaign_Members__r.records;
										let relatedAttendees = [];
										tempList.forEach(item =>{
												if(item.CMA_Care_Team_Member__c){
														item.Name = item.CMA_Care_Team_Member__r.Name;
														relatedAttendees.push(item);
												}
												if(item.CMA_Contact__c) {
														relatedAttendees.push(item);
														item.Name = item.CMA_Contact__r.Name;
												}
												this.meetingData = relatedAttendees;
												this.lastSavedData = JSON.parse(JSON.stringify(this.meetingData));
											
										});
								}else{
										this.meetingData= null;
								}
						}
				});
		}
		draftValues = [];
		handleSave(event) {
				//event.preventDefault();
				console.log('fieldsval ',JSON.stringify(event.detail));
				let cloneArray = JSON.parse(JSON.stringify(this.meetingData));
				this.meetingData = [];
				let updatedFieldsList = event.detail.draftValues;
				console.log('updatedFields 1 ',JSON.stringify(updatedFieldsList));
				console.log('updatedFields 2 ',updatedFieldsList);			
				updateObjectData({listUpdate: event.detail.draftValues})
						.then(data=>{
						console.log('data',data);						
						updatedFieldsList.forEach(element =>{
								let foundElement = cloneArray.find(item => item.Id === element.Id);
								const keys = Object.keys(element);
								keys.forEach(key => {
										foundElement[key] = element[key];
								});
								console.log('Updated Element',foundElement);
						});
						this.meetingData = cloneArray;
						this.lastSavedData = JSON.parse(JSON.stringify(this.meetingData));						
				}).catch(error =>{
						console.log('error ', error);
						this.meetingData = cloneArray;
				});
				this.draftValues = [];				
		}

		handleToggleSection(){
				
		}

		meetingNotes;
		handleNotes(event){
				this.meetingNotes = event.target.value;
		}

		openAccordion(event){
				var label = event.target.iconName;
				if(label == 'utility:jump_to_bottom') {
						event.target.iconName ="utility:jump_to_top";
				} else {
						event.target.iconName = "utility:jump_to_bottom";
				}
				if(this.activeSections){
						this.activeSections = null;
				}else{
						this.activeSections = ['A', 'B', 'C', 'D', 'E', 'F'];
				}				
				this.likeState = !this.likeState;
		}
		
		markComplete(){
				
				if(this.selectedCampaign == undefined || this.selectedCampaign == null ){
						const evt = new ShowToastEvent({
								title: 'Error Occured',
								message: 'Please Select ICT Meeting',
								variant: 'error',
						});
						this.dispatchEvent(evt);
				}else{
						updateCampaign({recordId : this.selectedCampaign})
								.then((result) => {
								const evt = new ShowToastEvent({
										title: 'Marked as Complete',
										message: 'Meeting marked as complete.',
										variant: 'success',
								});
								this.dispatchEvent(evt);
						})
								.catch((error) => {
								this.error = error;
								const evt = new ShowToastEvent({
										title: 'Error Occured',
										message: error.body.message,
										variant: 'error',
								});
								this.dispatchEvent(evt);
						});
				}
		}
		
		addNotes(){
				if(this.meetingNotes == undefined || this.meetingNotes == null){
						const evt = new ShowToastEvent({
								title: 'Error Occured',
								message: 'Please add meeting notes',
								variant: 'error',
						});
						this.dispatchEvent(evt);
				}else{
						insertNotes({recordId : this.recordId, notes : this.meetingNotes})
								.then((result) => {
								const evt = new ShowToastEvent({
										title: 'Record Captured',
										message: 'Meeting Notes inserted.',
										variant: 'success',
								});
								this.dispatchEvent(evt);
								this.meetingNotes='';
						})
								.catch((error) => {
								this.error = error;
								const evt = new ShowToastEvent({
										title: 'Error Occured',
										message: error.body.message,
										variant: 'error',
								});
								this.dispatchEvent(evt);
						});
				}
		}
		
		updateDataValues(updateItem) {
        let copyData = [... this.meetingData];
        copyData.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    item[field] = updateItem[field];
                }
            }
        });

        //write changes back to original data
        this.meetingData = [...copyData];
    }

    updateDraftValues(updateItem) {
        let draftValueChanged = false;
        let copyDraftValues = [...this.draftValues];
        //store changed value to do operations
        //on save. This will enable inline editing &
        //show standard cancel & save button
        copyDraftValues.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    item[field] = updateItem[field];
                }
                draftValueChanged = true;
            }
        });

        if (draftValueChanged) {
            this.draftValues = [...copyDraftValues];														
        } else {
            this.draftValues = [...copyDraftValues, updateItem];														
        }
				console.log('updated draft value ',this.draftValues);
    }
		
		picklistChanged(event) {
        event.stopPropagation();
				let dataRecieved = event.detail.data;
        let updatedItem = { Id: dataRecieved.context, CMA_Status__c: dataRecieved.value };				
        this.updateDraftValues(updatedItem);
        this.updateDataValues(updatedItem);
    }

    handleCancel(event) {
        
        this.meetingData = JSON.parse(JSON.stringify(this.lastSavedData));;
        this.draftValues = [];
    }
		
}