import { LightningElement, api, track } from 'lwc';
import getTemplates from '@salesforce/apex/CMA_CarePlanLettersController.getLetterTemplate';
import getTemplatesAttachment from '@salesforce/apex/CMA_CarePlanLettersController.getAttachmentRecord';
import getAccAdd from '@salesforce/apex/CMA_CarePlanLettersController.addressMethod';
import createCommReq from '@salesforce/apex/CMA_CarePlanLettersController.createCommReq';
import commRequest from '@salesforce/apex/CMA_CarePlanLettersController.fetchComRequest';
import refresh from '@salesforce/messageChannel/refreshLwc__c';
import { createMessageContext, subscribe } from 'lightning/messageService';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class CMA_CarePlanLetters extends LightningElement {

		
		//refresh component
    messageContext;
    subscription;
		
		//to assign values for multiselect dual box
		listOptions = [];
		defaultOptions = [];
		requiredOptions = [];
		
		//current record page record id
		@api recordId;
		
		//variables to store values based on conditions 
		CommunicationTemplateValue;
		error;
		selectedTempId;
		selectedType;
		selectedAttId;
		enableDropDown = false;
		communicationRelation;
		emailAddress = [];
		
		//for passing parameters to child component
		@api itemId;
		@api address = [];
		@api itemName;
		@api itemEmail;
		@api role;
		
		//for storing selected row values emails and address templates
		ccEmailAddresses = [];
		toEmailAddresses = [];
		toHomeAddresses = [];
		selectedTemplate = [];
		selectedTemp;
		
		@api emailAddress = [];
		@api itemid;
		@api itemname;
		@api itememail;
		
		
		/*variables to select values from current rows (Name, emails, address)*/
		selectedType;
		type = false;
		ccEmail;
		toEamil;
		ccEmailDisabled = false;
		toEmailDisabled = false;
		ccEmailList = [];
		toEamilList = [];
		
		/*variables to get parent's style attributes*/
		@api classtr;
		@api classth;
		@api classtd;
		
		/*fetch comm request for table*/
		@track errorComm;
		@track commReqList;
		@track columns = [{label: 'Name',fieldName:'Name',sortable: true},
											{label: 'Status',fieldName:'CMA_Status__c'},
											{label: 'Date Sent',fieldName:'CMA_Send_Date__c', type:'date',typeAttributes:{year: "numeric",month: "2-digit",day: "2-digit",hour: "2-digit",minute: "2-digit"}},
											{label: 'Template',fieldName:'CMA_Template_Id__c'},
											{label: 'Type',fieldName:'CMA_Type__c'}];
		
		
		constructor() {
        super();

        this.messageContext = createMessageContext();
        this.subscribeMC();
    }

    subscribeMC() {
        if (this.subscription) {
            return;
        }

        this.subscription = subscribe(this.messageContext, refresh, () => {
            this.refreshFiles();
        });
    }

		/*call back function to get member and auth party details*/
		connectedCallback() {
				getAccAdd({accountId : this.recordId})
						.then((result) => {
						this.emailAddress = result; //contains, name email, addresses of member and auth party
				})
						.catch((error) => {
						console.log('In emailAddress call back error....');
						this.error = error;
						this.emailAddress = undefined; 
				});
				
			this.fetchCommReq();
				
		}
		
		fetchCommReq(){
				commRequest({recordId : this.recordId})
						.then((result) => {
						this.commReqList = result; //contains, name email, addresses of member and auth party
				})
						.catch((error) => {
						this.errorComm = error;
						this.commReqList = undefined;
				});
		}
		
		/*mehtod to change type of communication and fetching template names based on type from apex class*/
		changeType(event){
				this.selectedType = event.target.value;
				this.type = (this.selectedType === 'Email' ||  this.selectedType === 'Fax') ? true : false;
				getTemplates({type : this.selectedType })
						.then((result) => {
						this.CommunicationTemplateValue = result; //storing template records based on template type
						
				})
						.catch((error) => {
						this.error = error;
						this.CommunicationTemplateValue = undefined;
				});
		}
		
		/*used to change template record in dropdown and based on selected template id retrieving attachment records*/
		changeCommunicationTemplate(event){
				this.selectedTempId = event.target.value;
				getTemplatesAttachment({templateId : this.selectedTempId})
						.then(result => {
						var vals = [];

						for(var i in result){
								vals.push({label : result[i], value : result[i]}); //adding attachment id in array as it is dual box
						}

						if(vals !== null){
								this.enableDropDown = true;  //flag to enable dual box condition render
								this.listOptions = vals; // assigning array of attachment id to variable used in html dual box
						}
				})
						.catch((error) => {
						this.error = error;
						this.communicationRelation = undefined;
				});
		}

		/*selec attachment id and creating array for that to pass it into class for record creation*/
		handleTemplateChange(event) {
				// Get the list of the "value" attribute on all the selected options
				//const selectedOptionsList = event.detail.value;
				var selectVal = [];
				selectVal.push(event.detail.value);
				this.selectedTemplate = selectVal;
				
		}
		
		/*check box event handler to get current rows cc email address from child component */
		handleCcEmail(event) {
				const factor = event.detail;
				this.ccEmailAddresses.push(factor);
		}
		
		/*check box event handler to get current rows to email address from child component */
		handleToEmail(event) {
				const factor = event.detail;
				const flagVal= event.detail.disabletocheckbox;
				this.toEmailDisabled  = flagVal;
				this.toEmailAddresses.push(factor);
								
		}
		
		/*dropdown of address event handler to get current rows of selected address from child component */
		handleAddress(event) {
				const factor = event.detail;
				this.toHomeAddresses.push(factor);
								console.log('address  '+JSON.stringify(this.toHomeAddresses));
		}
		
		
		@track isModalOpen = false;
		openModal() {
				// to open modal set isModalOpen tarck value as true
				this.isModalOpen = true;
		}
		closeModal() {
				// to close modal set isModalOpen tarck value as false
				this.isModalOpen = false;
				this.type=false;this.toEmailDisabled = false; this.enableDropDown = false; this.selectedType = null; this.selectedTempId= null; this.selectedTemplate = []; this.ccEmailAddresses=[];this.toEmailAddresses=[];this.toHomeAddresses=[];
								
		}
		
		/*create request button method to create communication request */
		errorMessage;
		submitDetails() {
				// to close modal set isModalOpen tarck value as false
				//calling apex to create communitcation request record
				
				if(this.selectedType === null || this.selectedType === undefined || this.selectedTempId === undefined || this.selectedTempId === null || this.selectedTemplate.length === 0){
						this.errorMessage = 'Type, Template and Attachments cannot be blank, please select the options.';
						const evt = new ShowToastEvent({
								title: 'Error Occured',
								message: this.errorMessage,
								variant: 'error',
						});
						this.dispatchEvent(evt);
				}else{
						if(this.selectedType === 'Letter'){
								if(this.toHomeAddresses.length === 0 || this.toEmailAddresses.length === 0){
										const evt = new ShowToastEvent({
												title: 'Error',
												message: 'Please Select Primary Member and Address',
												variant: 'error',
										});
										this.dispatchEvent(evt);
								}else{
										this.createRequest();
								}
						}
						else{
								if(this.toEmailAddresses.length === 0){
										const evt = new ShowToastEvent({
												title: 'Error',
												message: 'Please Select Primary Member',
												variant: 'error',
										});
										this.dispatchEvent(evt);
								}else{
										this.createRequest();
								}
						}
				}				
		}
		
		createRequest(){
				createCommReq({recId:this.recordId, comType:this.selectedType, comTempId:this.selectedTempId, selAtt:JSON.stringify(this.selectedTemplate), ccMail:JSON.stringify(this.ccEmailAddresses), toMail:JSON.stringify(this.toEmailAddresses), toAdd:this.toHomeAddresses})
						.then((result) => {
						this.isModalOpen = false;
						this.fetchCommReq();
						this.type=false;this.toEmailDisabled = false; this.enableDropDown = false; this.selectedType = null; this.selectedTempId= null; this.selectedTemplate = []; this.ccEmailAddresses=[];this.toEmailAddresses=[];this.toHomeAddresses=[];
						this.errorMessage = 'Communication Request has been created successfully.';
						const evt = new ShowToastEvent({
								title: 'Record Created',
								message: this.errorMessage,
								variant: 'success',
						});
						this.dispatchEvent(evt);
				})
						.catch((error) => {
						this.error = error;
						console.log('lin ',error.body.pageErrors[0].message);
						
						const evt = new ShowToastEvent({
								title: 'Error Occured',
								message: JSON.stringify(error.body.pageErrors[0].message).substr(153, 86),
								variant: 'error',
						});
						this.dispatchEvent(evt);
				});
		}
		
}