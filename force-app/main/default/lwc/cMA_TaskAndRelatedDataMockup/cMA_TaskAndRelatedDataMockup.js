import {LightningElement,api,track } from 'lwc';
import getPGIDetails from '@salesforce/apex/CMA_TaskToRelatedCallsController.getPGIDetails';
import getCallNotesDetails from '@salesforce/apex/CMA_TaskToRelatedCallsController.getCallNotesDetails';
import getCallNotesTasks from '@salesforce/apex/CMA_TaskToRelatedCallsController.getCallNotesTasks';
import getInterventionDetails from '@salesforce/apex/CMA_TaskToRelatedCallsController.getInterventionDetails';
export default class CMA_TaskAndRelatedDataMockup extends LightningElement {
		@api recordId;
		@api pgiList = [];
		value = '';
		@api selectedValue;
		@api taskId;
		@api callNotesList = [];
		@api notesList = [];
		@api callNotesId;
		@api interventionList = [];
		enableCallNoteList = false;
		enableRelatedData = false;
		dataFlag = false;
		
		get options() {
				return [
						{ label: 'Call to Intervention', value: 'CTI' },
						{ label: 'Intervention to Call', value: 'ITC' },
				];
		}
		@track columns = [{
				label: 'Call Notes to Intervention',
				fieldName: 'CNI',
				type: 'checkbox',
				sortable: true
		},
											{
													label: 'Intervention to Call Notes',
													fieldName: 'INC',
													type: 'checkbox',
													sortable: true
											},
											{
													label: 'Problem',
													fieldName: 'Problem',
													type: 'text',
													sortable: true
											},
											{
													label: 'Goal',
													fieldName: 'Goal',
													type: 'text',
													sortable: true
											},
											{
													label: 'Intervention',
													fieldName: 'Intervention',
													type: 'text',
													sortable: true
											},
											{
													label: 'Status',
													fieldName: 'Status',
													type: 'text',
													sortable: true
											},
											{
													label: 'Date',
													fieldName: 'Date',
													type: 'Date',
													sortable: true
											}
										 ];
		connectedCallback() {
				getPGIDetails({accountId : this.recordId})
						.then((result) => {
						this.pgiList = result;
						let dataList = JSON.stringify(this.pgiList);
						console.log('inside '+dataList);

						let finalList = JSON.parse(dataList);
						console.log('Data finalList- >'+JSON.stringify(finalList));
						this.pgiList = finalList;
						if(result.length === 0){
								this.dataFlag = true;
						}
						else{
							this.dataFlag = false;
						}
						console.log('data available :'+this.dataFlag);
				})
						.catch((error) => {
						console.log('call back error....');
						this.error = error;
						this.pgiList = undefined;
						console.log('Error is', this.error); 
				});

				getCallNotesTasks({accountId : this.recordId})
						.then((result) => {
						this.notesList = result;
						let noteDataList = JSON.stringify(this.notesList);
						console.log('inside '+noteDataList);

						let finalNotesList = JSON.parse(noteDataList);
						console.log('Data finalList- >'+JSON.stringify(finalNotesList));
						this.notesList = finalNotesList;
						if(result.length === 0){
								this.dataFlag = true;
						}
						else{
							this.dataFlag = false;
						}
						console.log('data available :'+this.dataFlag);
				})
						.catch((error) => {
						console.log('call back error....');
						this.error = error;
						this.notesList = undefined;
						console.log('Error is', this.error); 
				});
		}

		openNotes = false;
		callNotesPara;
		getOptionsValue(event){
				if(this.openNotes){
						this.openNotes = false;
						this.openNotesPara = null;
				}
				console.log('event -> '+event);
				const selectedOption = event.detail.value;
				console.log('selected value is : '+selectedOption);
				this.selectedValue = selectedOption;
				this.enableCallNoteList = false;
				this.enableRelatedData = false;
		}

		get showCTITable(){
				if(this.selectedValue == 'CTI')
						return true;
				return false;
		}

		get showITCTable(){
				if(this.selectedValue == 'ITC')
						return true;
				return false;
		}
		
		handleClick(event){
				
				console.log('event -> '+event);
				console.log('event  val 1-> '+event.target.value);
				this.taskId = event.target.value;
				console.log('event  val-> '+this.taskId);
				getCallNotesDetails({taskId : this.taskId})
						.then((result) => {
						this.callNotesList = result;
						this.enableCallNoteList = true;
						console.log('inside '+result);
						let callData = JSON.stringify(this.callNotesList);
						console.log('inside '+callData);
						if(result.length === 0){
								this.dataFlag = true;
						}
						else{
							this.dataFlag = false;
						}
						console.log('data available :'+this.dataFlag);

				})
						.catch((error) => {
						console.log('call back error....');
						this.error = error;
						this.callNotesList = undefined;
						console.log('Error is', this.error); 
				});
		}
		hideArticle=false;
		alertThat(event){
							this.hideArticle = this.hideArticle ==false ? true:false;	
		}
		
		handleClick2(event){
			
				console.log('event -> '+event);
				console.log('event  val 2-> '+event.target.value);
				this.callNotesId = event.target.value;
				console.log('event  val-> '+this.callNotesId);
				getInterventionDetails({callNotesId : this.callNotesId})
						.then((result) => {
						console.log('Result is ->'+result);
						this.interventionList = result;
						this.enableRelatedData = true;
						console.log('inside '+result);
						let callData1 = JSON.stringify(this.interventionList);
						console.log('inside '+callData1);
						if(result.length === 0){
								this.dataFlag = true;
						}
						else{
							this.dataFlag = false;
						}
						console.log('data available :'+this.dataFlag);

				})
						.catch((error) => {
						console.log('call back error....');
						this.error = error;
						this.interventionList = undefined;
						console.log('Error is', this.error); 
				});
		}
		
		openNotes1(event){
				this.callNotesPara = event.target.value;
				console.log('paragraph is ', this.callNotesPara);
				this.openNotes = true;
		}
		closeBox() {
				this.openNotes = false;
		}
		openRelatedTasks(){
				alert('clicked');
		}
}