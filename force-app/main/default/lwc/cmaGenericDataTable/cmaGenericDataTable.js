import { LightningElement, api, track } from 'lwc';
import getFieldsAndRecords from '@salesforce/apex/CMA_GenericDataViewController.getFieldsAndRecords';
import updateObjectData from '@salesforce/apex/CMA_GenericDataViewController.updateObjectData';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class CmaGenericDataTable extends NavigationMixin(LightningElement) {
		@api recordId;
		@api objectApiName;

		@api lblobjectName;
		@api sObjectName;
		@api fieldSetName; 
		@api criteriaFieldAPIName;
		@api firstColumnAsRecordHyperLink;
		@api editableField;
		@api newRecord;

		@track columns;
		@track tableData;
		draftValues = [];
		recordCount;
		showDataTable = false;
		loadTable = true;
		error = false;
		errorMessage;		

		get labelDataTable(){
				return this.recordCount ? this.lblobjectName +' ('+this.recordCount+')' : this.lblobjectName;
		}

		get listViewLink(){
				return '/lightning/o/' + this.sObjectName + '/list';
		}

		connectedCallback(){
				console.log('editableField-->'+this.editableField+'; newRecord-->'+this.newRecord);
				console.log('sObjectName-->'+this.sObjectName+'; fieldSetName-->'+this.fieldSetName);
				if( !(this.sObjectName === undefined || this.sObjectName === '') && !(this.fieldSetName === undefined || this.fieldSetName === '')){
						this.showDataTable = true;
						this.loadTable = true;
						this.getDataFromApex();	
				}else{
						console.log('No Apex Call');
				}
		}

		getDataFromApex(){
				getFieldsAndRecords({ strObjectApiName: this.sObjectName, strfieldSetName: this.fieldSetName, criteriaField: this.criteriaFieldAPIName, recordId: this.recordId})
						.then(data=>{
						let firstTimeEntry = false;
						let objStr = JSON.parse(data);
						let listOfFields= JSON.parse(objStr.Field_List);
						//console.log('list Of Fields',listOfFields);
						let listOfRecords = JSON.parse(objStr.Record_List);
						//console.log('list Of Records',listOfRecords);
						let recordListClone = JSON.parse(JSON.stringify(listOfRecords));
						const fieldRefApi = new Map(Object.entries(JSON.parse(objStr.Field_Ref_Map)));
						//console.log('fieldApi vs RefApi',fieldRefApi);
						const excepObjMap = new Map(Object.entries(JSON.parse(objStr.Exception_Objs_Map)));
						//console.log('Exception Obj Map',excepObjMap);
						const fieldEditable = new Map(Object.entries(JSON.parse(objStr.Field_Editable_Map)));
						//console.log('Field Editable Map',fieldEditable);

						let items = [];						
						listOfFields.forEach(element=>{
								let editFieldValue = false;
								if(this.editableField && fieldEditable.get(element.fieldPath)){
										editFieldValue = true;
								}

								if(this.firstColumnAsRecordHyperLink === true && firstTimeEntry === false){                  
										items = [...items ,{ label: element.label, fieldName: 'Id', type: 'button',
																				typeAttributes: { label: {fieldName: element.fieldPath}, name: {fieldName: 'Id'}, variant:'base'}  }];
										firstTimeEntry = true;
								} else if(element.type === 'reference'){ 
										items = [...items , { label: element.label, fieldName:element.fieldPath , type: 'button', 
																				 typeAttributes: { label: {fieldName: fieldRefApi.get(element.fieldPath)}, name: {fieldName: element.fieldPath}, variant: 'base'} }];
								} 
								else if(element.type === 'date'){
										items = [...items , {label: element.label, fieldName: element.fieldPath, type:'date-local', editable: editFieldValue} ];
								} 
								else if(element.type === 'datetime'){
										items = [...items , {label: element.label, fieldName: element.fieldPath, type:'date', editable: editFieldValue} ];
								}
								else if(element.type === 'boolean'){
										items = [...items , {label: element.label, fieldName: element.fieldPath, type:'boolean', editable: editFieldValue} ];
								}
								else if(element.type === 'integer'){
										items = [...items , {label: element.label, fieldName: element.fieldPath, type:'number', editable: editFieldValue} ];
								}
								else{
										items = [...items , {label: element.label, fieldName: element.fieldPath, editable: editFieldValue} ];
								}   
						});						
						this.columns = items;
						let fieldApiKey = Array.from( fieldRefApi.keys());
						let tableDataList = recordListClone.map(item =>{							
								if(fieldRefApi.size > 0){										
										fieldApiKey.forEach( elementData =>{
												if(item[elementData]){
														let referenceValue = item[elementData];
														//item[elementData] = '/lightning/r/' + item[elementData] + '/view';

														if(item[fieldRefApi.get(elementData)].Name){
																referenceValue = item[fieldRefApi.get(elementData)].Name;
														} else {
																let fieldName = excepObjMap.get(item[fieldRefApi.get(elementData)].attributes.type);
																if(item[fieldRefApi.get(elementData)][fieldName]){
																		referenceValue = item[fieldRefApi.get(elementData)][fieldName];	
																}
														}
														item[fieldRefApi.get(elementData)] = referenceValue;
												}
										});
								}								
								return item;								
						});
						//console.log('tableDataList --> ',tableDataList);
						this.tableData = tableDataList;
						this.recordCount = this.tableData.length;
						if(this.recordCount >0 ){
								this.showDataTable = true;
								if(this.recordCount >8){
										let objDiv = this.template.querySelector('.slds-card__body');
										objDiv.style.height = '320px';
								}
						} else {
								this.showDataTable = false;
								this.recordCount = 'No Records Found';
								let objDiv = this.template.querySelector('.slds-card__header');
								objDiv.style.color = 'green';
						}
						this.loadTable = false;
				})
						.catch(error =>{
						this.error = true;
						this.errorMessage = error?error.body.message:'error message not found';
						console.log('Error',error);						
						this.loadTable = false;
				})        
		}

		viewRecord(event){
				//	console.log('ROW Action',JSON.stringify(event.detail.action));
				//	console.log('ROW Data',JSON.stringify(event.detail.row));
				let actionObj = event.detail.action;
				let rowData = event.detail.row;
				let fieldName = actionObj.name.fieldName;
				let relatedId = rowData[fieldName];
				//console.log('fieldName-->'+fieldName+'; relatedId-->'+relatedId);
				if(!(relatedId === undefined || relatedId === null)){
						this.navigateToRecordPage(relatedId);
				}
		}

		handleRefresh(){
				this.tableData = [];
				this.loadTable = true;
				this.getDataFromApex();
		}

		handleSave(event){
				this.loadTable = true;
				let cloneArray = JSON.parse(JSON.stringify(this.tableData));
				this.tableData = [];
				const updatedFieldsList = event.detail.draftValues;
				//console.log('updatedFields',JSON.stringify(updatedFieldsList));			
				updateObjectData({ listUpdate: updatedFieldsList, strObjectApiName: this.sObjectName})
						.then(data=>{
						console.log('data',data);						
						updatedFieldsList.forEach(element =>{
								let foundElement = cloneArray.find(item => item.Id === element.Id);
								const keys = Object.keys(element);
								keys.forEach(key => {
										foundElement[key] = element[key];
								});
							//	console.log('Updated Element',foundElement);
						});
						this.tableData = cloneArray;
						this.loadTable = false;
						
						const eventNewRec = new ShowToastEvent({
								title: 'Success!',
								message: 'Records were updated Successfully!',
								variant: 'success'
						});
						this.dispatchEvent(eventNewRec);
						
				}).catch(error =>{
						// this.error = true;
						//this.errorMessage = error.body?error.body.message:'error message not found';
						console.log('Error',error);
						this.tableData = cloneArray;
						this.loadTable = false;
						const eventNewRec = new ShowToastEvent({
								title: 'Error!',
								message: JSON.stringify(error.body.message),
								variant: 'error'
						});
						this.dispatchEvent(eventNewRec);
				});
				this.draftValues = [];
		}

		handleClick(){
				console.log('New');
				this[NavigationMixin.Navigate]({
						type: 'standard__objectPage',
						attributes: {
								objectApiName: this.sObjectName,
								actionName: 'new',
						}
				});
		}

		navigateToObjectPage(){
				this[NavigationMixin.Navigate]({
						type: 'standard__objectPage',
						attributes: {
								objectApiName: this.sObjectName,
								actionName: 'home',
						},
				});
		}

		navigateToRecordPage(recordIdObj){
				this[NavigationMixin.Navigate]({
						type: 'standard__recordPage',
						attributes: {
								recordId: recordIdObj,
								actionName: 'view',
						},
				});
		}
}