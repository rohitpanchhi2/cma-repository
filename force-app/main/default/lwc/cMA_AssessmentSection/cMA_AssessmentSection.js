import { LightningElement ,api,track } from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import { OmniscriptBaseMixin } from "vlocity_ins/omniscriptBaseMixin";

export default class CMA_AssessmentSection extends OmniscriptBaseMixin(NavigationMixin(LightningElement)) {
    @api assessmentNameList = [];
    @api assessmentDateList = [];
    @api assessmentIdList = [];
    @track columns = [{
        label: 'Assessment',
        fieldName: 'Name',
        type: 'text',
        sortable: true
    },
    {
        label: 'Link',
        fieldName: 'Link',
        type: 'url',
        sortable: true
    },
    {
        label: 'Date Completed',
        fieldName: 'Date',
        type: 'text',
        sortable: true
    }
];
 hasRendered = true;
 renderedCallback(){
    if(this.hasRendered){
            console.log('Rendered');
            this.hasRendered = false;
            this.callOSRemoteCall();						
    }
}
connectedCallback() {
    /*getAssessment()
    .then((result) => {
        this.assessmentList = result;
        console.log(JSON.stringify(this.assessmentList));
        })
        .catch((error) => {
            console.log('call back error....');
            this.error = error;
            this.assessmentList = undefined;
            console.log('Error is', this.error); 
    });*/

    this._remoteActionJsonDef = {
        type: 'Remote Action',
        propSetMap: {
            extraPayload: { AccountId: '0015f000004NaSLAA0'},
            remoteOptions: {} ,
            label: 'RemoteAction1', 
            remoteMethod: 'getAssessment',
            remoteClass: 'CMA_AssessmentDetailsSection', 
        },
        name: 'RemoteAction1', 
        level: 0,  
        bRemoteAction: true,
    };
  }

  callOSRemoteCall() {
    this.template
        .querySelector('[data-omni-key=RemoteAction1]')
        .execute()
        .then(resp => {
                console.log('Response : '+resp);
                const respObj = JSON.stringify(resp);
                const obj = JSON.parse(respObj);
                console.log('Respone2: '+obj);
                console.log('Respone3: '+respObj);
                console.log('Respone4: '+obj.result.data);
                let assessmentList = JSON.stringify(obj.result.data);
                console.log('Data is ->'+assessmentList);
                this.assessmentNameList = obj.result.data;
                //this.assessmentDateList = obj.result.date;
                //this.assessmentIdList = obj.result.Id;
                //console.log('Data is ->'+JSON.stringify(this.assessmentList));
               /* const myEvent = new CustomEvent('omniactionbtn', {
                    bubbles: true,
                    cancelable: true,
                    composed: true,
                    detail:' respObj,
                });
            
                this.dispatchEvent(myEvent)*/
        })
        .catch(err => {
            // system error
        });
    }

  navigateToRecordPage(event) {
      console.log('event -> '+event);
      console.log('event  val-> '+event.currentTarget.dataset.value);
      const recId = event.currentTarget.dataset.value;
     this[NavigationMixin.Navigate]({
        type: 'standard__recordPage',
        attributes: {
            recordId: recId,
            objectApiName: 'vlocity_ins__Assessment__c',
            actionName: 'view'
        }
    });
}
}