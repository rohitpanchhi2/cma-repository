import { LightningElement, api } from 'lwc';
import getInterventionDetails from '@salesforce/apex/CMA_TaskToRelatedCallsController.getInterventionDetails';
export default class CMA_AccordionCTI extends LightningElement {
		activeSections = ['B'];
		activeSectionsMessage = '';
		interventionlist;
		flag;
		@api idvaluecti;
		enablerelateddata;
		sfdcBaseURL;
		problemUrl;
		goalUrl;
		taskUrl;
		
		
		handleSectionToggle(event) {
				const openSections = event.detail.openSections;

				if (openSections.length === 0) {
						this.activeSectionsMessage = 'All sections are closed';
				} else {
						this.activeSectionsMessage =
								'Open sections: ' + openSections.join(', ');
				}
		}

		connectedCallback(){
				//call to intervention apex call
				console.log('call notes id ', JSON.stringify(this.idvaluecti));
				this.sfdcBaseURL = window.location.origin;
				this.problemUrl = this.sfdcBaseURL +'/'+ 'Item5.HealthCloudGA__CarePlanProblem__c';
				getInterventionDetails({callNotesId : this.idvaluecti.Id})
						.then((result) => {
						console.log('Result is ->'+result);
						this.interventionlist = result;
						this.enablerelateddata = true;
						console.log('inside '+result);
						let callData1 = JSON.stringify(this.interventionlist);
						console.log('inside '+callData1);
						if(result.length === 0){
								this.flag = true;
						}
						else{
								this.flag = false;
						}
						console.log('data available :'+this.dataflag);

				})
						.catch((error) => {
						console.log('call back error....');
						this.error = error;
						this.interventionlist = undefined;
						console.log('Error is', this.error); 
				});
		}
}