import { LightningElement, api } from 'lwc';
export default class CMA_ArticleCTI extends LightningElement {
		@api item;
		@api itemid;
		@api itemname;
		@api itemcreateddate;
		@api itemcreatedbyname;
		@api itemtype;
		@api itemnotes;
		hideArticle=false;
		sfdcBaseURL;
		noteurl;
		subjectUrl;
		accountname;
		accounturl;
		
		connectedCallback(){
				this.sfdcBaseURL = window.location.origin;
				this.accountname = this.item.Account__r.Name;
				this.noteurl = this.sfdcBaseURL +'/'+ this.item.Id;
				this.accounturl = this.sfdcBaseURL +'/'+ this.item.Account__c;
		}
		
		alertThat(event){
				this.hideArticle = this.hideArticle ==false ? true:false;	
		}
}