import { LightningElement,track,api} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getTitle from '@salesforce/apex/CMA_knowledgeSearchController.getTitle';
export default class KnowledgeView extends NavigationMixin(LightningElement) {
		@api ranger;
		@api top = 50;
		@api left = 50;
		activeSectionMessage = '';
		@api showmodalvalue;	
		@track articleList = [];
		@api flag = [];

		@api
		get myflag(){
				return this.flag;
		}

		set myflag(value) {
				this.flag = value;
		}

		@api
		get myranger(){
				return this.ranger;
		}

		set myranger(value) {
				this.ranger = value;
		}

		@api
		get topmargin(){
				return this.top;
		}

		set topmargin(value) {
				this.top = value;
		}

		@api
		get showmodal(){
				return this.showModalValue;
		}

		set showmodal(value) {
				this.showModalValue = value;
		}
		
		hasRendered = true;
 		renderedCallback(){
    				if(this.hasRendered){
            					console.log('Rendered');
            					this.hasRendered = false;
            					this.connectedCallback();						
    				}
		}

		connectedCallback(){
				console.log('inside child');
				getTitle({articleId : this.ranger})
				.then((result => {
					console.log('result is : '+JSON.stringify(result));
					let dataList = JSON.stringify(result);
					let finalList = JSON.parse(dataList);
					this.articleList = finalList;
					console.log('title is : '+finalList.Title);
				}))
				.catch((error) => {
					console.log('call back error....');
					console.log('error is '+this.error);
			}); 
		}



		handleToggleSection(event) {
				this.activeSectionMessage =
						'Open section name:  ' + event.detail.openSections;
		}

		handleClick(event){
				
				//this.showmodalvalue = false;
				console.log('clicked');
				//console.log('value -> '+this.showModalValue);
				const passEvent = new CustomEvent('modalshow', {
						detail:{recordId:false} 
				});
				this.dispatchEvent(passEvent);

		}

		navigateToRecordPage(event) {
			console.log('event -> '+event);
			console.log('event  val-> '+event.currentTarget.dataset.value);
			const recId = event.currentTarget.dataset.value;
		   this[NavigationMixin.Navigate]({
			  type: 'standard__recordPage',
			  attributes: {
				  recordId: recId,
				  objectApiName: 'Knowledge__kav',
				  actionName: 'view'
			  }
		  });
		}
}