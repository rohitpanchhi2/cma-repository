import { LightningElement, api } from 'lwc';
import { createMessageContext, subscribe } from 'lightning/messageService';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class CMA_CarePlanLetterEmailRow extends LightningElement {
		
		/*public parameter variables to take values from parent cmp*/
		@api address;
		@api emailAddress = [];
		@api itemid;
		@api itemname;
		@api itememail;
		@api role;
		@api type = false;
		@api toemaildisabled = false;
		
		checkInput1 = 'input1'+this.itemid;
		checkInput2 = 'input2'+this.itemid;
		
		/*variables to select values from current rows (Name, emails, address)*/
		selectedAddress = null;
		ccEmail;
		toEamil;
		ccEmailDisabled = false;
		//toemaildisabled = false;
		ccEmailList = [];
		toEamilList = [];
		
		/*variables to get parent's style attributes*/
		@api classtr;
		@api classth;
		@api classtd;
		
		alertMessage;
		
		connectedCallback(){
				console.log('test'+ JSON.stringify(this.address));
				console.log('testE'+ this.itemEmail);
		}
		
		/*method to select address and pass selected address to parent component*/
		changeAddress(event){
				this.selectedAddress = event.target.value;
				//firing an event to pass selected address
				this.dispatchEvent(new CustomEvent('selectaddress', {
						detail: event.target.value
				}));
		}
		
		/*method to select to emails and pass selected to emails to parent component*/
		
		selectToEmail(event){
				
				//firing an event to pass selected to email
				if(event.target.checked){
						if(this.type === false){
								if(this.selectedAddress === null || this.selectedAddress === undefined){
										this.alertMessage = 'Please select Address First'
										const evt = new ShowToastEvent({
												title: 'Erro Occured',
												message: this.alertMessage,
												variant: 'error',
										});
										this.dispatchEvent(evt);
								}else{
										this.ccEmailDisabled = this.ccEmailDisabled === false ? true : false; //flag used to handle check box selection to avoid duplicacy between to and cc emails of selected emails
				
										this.dispatchEvent(new CustomEvent('toemail', {
												detail:{primaryId: event.target.id, recipient:event.target.value, disabletocheckbox:true, selectedtoaddress:this.selectedAddress}
										}));
								}	
						}else{
								this.ccEmailDisabled = this.ccEmailDisabled === false ? true : false; //flag used to handle check box selection to avoid duplicacy between to and cc emails of selected emails
				
								this.dispatchEvent(new CustomEvent('toemail', {
										detail:{primaryId: event.target.id, recipient:event.target.value, disabletocheckbox:true}
								}));
						}
				}
		}
		
		/*method to select select emails and pass selected ccEmails to parent component*/
		selectCcEmail(event){
				//firing an event to pass selected cc email
				if(event.target.checked){
						if(this.type === false){
								if(this.selectedAddress === null || this.selectedAddress === undefined){
										this.alertMessage = 'Please select Address First'
										const evt = new ShowToastEvent({
												title: 'Erro Occured',
												message: this.alertMessage,
												variant: 'error',
										});
										this.dispatchEvent(evt);

								}else{
										this.toemaildisabled = this.toemaildisabled === true ? true : false; //flag used to handle check box selection to avoid duplicacy between to and cc emails of selected emails
										this.dispatchEvent(new CustomEvent('ccemail', {
												detail: {ccId:event.target.id, ccEmail:event.target.value, ccAddress:this.selectedAddress}
										}));								
								}
								
						}else{
								this.toemaildisabled = this.toemaildisabled === true ? true : false; //flag used to handle check box selection to avoid duplicacy between to and cc emails of selected emails
								
								this.dispatchEvent(new CustomEvent('ccemail', {
										detail: event.target.value
								}));
						}
				}else{
						if(this.type === false){
								if(this.selectedAddress === null && this.toemaildisabled === false){
										this.toemaildisabled = false;
								}else if(this.selectedAddress === null && this.toemaildisabled === true){
										this.toemaildisabled = true;
								}
						}else{
								if(this.selectedAddress === null && this.toemaildisabled === false){
										this.toemaildisabled = false;
								}else if(this.selectedAddress === null && this.toemaildisabled === true){
										this.toemaildisabled = true;
								}
						}
				}
				
		}
		
}