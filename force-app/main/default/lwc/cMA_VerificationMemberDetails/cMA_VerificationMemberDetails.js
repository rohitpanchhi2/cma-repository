import { LightningElement,api } from 'lwc';
import getFieldsAndRecords from '@salesforce/apex/CMA_GenericDataViewController.getFieldsAndRecords';
export default class CMA_VerificationMemberDetails extends LightningElement {

		@api recordId;
		@api lblobjectName;
		@api sObjectName;
		@api fieldSetName; 
		@api criteriaFieldAPIName;
		@api inputobject;
		@api output;
		@api input;
		@api outputobject;
		verificationDetails = [];
		verificationRecord = [];

		@api verifiedCheck = false;	
		@api count = 0;
		@api incount;
		

		ALLFIELDS = [];

		connectedCallback(){

				if(!(this.incount === null || this.incount === undefined)){
						this.count = this.incount;
				}
				if(!(this.input === null || this.input === undefined)){
						this.verificationDetails = JSON.parse(this.input);
				}
				if(!(this.inputobject === null || this.inputobject === undefined)){
						
						this.ALLFIELDS = JSON.parse(this.inputobject);
				}else{
						getFieldsAndRecords({ strObjectApiName: this.sObjectName, strfieldSetName: this.fieldSetName, criteriaField: this.criteriaFieldAPIName, recordId: this.recordId})
								.then(data=>{
								let objStr = JSON.parse(data);
								let listOfFields= JSON.parse(objStr.Field_List);
								let listOfRecords = JSON.parse(objStr.Record_List);
								let recordListClone = JSON.parse(JSON.stringify(listOfRecords));
								const fieldRefApi = new Map(Object.entries(JSON.parse(objStr.Field_Ref_Map)));
								const excepObjMap = new Map(Object.entries(JSON.parse(objStr.Exception_Objs_Map)));
							

								let items = [];						
								listOfFields.forEach(element=>{	
										items = [...items , {label: element.label, fieldName: element.fieldPath,} ];
										this.ALLFIELDS = [...this.ALLFIELDS , {Label:element.label, Api:element.fieldPath, Value:null, Checked:false,}];
								});						
								this.columns = items;

								let fieldApiKey = Array.from( fieldRefApi.keys());
								let tableDataList = recordListClone.map(item =>{							
										if(fieldRefApi.size > 0){										
												fieldApiKey.forEach( elementData =>{
														if(item[elementData]){
																let referenceValue = item[elementData];
																//item[elementData] = '/lightning/r/' + item[elementData] + '/view';

																if(item[fieldRefApi.get(elementData)].Name){
																		referenceValue = item[fieldRefApi.get(elementData)].Name;
																} else {
																		let fieldName = excepObjMap.get(item[fieldRefApi.get(elementData)].attributes.type);
																		if(item[fieldRefApi.get(elementData)][fieldName]){
																				referenceValue = item[fieldRefApi.get(elementData)][fieldName];	
																		}
																}
																item[fieldRefApi.get(elementData)] = referenceValue;
														}
												});
										}								
										return item;								
								});

								this.tableData = tableDataList;

								this.tableData.forEach(tdata=>{										
										this.ALLFIELDS.forEach(element =>{
												Object.keys(tdata).forEach(key =>{
														if(key === element.Api){
																element.Value = tdata[key];
														}
												})
										})
								});
						})
								.catch(error =>{
								this.error = true;
								this.errorMessage = error.body.message?error.body.message:'error message not found';												
						}) 
				}
		}		
		
		handleCheckbox(event){

				this.verificationRecord.push({Label:event.target.dataset.label, Api:event.target.dataset.api, Value:event.target.value, Checked:event.target.checked});	

				this.ALLFIELDS.forEach(tdata=>{
						this.verificationRecord.forEach(element =>{
								if(tdata.Api === element.Api){
										tdata.Checked = element.Checked;
								}
						})
				});

				this.output= JSON.stringify(this.ALLFIELDS);
				let fieldLabelValue = {};
				fieldLabelValue[event.target.dataset.label] = event.target.value;
				this.verificationDetails.push(fieldLabelValue);
				this.outputobject = JSON.stringify(this.verificationDetails); 

				if(event.target.checked){
						this.count +=1;		
				}else{
						this.count -=1;
				}

				if(this.count>2){
						this.verifiedCheck =true;
				}else{
						this.verifiedCheck =false;
				}
				
				console.log('count ',this.count);
								console.log('output ', this.outputobject);
								console.log('allfields ', this.output);
								console.log('check ',this.verifiedCheck);
		}
}