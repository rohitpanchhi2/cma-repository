import { LightningElement,api,wire,track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { OmniscriptBaseMixin } from "vlocity_ins/omniscriptBaseMixin";
import { getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi';
//import { refreshApex } from '@salesforce/apex';

export default class CMA_UpdatePhoneDetailsLWC extends OmniscriptBaseMixin(LightningElement){
		@api recordId;
		@api accountId;
		@api calledPhoneNumbers;
		@api preCalledNumbers;
		phoneRecordList;
		defaultRecordList;
		//refreshTable;
		prefPicklistOptions;
		typePicklistOptions;
		defaultRecordTypeId;
		showButtons = false;
		showDefaultData = true;
		showSpinner = true;
		openBox = false;	

		constructor() {
			super();
		 
			document.addEventListener("lwc://CMA_UpdatePhoneDetailsLWC", () => {
				const evt = new ShowToastEvent({
					title: "Info",
					message: "received refreshView event",
					variant: "info",
				});
				this.dispatchEvent(evt);
			});
		  }
		  refreshViews() {
			document.dispatchEvent(new CustomEvent("aura://CMA_RefreshViewForLWC"));
		  }

		hasRendered = true;
		renderedCallback(){
				if(this.hasRendered){
						console.log('Rendered');
						this.hasRendered = false;
						this.callOSRemoteCall();						
				}
		}

		connectedCallback() {
				this.showSpinner = true;
				console.log('accountId ->'+this.recordId);
				this.accountId = this.recordId;
				this._remoteActionJsonDef = {
						type: 'Remote Action',
						propSetMap: {
								extraPayload: { AccountId: this.recordId},
								remoteOptions: {} ,
								label: 'RemoteAction1', 
								remoteMethod: 'getAccountPhone',
								remoteClass: 'CMA_AssessmentDetailsSection', 
						},
						name: 'RemoteAction1', 
						level: 0,  
						bRemoteAction: true,
				};
				this.showSpinner = false;
		}

		callOSRemoteCall() {
				this.template
						.querySelector('[data-omni-key=RemoteAction1]')
						.execute()
						.then(resp => {
						console.log('Response : '+resp);
						const respObj = JSON.stringify(resp);
						console.log('response ->' +respObj);
						const obj = JSON.parse(respObj);
						console.log('Respone2: '+obj);
						console.log('Respone3: '+respObj);
						console.log('Respone4: '+obj.result.data);
						let phoneList = JSON.stringify(obj.result.data);
						console.log('Data is ->'+phoneList);
						this.defaultRecordList = obj.result.data;
				})
						.catch(err => {
						// system error
				});
		}

		@wire(getPicklistValuesByRecordType, {
				objectApiName: 'CMA_Phone__c',
				recordTypeId: '012000000000000AAA'

		})
		levelPicklistValues({error, data}) {
				if (error) {
						alert(JSON.stringify(error));
				} else if (data) {
						const a= data.picklistFieldValues['CMA_Phone_Preference__c'].values;
						const b = data.picklistFieldValues['CMA_Phone_Type__c'].values;
						this.prefPicklistOptions  = a;
						this.typePicklistOptions = b;
						console.log('pref value ->'+this.prefPicklistOptions);
						console.log('type value ->'+this.typePicklistOptions);

				}
		};

		handleClickEdit(event){
				if(this.showDefaultData === true){
						const cloneArray = JSON.parse(JSON.stringify(this.defaultRecordList));
						this.phoneRecordList = Array.from(cloneArray);
						this.showDefaultData = false;
						console.log('Phone Record List',this.phoneRecordList);
				}				
				let targetId = event.target.value;
				this.showButtons = true;

				let tempData = this.phoneRecordList.map( function(element){
						if(element.Id === targetId){
								element.prefReadyOnly = false;
						}
						else{
							element.prefReadyOnly = true;
						}
						return element;
				});
				console.log(' Updated Phone Records:',tempData);
				this.phoneRecordList = tempData;
		}

		handleCancel(){
				console.log('Cancel');
				this.showDefaultData = true;
				this.phoneRecordList = [];
				this.showButtons = false;
		}

		handleTypeChange(event){	
				let dataValue = event.target.value;
				console.log('dataValue -> '+dataValue);
				let dataId = event.target.name;
				console.log('dataId -> '+dataId);
				this.phoneRecordList.forEach( element => {
						if(element.Id === dataId){
								console.log('Inside');
								element.CMA_Phone_Type__c = dataValue;
								const foundElement = this.defaultRecordList.find(item => item.Id === element.Id);
								if(foundElement.CMA_Phone_Type__c !== element.CMA_Phone_Type__c){
										element.updated = true;
								}else{
										element.updated = false;
								}
						}
				});	
				console.log(' Updated Phone Records:',this.phoneRecordList);				 
				console.log(' Default Records:',this.defaultRecordList);		
		}

		handlePrefChange(event){
				let dataValue = event.target.value;
				let dataId = event.target.name;
				console.log('dataValue -> '+dataValue);
				console.log('dataId -> '+dataId);
				this.phoneRecordList.forEach( element => {
						if(element.Id === dataId){
								element.CMA_Phone_Preference__c = dataValue;
								const foundElement = this.defaultRecordList.find(item => item.Id === element.Id);
								if(foundElement.CMA_Phone_Preference__c !== element.CMA_Phone_Preference__c){
										element.updated = true;
								}else{
										element.updated = false;
								}
						}
				});
				console.log(' Updated Phone Records:',this.phoneRecordList);				 
				console.log(' Default Records:',this.defaultRecordList);
		}

		handleSave(){
				this.showSpinner = true;
				console.log('SAVE');
				let phoneListToUpdate = [];
				this.phoneRecordList.forEach( element => {
						if(element.updated === true){
								let phoneRecord = { CMA_Phone_Preference__c: element.CMA_Phone_Preference__c, CMA_Phone_Type__c: element.CMA_Phone_Type__c, Id: element.Id}
								phoneListToUpdate.push(phoneRecord);
						}
				});

				if(phoneListToUpdate.length === 0){
						console.log('No Records to Update');
						const event = new ShowToastEvent({
								title: 'Info',
								message: 'No Phone Records were Updated',
								variant: 'warning'
						});
						this.dispatchEvent(event);
						this.showButtons = false;
						this.showSpinner = false;
						return;
				}
				console.log('Phone Records',phoneListToUpdate);
				let jsonObj = {recordsToUpdate:phoneListToUpdate,accId:this.recordId};
				const params = {
						input: JSON.stringify(jsonObj),
						sClassName: 'CMA_AssessmentDetailsSection',
						sMethodName: 'updateAccountPhone',
						options: '{}',
				};
			this.omniRemoteCall(params, true).then(response => {
					window.console.log(response, 'response');

					if(response.error == false){
						let updatedListOfPhone = response.result.dataPhone;
					console.log('updatedListOfPhone ->'+updatedListOfPhone);
					updatedListOfPhone.forEach( element => {
						this.defaultRecordList.forEach(item => {
								if(item.Id === element.Id){
										item.CMA_Phone_Preference__c = element.CMA_Phone_Preference__c;
										item.CMA_Phone_Type__c = element.CMA_Phone_Type__c;
								}
						});
						this.showDefaultData = true;
			this.phoneRecordList = [];
			this.showButtons = false;
			this.showSpinner = false;
			const event = new ShowToastEvent({
				title: 'Success!',
				message: 'Phone Details Updated Successfully',
				variant: 'success'
		});
		this.dispatchEvent(event);	
				});

					}
					else{
						console.log('Error occurred');
					}
					
			}).catch(error => {
					window.console.log(error, 'error');
			});
				
				
				
				
		}

		// Modal Fucntions
		handleCreatePhone(){
				this.openBox = true;
				this.showButtons = false;
		}

		closeBox() {
				this.openBox = false;
		}


		handleSubmitPhone(event){
				event.preventDefault();
				let fields = event.detail.fields;
				if(!fields.CMA_Phone_Number__c == null || !fields.CMA_Phone_Number__c == undefined || !fields.CMA_Phone_Number__c == ''){
						this.template.querySelector('.secondForm').submit(fields);
						this.openBox = false;
				}else{
						this.openBox = false;
						const customEvent = new ShowToastEvent({
								title: 'Missing Infromation!',
								message: 'Please add Phone details',
								variant: 'error'
						});
						this.dispatchEvent(customEvent);
				}
						
				
		}
		handleSuccessPhone(event){
				this.showSpinner = true;
				console.log('Created Phone Rec: ',event.detail.id)
				const customEvent = new ShowToastEvent({
						title: 'Success!',
						message: 'Phone Record Created Successfully',
						variant: 'success'
				});
				this.dispatchEvent(customEvent);
				this.defaultRecordList = [];
				this.phoneRecordList = [];
				this.callOSRemoteCall();
				this.showSpinner = false;
		}

		errorHandler(event){
				let errorMessage = event.detail.output;
				let fieldError = errorMessage.fieldErrors;
				
				const customEvent = new ShowToastEvent({
						title: 'Error!',
						message: fieldError.CMA_Phone_Number__c[0].message,
						variant: 'error'
				});
				this.dispatchEvent(customEvent);
		}
}