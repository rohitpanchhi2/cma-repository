import { LightningElement,api,wire,track} from 'lwc';
import getAccountPhone from '@salesforce/apex/CMA_GetAccountPhoneData.getAccountPhone';
import updatePhoneRecords from '@salesforce/apex/CMA_GetAccountPhoneData.savePhone';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getObjectInfo, getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi'; 
import PHONE_OBJ from '@salesforce/schema/CMA_Phone__c';

//const PHONE_PREF = {fieldApiName: "CMA_Phone_Preference__c", objectApiName: "CMA_Phone__c"}
//const PHONE_TYPE = {fieldApiName: "CMA_Phone_Type__c", objectApiName: "CMA_Phone__c"}

export default class CMA_GetAccountPhoneData extends LightningElement {
		@api recordId;
		@api accountId;
		@api calledPhoneNumbers;
		@api preCalledNumbers;
		phoneRecordList;
		defaultRecordList;
		prefPicklistOptions;
		typePicklistOptions;
		defaultRecordTypeId;
		showButtons = false;
		showDefaultData = true;
		showSpinner = true;
		openBox = false;	

		@wire(getObjectInfo, { objectApiName: PHONE_OBJ })
		wiredObject({data, error}){
				if(data){
						this.defaultRecordTypeId = data.defaultRecordTypeId;
				}
				if(error){
						console.log(error);
				}
		}

		@wire(getPicklistValuesByRecordType, { recordTypeId : '$defaultRecordTypeId', objectApiName : PHONE_OBJ })
		wiredRecordtypeValues({data, error}){
				if(data){
						this.prefPicklistOptions = data.picklistFieldValues.CMA_Phone_Preference__c.values;
						this.typePicklistOptions = data.picklistFieldValues.CMA_Phone_Type__c.values;
				}
				if(error){
						console.log(JSON.stringify(error));
				}
		}

		connectedCallback() {
				this.showSpinner = true;
			//console.log('recordId ->'+this.recordId);
				console.log('accountId ->'+this.accountId);
				console.log('preCalledNumbers ->'+this.preCalledNumbers);
				this.getPhoneDataFromApex();
			  this.showSpinner = false;
		}

		getPhoneDataFromApex(){
				getAccountPhone({accountId : this.accountId})
						.then((result) => {
						
						let preDefinedValue = false;
						let splitNumbers = [];
						if(this.preCalledNumbers === undefined || this.preCalledNumbers === null){
								console.log('No Values');
						}else{
								splitNumbers = this.preCalledNumbers.split('; ');
							//splitNumbers.forEach(element =>{ element.trim() });
								console.log('Numbers List',splitNumbers);
								preDefinedValue = true;
								this.calledPhoneNumbers = this.preCalledNumbers;
						}
						
						console.log(' Phone Records: ',result);
						let dataList = result.map(function(element) {
								let obj = {...element, checkboxValue: false, typeReadOnly: true, prefReadyOnly: true, updated: false};
								
								if(preDefinedValue){
										splitNumbers.forEach( item =>{
												if(item === element.CMA_Phone_Number__c){
														obj.checkboxValue = true;
												}
										});
								}
								return obj;
						});						
						
						console.log(' Updated Phone Records: ',dataList);
						const cloneArray = JSON.parse(JSON.stringify(dataList));
						this.defaultRecordList = cloneArray;
						//	this.phoneRecordList = Array.from(dataList);					
				})
						.catch((error) => {
						console.log('Error '+JSON.stringify(error));
						this.phoneRecordList = undefined;
				});
		}

		handleTypeChange(event){				
		}

		handlePrefChange(event){
				let dataValue = event.target.value;
				let dataId = event.target.name;

				this.phoneRecordList.forEach( element => {
						if(element.Id === dataId){
								element.CMA_Phone_Preference__c = dataValue;
								const foundElement = this.defaultRecordList.find(item => item.Id === element.Id);
								if(foundElement.CMA_Phone_Preference__c !== element.CMA_Phone_Preference__c){
										element.updated = true;
								}else{
										element.updated = false;
								}
						}
				});
				//	console.log(' Updated Phone Records:',this.phoneRecordList);				 
				//	console.log(' Default Records:',this.defaultRecordList);
		}

		handleCheckbox(event){
				let dataId = event.target.name;
				let checkboxValue = event.target.checked;
				//	console.log('Id: ',dataId);
				//	console.log('Value: ',event.target.value);
				//	console.log('Checked: ',checkboxValue);
				let selectedNumbers = '';
				this.defaultRecordList.forEach(item => {
						if(item.Id === dataId){
								item.checkboxValue = checkboxValue;
						}
						if(item.checkboxValue === true){
								if(selectedNumbers == ''){
										selectedNumbers = item.CMA_Phone_Number__c;
								}else{
										selectedNumbers = selectedNumbers+'; '+item.CMA_Phone_Number__c ;	
								}
						}
				});
				const cloneArray = JSON.parse(JSON.stringify(this.defaultRecordList));
				this.phoneRecordList = cloneArray;
				this.calledPhoneNumbers = selectedNumbers;
				console.log('Called Numbers: ',selectedNumbers);
		}

		handleClickEdit(event){
				if(this.showDefaultData === true){
						const cloneArray = JSON.parse(JSON.stringify(this.defaultRecordList));
						this.phoneRecordList = Array.from(cloneArray);
						this.showDefaultData = false;
						console.log('Phone Record List',this.phoneRecordList);
				}				

				// console.log('Click: ',event.target.value);
				let targetId = event.target.value;
				this.showButtons = true;

				let tempData = this.phoneRecordList.map( function(element){
						if(element.Id === targetId){
								element.prefReadyOnly = false;
						}
						return element;
				});
				console.log(' Updated Phone Records:',tempData);
				this.phoneRecordList = tempData;
				//	console.log(' Default Records:',this.defaultRecordList);
		}

		handleSave(){
				this.showSpinner = true;
				console.log('SAVE');
				let phoneListToUpdate = [];
				this.phoneRecordList.forEach( element => {
						if(element.updated === true){
								let phoneRecord = { CMA_Phone_Preference__c: element.CMA_Phone_Preference__c, CMA_Phone_Type__c: element.CMA_Phone_Type__c, Id: element.Id}
								phoneListToUpdate.push(phoneRecord);
						}
				});

				if(phoneListToUpdate.length === 0){
						console.log('No Records to Update');
						const event = new ShowToastEvent({
								title: 'Info',
								message: 'No Phone Records were Updated',
								variant: 'warning'
						});
						this.dispatchEvent(event);
						this.showButtons = false;
						this.showSpinner = false;
						return;
				}
				console.log('Phone Records',phoneListToUpdate);

				updatePhoneRecords({lstPhone : phoneListToUpdate})
						.then((result) => {
						console.log(' Successful Update ',result);
						const event = new ShowToastEvent({
								title: 'Success!',
								message: 'Phone Details Updated Successfully',
								variant: 'success'
						});
						this.dispatchEvent(event);

						result.forEach( element => {
								this.defaultRecordList.forEach(item => {
										if(item.Id === element.Id){
												item.CMA_Phone_Preference__c = element.CMA_Phone_Preference__c;
												item.CMA_Phone_Type__c = element.CMA_Phone_Type__c;
										}
								});
						});
						console.log(' Updated Default Records:',this.defaultRecordList);
						this.showDefaultData = true;
						this.phoneRecordList = [];
						this.showButtons = false;
						this.showSpinner = false;
				})
						.catch((error) => {
						console.log('Error ',error);
				});
		}

		handleCancel(){
				console.log('Cancel');
				this.showDefaultData = true;
				this.phoneRecordList = [];
				this.showButtons = false;
		}

		// Modal Fucntions
		handleCreatePhone(){
				this.openBox = true;
				this.showButtons = false;
		}

		closeBox() {
				this.openBox = false;
		}

		handleSubmitPhone(event){
				event.preventDefault();
				let fields = event.detail.fields;
				this.template.querySelector('.secondForm').submit(fields);
				this.openBox = false;
		}

		handleSuccessPhone(event){
				this.showSpinner = true;
				console.log('Created Phone Rec: ',event.detail.id)
				const customEvent = new ShowToastEvent({
						title: 'Success!',
						message: 'Phone Record Created Successfully',
						variant: 'success'
				});
				this.dispatchEvent(customEvent);
				this.defaultRecordList = [];
				this.phoneRecordList = [];
				this.getPhoneDataFromApex();
				this.showSpinner = false;
		}

		errorHandler(event){
				console.log('Error in Create Phone',event.detail);
				const customEvent = new ShowToastEvent({
						title: 'Error!',
						message: 'Unable to Create Record',
						variant: 'error'
				});
				this.dispatchEvent(customEvent);
		}
}