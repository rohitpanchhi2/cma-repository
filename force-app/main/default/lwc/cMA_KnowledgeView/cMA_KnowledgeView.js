import { LightningElement, track, wire, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import KnowledgeArticles from '@salesforce/apex/CMA_knowledgeSearchController.getKnowledgeArticles';
//import assignArticleToCase from '@salesforce/apex/CMA_knowledgeSearchController.assignArticleToCase';
import {ShowToastEvent} from 'lightning/platformShowToastEvent'; // import toast message event .


const columns = [
		{label: 'Title', fieldName: 'url', type: 'url',sortable : true,typeAttributes: {label: { fieldName: 'Title' }, target: '_self'}},
		{ label: 'Associated Cases', fieldName: 'ArticleCaseAttachCount', type: 'text' },
		{ label: 'Total View', fieldName: 'ArticleTotalViewCount', type: 'text' },
];
export default class CMA_KnowledgeView extends NavigationMixin(LightningElement) {
		@track article;  
		@track results;
		@track data = [];
		columns = columns;
		@api recordId;
		@track recordSelected=false;
		caseId;
		error;
		showModal=false;
		@track ranger;
		@track left;
		@track top;
		@track articleId;
		@track flag;

		connectedCallback()
		{
				this.caseId=this.recordId;
		}

		@wire(KnowledgeArticles, {searchText : '$article'})
		wiredArticles({error, data}) {
				if (data) {
						this.articles = [];
						for (let article of data) {
								let myArticle = {};
								console.log('article:' + JSON.stringify(article));
								this.articleId = article.Id;
								console.log('Article Id -> '+this.articleId);
								this.KnowledgePageRef = {
										type: "standard__recordPage",
										attributes: {
												"recordId": article.Id,
												"objectApiName": "Knowledge__kav",
												"actionName": "view"
										}
								};

								this[NavigationMixin.GenerateUrl](this.KnowledgePageRef)
										.then(articleUrl => {
										myArticle = {...article};
										myArticle.url = articleUrl;
										this.data = [...this.data, myArticle];
								});
						}
						this.error = undefined;
				}
				if (error) {
						console.log('error');
						this.error = undefined;
						this.data = [];
				}
		}

		changeHandler(event) {
				this.article = event.target.value;
				this.data = [];
				console.log('article -> ', this.article);
		}
		assignFaqToCase = event => {
				var el = this.template.querySelector('lightning-datatable');
				var selectedRows = el.getSelectedRows();
				var caseArticles=[];
				for (let i = 0; i < selectedRows.length; i++){
						var caseArticle={
								"CaseId":this.caseId,
								"KnowledgeArticleId":selectedRows[i].KnowledgeArticleId
						};
						caseArticles.push(caseArticle);
						console.log('caseArticles ->'+JSON.stringify(caseArticles));
				}

				assignArticleToCase({articles: caseArticles})
						.then((data,error) => {
						if (data) {
								this.error = undefined;
								this.dispatchEvent(
										new ShowToastEvent({
												message: 'Case Associated Successfully',
												variant: 'success',
										}),
								);
						} else if (error) {
								this.error = error;
								console.log('Error:'+ JSON.stringify(error));
								this.dispatchEvent(
										new ShowToastEvent({
												message: 'Error in Case Assiciation:' + error,
												variant: 'error'
										}), );
						}
				});

		}
		handleRowSelection = event => {
				var selectedRows=event.detail.selectedRows;
				this.recordSelected=false;
				if(selectedRows.length>0)
				{  
						this.recordSelected=true;
				}
		}
		handleCancel= event => {
				this.recordSelected=false;
		}

		showData(event){
				this.ranger = event.currentTarget.dataset.rangerid;
				console.log('@@ -'+this.ranger);
				this.left = event.clientX;
				console.log('@@ -'+this.left);
				this.top=event.clientY;
				console.log('@@ -'+this.top);
				this.showModal = true;
				console.log('daaa '+this.showModal);
		}
		hideData(event){
				this.ranger = "";
		}
		get assignClass() { 
				return this.active ? '' : 'slds-hint-parent';
		}

		onModalshow(event){
				let valueFromchild = event.detail.recordId;
				console.log('Hi from child ->'+valueFromchild);
				if(valueFromchild){
						console.log('IFFFFFF');
						this.ranger = "";
				}
				else{
						console.log('ElSE');
						this.ranger = "";
						this.showModal = false;
						this.article = "";
						console.log('daaa '+this.showModal);
				}
		}


}