import { LightningElement,api,track } from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import recordTypeOfAccount from '@salesforce/apex/CMA_CreateNewDemoUser.recordTypeOfAccount';
import updateInsertedRecord from '@salesforce/apex/CMA_CreateNewDemoUser.updateInsertedRecord';
export default class CMA_CreateDemoUser extends NavigationMixin(LightningElement) {
		@api recordId;
		@api objectApiName = 'Account';
		@track recordTypeOptions =[];
		@api recordtypeName;
		@track accountFName;
		@track accountSName;
		showSpinner = true;
		showToast = false;
		checkboxValue;
        insertedRecordId;
		value=[];
        

		get options() {
				return [
						{ label: 'Address', value: 'Address' },
                        { label: 'Phone', value: 'Phone' },
                        { label: 'Diagnosis', value: 'Diagnosis' },
                        { label: 'Allergy', value: 'Allergy' },
                        { label: 'Immunization', value: 'Immunization' },
				];
		}

		connectedCallback(){
				recordTypeOfAccount()
						.then((result => {
						console.log('Response ->'+result);
						let values = [];
						result.forEach(element => {
							console.log('data - '+element);
								let object = {label: element, value: element};
								values.push(object);
						});
						console.log('Options -> '+values);
						this.recordTypeOptions = values;
						console.log('Options -> '+this.recordTypeOptions);
				}));

		}
		handleChange(event){
				let value = event.detail.value;
				this.recordtypeName = value;
				console.log('Value ->'+value);
		}

		handleFAccName(event){
			this.accountFName = event.detail.value;
			console.log('accountFName -> '+this.accountFName);
		}
		handleSAccName(event){
			this.accountSName = event.detail.value;
			console.log('accountSName -> '+this.accountSName);
		}

		handleSuccess(event){
				this.showSpinner = true;
                updateInsertedRecord({accFName : this.accountFName, accSName : this.accountSName, recordtypeId : this.recordtypeName, selectedValue : this.checkboxValue})
						.then((result => {
                            console.log('Call Apex');
                            console.log('$$@@'+this.recordtypeName);
							console.log('result -> '+JSON.stringify(result));
							let resp = JSON.stringify(result);
							let newResp = JSON.parse(resp);
							this.insertedRecordId = newResp.Id;
							console.log('result newResp.Id;-> '+newResp.Id);
							const customEvent = new ShowToastEvent({
								title: 'Success!',
								message: 'Account Created Successfully',
								variant: 'success'
							});
							this.dispatchEvent(customEvent);
							this.navigateToRecordPage();
							eval("$A.get('e.force:refreshView').fire();");
                        }))
						.catch((error) => {
							console.log('call back error....');
							console.log('error is '+this.error);
							const customEvent = new ShowToastEvent({
								title: 'Error!',
								message: 'Unable to create record',
								variant: 'error'
							});
							this.dispatchEvent(customEvent);
							console.log('error '+this.error);
							eval("$A.get('e.force:refreshView').fire();");
					}); 
                this.showSpinner = false;
		}

		handleCheckbox(event){
				this.checkboxValue = event.detail.value;
				console.log('check is ->'+this.checkboxValue);
		}

		navigateToRecordPage() {
		   this[NavigationMixin.Navigate]({
			  type: 'standard__recordPage',
			  attributes: {
				  recordId: this.insertedRecordId,
				  objectApiName: 'Account',
				  actionName: 'view'
			  }
		  });
	  }
}