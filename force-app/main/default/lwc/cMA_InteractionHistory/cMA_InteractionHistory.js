import { LightningElement, api, track } from 'lwc';
//import getAllVerification from '@salesforce/apex/CMA_IntractionHistoryController.getAllVerificationRecord';
import getAllVerificationSearch from '@salesforce/apex/CMA_IntractionHistoryController.getAllVerificationRecordOnSearch';

export default class CMA_InteractionHistory extends LightningElement {
		@api recordId;
		@track records;
		@track dataNotFound;
		@track error;
		comType;
	  startDate;
		endDate;
		buttonDisabled = true;
		defaultMessage = true;
		
		/*connectedCallback() {
				console.log('In connected call back function....');
				getAllVerification({recordId: this.recordId})
						.then((result) => {
						this.records = result;
				})
						.catch((error) => {
						console.log('In connected call back error....');
						this.error = error;
						this.records = undefined;
						console.log('Error is', this.error); 
				});
		}*/
		
		handleSearch(){
				console.log('searching' + this.comType);
				if(this.comType !== null && this.comType !== undefined && this.startDate !== null && this.endDate !== null){
						getAllVerificationSearch({recordId: this.recordId, keyword:this.comType, startDate:this.startDate, endDate:this.endDate})
								.then((result) => {
								if(result.length>0){
										this.records = result;
										this.defaultMessage =false;
								}
								else{
										this.records = null;
										this.defaultMessage =false;
								}
								
						})
								.catch((error) => {
								console.log('In connected call back error....');
								this.error = error;
								this.records = undefined;
								console.log('Error is', this.error); 
						});
				}else{
						alert('Please Select Date and Interaction Type');
				}
		}
		
		getInteractionType(event){
				this.comType = event.target.value;				
		}
		
		getStartDate(event){
				this.startDate = event.target.value;
				if(this.startDate === null){
						this.records = null;
						this.defaultMessae = true;
				}else{
						this.defaultMessae = false;
				}
		}
		getEndDate(event){
				this.endDate = event.target.value;
				if(this.endDate === null){
						this.records = null;
						this.buttonDisabled = true;
						this.defaultMessae = true;
				}else{
						this.buttonDisabled = false;
						this.defaultMessae = false;
				}
				
				
		}
}