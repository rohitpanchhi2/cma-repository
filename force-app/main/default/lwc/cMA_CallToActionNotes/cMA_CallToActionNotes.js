import { LightningElement, api } from 'lwc';
export default class CMA_CallToActionNotes extends LightningElement {
		@api notes;
		@api itemnotes;
		opennotes = false;
		@api callnotespara;
		openNotes1(event){
				this.callnotespara = event.target.value;
				console.log('paragraph is ', this.callnotespara);
				this.opennotes = this.opennotes == true ? false :true;
		}
		closeBox() {
				this.opennotes = false;
		}
}