import { LightningElement, api, wire, track } from 'lwc';
import getMeetingDetails from '@salesforce/apex/CMA_ICTMeetingSetupController.getMeetingDetails';
import getCaseTeamMembers from '@salesforce/apex/CMA_ICTMeetingSetupController.getCaseTeamMembers';
import getRecordName from '@salesforce/apex/CMA_ICTMeetingSetupController.getRecordName';
import insertCampaignMembersCP from '@salesforce/apex/CMA_ICTMeetingSetupController.insertCampaignMembersCP';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const columnData = [
		{ label: 'Name', fieldName: 'Name', hideDefaultActions: true },
		{ label: 'Role', fieldName: 'CMA_Role__c', hideDefaultActions: true },
		{ label: 'Invite Date', fieldName: 'CreatedDate', type: 'date', hideDefaultActions: true },
		{ label: 'Type', fieldName: 'CMA_Type__c', hideDefaultActions: true },
		{ label: 'Status', fieldName: 'CMA_Status__c', hideDefaultActions: true },
		{ label: 'Action', type: 'button-icon', typeAttributes: { iconName:'utility:delete', title: 'Remove'} },
];

export default class CmaICTMeetingSetup extends LightningElement { 

		@api recordId;
		columns = columnData;
		accountObj= {};
		caseObj= {};
		existingMeetings;
		meetingDateTime;

		optionsRole =[];
		@track optionsCampaigns = [];
		defaultUserContact = 'User';
		contactRecordTypeId = '0125f000000KUhrAAG';
		campaignData = [];
		campaignMemberList =[];
		@track tabledata = [];

		showUser = true;
		showMeetingInfo = false;
		openModal = false;
		showSpinner = true;
		showModalSpinner = false;

		@wire(getMeetingDetails, { recordId: '$recordId' })
		wiredObject({data, error}){
				if(data){
						let objStr = JSON.parse(data);
						console.log('Data',objStr);

						let caseData= JSON.parse(objStr.CaseObj);
						console.log('Case Details',caseData);
						this.accountObj.Name = caseData.Account? caseData.Account.Name : 'No Associated Account';
						this.accountObj.Id = caseData.Account.Id;
						this.caseObj.Name = caseData.Subject +' - '+ caseData.CaseNumber;
						this.caseObj.Id = caseData.Id;

						let caseTeamRoles= JSON.parse(objStr.CaseTeamRoles);
						console.log('Case Team Roles',caseTeamRoles);
						this.optionsRole = caseTeamRoles;

						this.campaignData= JSON.parse(objStr.CampaignList);
						console.log('Campaign List',this.campaignData);
						let campaignDetails =[];
						this.campaignData.forEach(element =>{
								let newObj = { label: element.Name , value: element.Id};
								campaignDetails.push(newObj);
								if(element.Care_Plan_Campaign_Members__r){
										let tempList = element.Care_Plan_Campaign_Members__r.records;
										tempList.forEach( item =>{
												if(item.CMA_Care_Team_Member__c){
														item.Name = item.CMA_Care_Team_Member__r.Name;
														item.tableId = item.CMA_Care_Team_Member__c;
												} else if(item.CMA_Contact__c) {
														item.Name = item.CMA_Contact__r.Name;
														item.tableId = item.CMA_Contact__c;
												}
												this.campaignMemberList.push(item);
										});
								}
						});
						this.optionsCampaigns = campaignDetails;
						console.log('campaignMemberList',this.campaignMemberList);
						this.showSpinner = false;
				}
				if(error){
						console.log(error);
						this.showSpinner = false;
				}
		}

		connectedCallback(){
				const dateVar = new Date ();
				this.currentDateTime = dateVar.toISOString();
				console.log('Record Id',this.recordId);
				//	this.showSpinner = false;
		}

		get showDataTable(){
				return this.tabledata.length > 0 ? true: false;
		}

		handleCreateMeeting(){
				console.log('Create Meeting');
				this.showSpinner = true;
				if(this.meetingDateTime){
						let datevalue = new Date(this.meetingDateTime);
						let dateFinal = datevalue.getMonth()+1 +'/'+ datevalue.getDate();
						let fields = { 'Name': dateFinal +' - '+ this.caseObj.Name,
													'CMA_Meeting_Date_Time__c' : this.meetingDateTime,
													'Type' : 'ICT Meeting',
													'IsActive': true, 
													'CMA_Care_Plan__c': this.caseObj.Id, 
													'CMA_Account__c': this.accountObj.Id };
						let objRecordInput = {'apiName' : 'Campaign', fields};
						console.log('Meeting Record',objRecordInput);
						createRecord(objRecordInput).then(response => {
								//    alert('Campaign created with Id: ' +response.id);
								const eventNewRec = new ShowToastEvent({
										title: 'Success!',
										message: 'New Meeting Record '+response.id+' was created!',
										variant: 'success'
								});
								this.dispatchEvent(eventNewRec);

								let objOption = { label: fields.Name , value: response.id};								
								let tempList = JSON.parse(JSON.stringify(this.optionsCampaigns));
								tempList.push(objOption);
								this.optionsCampaigns = tempList;
								this.tabledata = [];
								this.campaignId = response.id;
								this.meetingDateTime = undefined;
								this.showMeetingInfo = true;
								this.showSpinner = false;
						}).catch(error => {
								this.showSpinner = false;
								console.log('Error',error);
								const eventNewRec = new ShowToastEvent({
										title: 'Error!',
										message: JSON.stringify(error),
										variant: 'error'
								});
								this.dispatchEvent(eventNewRec);
						});
				} else{
						this.showSpinner = false;
						const eventNewRec = new ShowToastEvent({
								title: 'Missing Fields',
								message: 'Enter Meeting Date and Time!',
								variant: 'warning'
						});
						this.dispatchEvent(eventNewRec);
				}
		}

		handleMeetingTime(event){
				this.meetingDateTime = event.detail.value;
		}

		campaignId
		handleChangeMeeting(event){
				this.showSpinner = true;
				this.campaignId = event.target.value;
				console.log('campaignId',this.campaignId);
				const result = this.campaignMemberList.filter(element => element.CMA_Campaign__c === this.campaignId);
				this.tabledata = result;
		//		const foundElement = this.campaignData.find(element => element.Id === this.campaignId);
		//		this.meetingDateTime = foundElement.CMA_Meeting_Date_Time__c;
				this.showMeetingInfo = true;
				this.showSpinner = false;
		}

		userContactIdInvitee;
		handleUserContactId(event){
				console.log('Detail',event.detail.options);
				this.userContactIdInvitee = event.target.value;
		}

		roleInvitee;
		handleRoleChange(event){
				this.roleInvitee = event.target.value;
		}

		typeInvitee;
		handleTypeChange(event){
				this.typeInvitee = event.target.value;
		}

		handleAddInvitee(){
				this.showSpinner = true;
				if(!(this.userContactIdInvitee === undefined || this.userContactIdInvitee === null)){
						const foundElement = this.tabledata.find(element => element.tableId === this.userContactIdInvitee);
						if(!foundElement){
								getRecordName({ recordId: this.userContactIdInvitee })
										.then(data=>{
										console.log('Object',data);
										let tempData ={};
										tempData.Name = data.Name;
										tempData.CMA_Role__c = this.roleInvitee;
										tempData.CMA_Type__c= this.typeInvitee;
										tempData.CMA_Status__c = 'New';
										tempData.CreatedDate = new Date ();
										tempData.tableId = this.userContactIdInvitee;
										tempData.toInsert = true;
										if(this.defaultUserContact === 'User'){
												tempData.CMA_Care_Team_Member__c = this.userContactIdInvitee;
										} else{
												tempData.CMA_Contact__c = this.userContactIdInvitee;
										}
										console.log('New Obj',tempData);

										let tempList = JSON.parse(JSON.stringify(this.tabledata));
										tempList.push(tempData);
										this.tabledata = tempList;

										this.userContactIdInvitee = undefined;
										this.roleInvitee= undefined;
										this.typeInvitee= undefined;
										this.showSpinner = false;
								})
										.catch(error =>{
										this.showSpinner = false;
										console.log('Error',error);
								});
						} else{
								this.showSpinner = false;
								const eventNew = new ShowToastEvent({
										title: 'Error',
										message: 'The User/Contact is already in Invitee List',
										variant: 'error'
								});
								this.dispatchEvent(eventNew);
						}
				} else{
						console.log('userContactIdInvitee not found');
						this.showSpinner = false;
				}
		}

		handleAddExternal(){
				this.openModal = true;
				this.roleInvitee= undefined;
				this.typeInvitee= undefined;
				this.showSpinner = true;
				this.showModalSpinner = true;
		}

		handleAddCareTeam(){
				this.showSpinner = true;
				let careTeamMember = [];
				getCaseTeamMembers({ caseId: this.caseObj.Id })
						.then(data=>{
						console.log('Care Team',data);
						data.forEach(element=>{
								const foundElement = this.tabledata.find(item => item.tableId === element.MemberId);
								if(!foundElement){
										let tempData ={};
										tempData.Name = element.Member.Name;
										tempData.CMA_Role__c = element.TeamRole.Name;
										tempData.CMA_Type__c= 'Email';
										tempData.CMA_Status__c = 'New';
										tempData.CreatedDate = new Date ();
										tempData.tableId = element.MemberId;
										tempData.toInsert = true;
										if(element.MemberId.startsWith('003')){
												tempData.CMA_Contact__c = element.MemberId;
										} else if(element.MemberId.startsWith('005')){
												tempData.CMA_Care_Team_Member__c = element.MemberId;
										}
										careTeamMember.push(tempData);
								} else{
										console.log('Duplicate Care Team Member');
								}
						});
						console.log('careTeamMember',careTeamMember);
						let tempList = JSON.parse(JSON.stringify(this.tabledata));
						this.tabledata = tempList.concat(careTeamMember);
						this.showSpinner = false;
				}).catch(error =>{
						this.showSpinner = false;
						console.log('Error',error);
				});
		}

		handleLoad(event){
				console.log('Load');
				this.showModalSpinner = false;
		}

		handleSuccess(event) {
				console.log('onsuccess event',event.detail.id);
				console.log('Event',JSON.stringify(event.detail));
				let newContactId = event.detail.id;

				const eventNewRec = new ShowToastEvent({
						title: 'Success!',
						message: 'Contact Record '+newContactId+' was created!',
						variant: 'success'
				});
				this.dispatchEvent(eventNewRec);

				getRecordName({ recordId: newContactId })
						.then(data=>{
						let tempData ={};
						tempData.Name = data.Name;
						tempData.CMA_Role__c = this.roleInvitee;
						tempData.CMA_Type__c= this.typeInvitee;
						tempData.CMA_Status__c = 'New';
						tempData.CreatedDate = new Date ();
						tempData.CMA_Contact__c = newContactId;
						tempData.tableId = newContactId;
						tempData.toInsert = true;
						console.log('New Obj',tempData);

						let tempList = JSON.parse(JSON.stringify(this.tabledata));
						tempList.push(tempData);
						this.tabledata = tempList;

						this.showModalSpinner = false;
						this.roleInvitee= undefined;
						this.typeInvitee= undefined;
						this.showSpinner = false;
						this.openModal = false;

				}).catch(error =>{
						this.showModalSpinner = false;
						this.showSpinner = false;

						const eventNewRec = new ShowToastEvent({
								title: 'Error!',
								message: JSON.stringify(error),
								variant: 'error'
						});
						this.dispatchEvent(eventNewRec);

						console.log('Error',error);
				});

		}

		handleSubmit(event) {
				this.showModalSpinner = true;
				console.log('onsubmit event',event.detail.fields);
		}

		handleError(event){
				this.showSpinner = false;
				this.showModalSpinner = false;

				const eventNewRec = new ShowToastEvent({
						title: 'Error!',
						message: JSON.stringify(event.detail),
						variant: 'error'
				});
				this.dispatchEvent(eventNewRec);

				console.log('Error', JSON.stringify(event.detail));
		}

		closeModal(){
				this.showModalSpinner = false;
				this.showSpinner = false;
				this.openModal = false;
		}

		get optionsType() {
				return [
						{ label: 'Email', value: 'Email' },
						{ label: 'Letter', value: 'Letter' }
				];
		}

		get optionsUserContact(){
				return [
						{ label: 'User', value: 'User' },
						{ label: 'Contact', value: 'Contact' }
				];
		}

		handleUserContact(event){
				this.defaultUserContact = event.target.value;
				this.userContactIdInvitee = undefined ;
				if(this.defaultUserContact === 'User'){
						this.showUser = true;
				} else{
						this.showUser = false;
				}
		}

		handleAddCampaignMembers(){
				this.showSpinner = true;
				console.log('Add New Members');
				const result = this.tabledata.filter(element => element.toInsert === true);

				if(result.length >0){
						let newCampaignMembers = result.map( item =>{
								let newCPCMObj ={
										CMA_Campaign__c: this.campaignId,
										CMA_Role__c: item.CMA_Role__c, 
										CMA_Status__c: item.CMA_Status__c,
										CMA_Type__c: item.CMA_Type__c,
										Name: item.Name,
								};
								if(item.CMA_Care_Team_Member__c){
										newCPCMObj.CMA_Care_Team_Member__c = item.CMA_Care_Team_Member__c;		
								}
								if(item.CMA_Contact__c){
										newCPCMObj.CMA_Contact__c = item.CMA_Contact__c;
								}																
								return newCPCMObj;
						});

						console.log('New List to Insert',newCampaignMembers);

						insertCampaignMembersCP({ campaignMemCPList: newCampaignMembers })
								.then(data=>{
								console.log('data',data);
								if(data){
										const eventNewRec = new ShowToastEvent({
												title: 'Success!',
												message: 'New Invitees were added Successfully!',
												variant: 'success'
										});
										this.dispatchEvent(eventNewRec);
										newCampaignMembers.forEach(item =>{
												item.CreatedDate = new Date ();
												item.toInsert = false;
										})
										this.campaignMemberList = this.campaignMemberList.concat(newCampaignMembers);
										const resultList = this.campaignMemberList.filter(element => element.CMA_Campaign__c === this.campaignId);
										this.tabledata = resultList;
										this.showSpinner = false;
								}
						}).catch(error =>{
								this.showSpinner = false;
								console.log('Error',error);
						});
				} else {
						console.log('No Members Added');
				}
		}

		handleRemoveAction(event){
				let rowData = event.detail.row;
				//		console.log('rowData',JSON.stringify(rowData));
				if(rowData.CMA_Status__c !== 'New'){
						const eventNew = new ShowToastEvent({
								title: 'Error',
								message: 'Only Invitees in New status can be removed',
								variant: 'error'
						});
						this.dispatchEvent(eventNew);
				} else{
						const resultArray = this.tabledata.filter(element => element.tableId !== rowData.tableId);
						this.tabledata = resultArray;
				}
		}
}