/*
Description : Class for Letter Generation Screen           
*Author : KPMG
*Code Created Date : 26th October 2021
*/
trigger CMA_AccountAccountRelationTrigger on HealthCloudGA__AccountAccountRelation__c (After Insert) {
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            CMA_AccountRelationTriggerHandler.updateAccRelation(trigger.new);
        }
    }
}