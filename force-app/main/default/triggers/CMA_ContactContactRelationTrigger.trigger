/*
Description : Class for Letter Generation Screen           
*Author : KPMG
*Code Created Date : 26th October 2021
*/

trigger CMA_ContactContactRelationTrigger on HealthCloudGA__ContactContactRelation__c (After Insert) {
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            CMA_AccountRelationTriggerHandler.updateConRelation(trigger.new);
        }
    }
}