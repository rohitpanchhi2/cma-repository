trigger CMA_CommunicationRequestTrigger  on Communication_Request__c (After Insert) {
    
    if(trigger.isAfter){
        if(trigger.isInsert){
            CMA_CarePlanLettersController.sendEmailAlert(trigger.new);
        }
    }

}