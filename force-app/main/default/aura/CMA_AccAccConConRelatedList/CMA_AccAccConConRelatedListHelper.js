({
    
    getAccontRecord : function( component, event, intLimit ) {
        var action = component.get("c.getAccountRecord"); 
        action.setParams({
            "RecId" : component.get("v.recordId"),
            "intLimit" : intLimit      
        });  
        action.setCallback(this, function(response) {
            var state = response.getState(); 
            var result = JSON.stringify(response.getReturnValue());
            if (component.isValid() && state === "SUCCESS")
                component.set("v.accLst", response.getReturnValue());  
        });
        $A.enqueueAction(action);
    }
    
})