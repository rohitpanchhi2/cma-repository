({
    doInit : function(component, event, helper) {
        helper.getAccontRecord(component, event, 4); 
    },
    createRecord : function (component, event, helper) { 
        var createRecordEvent = $A.get("e.force:createRecord"); 
        createRecordEvent.setParams({
            "entityApiName": "HealthCloudGA__AccountAccountRelation__c" ,
                  "defaultFieldValues": {
                      'HealthCloudGA__Active__c' : true,
                      'HealthCloudGA__Account__c' : component.get("v.recordId")
                      }
        }); 
        createRecordEvent.fire(); 
    },
    handleClick : function (component, event, helper) {
        var rectarget = event.currentTarget;
        var idstr = rectarget.getAttribute("data-accId"); 
    var navEvt = $A.get("e.force:navigateToSObject");
    navEvt.setParams({
      "recordId": idstr,
      "slideDevName": "related"
    });
    navEvt.fire();
},
    
     loadAll : function(component, event, helper) {    
        component.set("v.viewAllBool", false);
        helper.getAccontRecord(component, event, 100);   
    },
    
    loadless : function(component, event, helper) {    
        component.set("v.viewAllBool", true);
        helper.getAccontRecord(component, event, 4);   
    }

})