({
    doInit : function(component, event, helper) {
        var recID = component.get("v.CaseId");
        var action = component.get("c.getPGIDetails");
        action.setParams({
            caseId: recID
        });
        action.setCallback(this, function(response){
            var data = response.getReturnValue();
            component.set("v.TaskList", data);   
        });
        $A.enqueueAction(action);
    },
    
    doInit1 : function(component, event, helper) {
        var recID = component.get("v.AccountId");
        var action = component.get("c.getAssessmentDetails");
        action.setParams({
            accountId: recID
        });
        action.setCallback(this, function(response){
            var data = response.getReturnValue();
            component.set("v.AssessmentList", data);
        });
        $A.enqueueAction(action);       
    },
    
    navigateToRecord : function(component , event, helper){
        var rectarget = event.currentTarget;
        var idstr = rectarget.getAttribute("data-accId"); 
        window.open('/lightning/r/vlocity_ins__Assessment__c' +'/' + idstr + '/view');  
    },
    
    onClickIncluded : function(component, event, helper) {
        var selectedItem = event.getSource().get("v.value");
        component.set("v.include", selectedItem); 
        var target = event.getSource().get("v.name");
        let selectedVal1 = component.get("v.selectedrow");
        selectedVal1.push({
            'target' : target,
            'included': selectedItem});
      //  alert(JSON.stringify(selectedVal1)); 
        component.set("v.IncludedValue", JSON.stringify(selectedVal1));        
    },
    
    onclickClosed : function(component, event, helper) {
        var selectedItem1 = event.getSource().get("v.value");
        component.set("v.close", selectedItem1); 
        var target = event.getSource().get("v.name");
        var checkboxes = component.find("checkboxid");
        var controllerValue = event.getSource().get("v.name");
        let selectedVal = component.get("v.selectedrow");
        let tsklist =   component.get("v.TaskList"); 
      //  alert(tsklist.length);
        console.log(checkboxes);
        

        if(tsklist.length > 1){
        for (var i=0; i<checkboxes.length; i++) {
            if(controllerValue == checkboxes[i].get("v.name") && selectedItem1 == true){
                checkboxes[i].set("v.value",true);
                checkboxes[i].set("v.disabled",true);
                selectedVal.push({
                    'target' : target,
                    'included':true,
                    'closed': selectedItem1});
             //  alert(JSON.stringify(selectedVal)); 
                
            }
            else if(controllerValue == checkboxes[i].get("v.name") && selectedItem1 == false){
                checkboxes[i].set("v.value",false);
                checkboxes[i].set("v.disabled",false);
                console.log('checkboxes - ' + checkboxes[i].get('v.name'));
            }
        }
    } 
        else
        {
            if(controllerValue == checkboxes.get("v.name") && selectedItem1 == true){
                checkboxes.set("v.value",true);
                checkboxes.set("v.disabled",true);
                selectedVal.push({
                    'target' : target,
                    'included':true,
                    'closed': selectedItem1});
                //  alert(JSON.stringify(selectedVal)); 
                
            }
            else if(controllerValue == checkboxes.get("v.name") && selectedItem1 == false){
                checkboxes.set("v.value",false);
                checkboxes.set("v.disabled",false);
                console.log('checkboxes - ' + checkboxes.get('v.name'));
            }
        }
component.set("v.IncludedValue", JSON.stringify(selectedVal));         
    }   
})