({
   doInit: function(component, event, helper) {
    //   var varlabel = $A.get('v.recordId')
   // window.open(varlabel, '_self');
    var id = component.get("v.recordId");
    var navEvt = $A.get("e.force:navigateToSObject");
    navEvt.setParams({
      "recordId": id
    });
    navEvt.fire();
   }
})