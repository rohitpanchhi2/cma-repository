({
    doInit : function(component, event, helper) {
        helper.getContactRecord(component, event, 4); 
    },
    
    createRecord : function (component, event, helper) { 
        var createRecordEvent = $A.get("e.force:createRecord"); 
        createRecordEvent.setParams({
            "entityApiName": "HealthCloudGA__ContactContactRelation__c" ,
                  "defaultFieldValues": {
                      'HealthCloudGA__Active__c' : true,
                      'Account__c' : component.get("v.recordId")
                      }
        }); 
        createRecordEvent.fire(); 
    },
    
    handleClick : function (component, event, helper) {
        var rectarget = event.currentTarget;
        var idstr = rectarget.getAttribute("data-conId"); 
    var navEvt = $A.get("e.force:navigateToSObject");
    navEvt.setParams({
      "recordId": idstr,
      "slideDevName": "related"
    });
    navEvt.fire();
},
    
    loadAll : function(component, event, helper) {    
        component.set("v.viewAllBool", false);
        helper.getContactRecord(component, event, 100);   
    },
    
    loadless : function(component, event, helper) {    
        component.set("v.viewAllBool", true);
        helper.getContactRecord(component, event, 4);   
    }
})