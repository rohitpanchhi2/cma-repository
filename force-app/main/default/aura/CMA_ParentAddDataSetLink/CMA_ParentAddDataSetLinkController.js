({
    fetchDecisionTblParaFields :function(component,event,helper) {
        var recId = component.get("v.recordId");
        console.log('recordId : '+recId);
        var action = component.get('c.getFieldsOfDecisionTblPara');
        action.setParams({
            'recordId' : recId
        });
        action.setCallback(this,function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var response = a.getReturnValue();
                component.set("v.fieldName", response);
                console.log('obj are : '+response);
            }
            else if (state === "ERROR"){
                var errors = a.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log('Error : '+errors[0].message);
                    }
                }
                else{
                    console.log('unknown error');
                }
            }
        })
        $A.enqueueAction(action);
    },
    
    fetchAllObject : function(component,event,helper) {
        var action = component.get('c.getAllObjects');
        action.setCallback(this,function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var response = a.getReturnValue();
                component.set("v.sourceObject", response);
                console.log('obj are : '+response);
            }
            else if (state === "ERROR"){
                alert('Something went wrong!!!');
            }
        })
        $A.enqueueAction(action);
    },
    
    handleComponentEvent : function(component, event, helper) {
        var valueFromChild = event.getParam("selectedKeyList");
        console.log('from child : '+JSON.stringify(valueFromChild));
        let selectedValuesFromChild = component.get("v.selectedKeyList");
        //  selectedValuesFromChild.push(JSON.stringify(valueFromChild));
        //alert(JSON.stringify(selectedValuesFromChild));
        //  var test = component.set('v.selectedKeyList',selectedValuesFromChild);
        //   console.log('test '+test);
        
        if(selectedValuesFromChild.length != 0){
            for(var i=0;i<selectedValuesFromChild.length;i++){
                if(selectedValuesFromChild[i]["fieldname"] === valueFromChild[0]["fieldname"]){
                    selectedValuesFromChild[i]["fields"] = valueFromChild[0]["fields"];
                    component.set('v.selectedKeyList',selectedValuesFromChild);
                    //alert('@@@  Parent :: '+JSON.stringify(component.get("v.selectedKeyList")));
                    return;
                }
                /*
                     else{
                         selectedValuesFromChild.push(message[0]);
                     }*/
            }
            selectedValuesFromChild.push(valueFromChild[0]);
        }else{
            selectedValuesFromChild.push(valueFromChild[0]);
        }
        component.set('v.selectedKeyList',selectedValuesFromChild);
        //alert('@@@  Parent :: '+JSON.stringify(component.get("v.selectedKeyList")));
        
        component.set('v.selectedKey',JSON.stringify(selectedValuesFromChild));
        var finalData = component.get('v.selectedKey');
        //alert(finalData);
    }
})