({
    doInit : function(component, event, helper) {
        var key = component.get("v.key");
        var map = component.get("v.values");
    },
    fetchFields :function(component, event, helper) {
        var selectedValue = event.getSource().get("v.value");
        component.set("v.key", selectedValue);
        component.set("v.selectedvalue1", selectedValue);
        var action = component.get("c.getObjectFields");
        action.setParams({ selectedObj : selectedValue });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var arrayOfMapKeys = component.get("v.values");     
                var StoreResponse = response.getReturnValue();   
                for (var singlekey in StoreResponse) {
                    console.log(singlekey)
                    component.set("v.fields",  StoreResponse[singlekey]);
                    arrayOfMapKeys.push({
                        'key' : singlekey,
                        'values': StoreResponse[singlekey]
                    });
                    
                }
                component.set("v.values", arrayOfMapKeys);
                console.log(component.get("v.values"));
            }
            else if(state === 'ERROR'){
                alert('ERROR OCCURED.');
            }
        })        
        $A.enqueueAction(action);
        if(selectedValue == 'OUTPUT'){
            var selectedItem1 = event.getSource().get("v.value");
            var target = event.target.getAttribute("title");
            let selectedVal1 = component.get("v.selectedrow");
            let index = component.get("v.indexVal");
            selectedVal1.push({
                'useAs': component.get("v.selectedvalue1"),
                'fieldName':target
                
            });
            
            component.set("v.indexVal",index++);
           //    alert(JSON.stringify(selectedVal1));
            component.set("v.selectedvalue2", JSON.stringify(selectedVal1)); 
            var compEvent = component.getEvent("compEvent");
            compEvent.setParams({
                "message" : selectedVal1
            });
            compEvent.fire(); 
        }    
    },      
    onChangeSelect2 : function(component, event, helper) {
        var selectedItem1 = event.getSource().get("v.value");
       // alert(selectedItem1);
        component.set("v.selectedvalue3", selectedItem1);
        var target = event.target.getAttribute("title");
        let selectedVal = component.get("v.selectedrow");
        let index = component.get("v.indexVal");
        if(selectedVal.length !=  0 ){
            for(var i =0; i<selectedVal.length ;i++){
                if(selectedVal[i]["fieldName"] === target){
                    selectedVal[i]["operator"] = selectedItem1;
                }
            }
        }else{
            selectedVal.push({
                'useAs': component.get("v.selectedvalue1"),
                'fieldName':target,
                'operator': selectedItem1});
            
        }
      //  alert('@@@ Child :: '+JSON.stringify(selectedVal));
        /*   selectedVal.push({
                        'useAs': component.get("v.selectedvalue1"),
            			'fieldName':target,
                        'operator': selectedItem1});
        */
           component.set("v.indexVal",index++); 
         
        
      /*  component.set('v.counter',component.get('v.counter')+1);
         var ss = component.get("v.counter");
        alert(ss); */
        
        
           component.set("v.selectedvalue3", JSON.stringify(selectedVal));
           var compEvent = component.getEvent("compEvent");
           compEvent.setParams({
               "message" : selectedVal
           });
           compEvent.fire(); 
       }
})