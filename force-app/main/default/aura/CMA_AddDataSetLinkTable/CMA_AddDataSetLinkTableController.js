({  
    fetchFields :function(component, event, helper) {
        var selectedValue = event.getSource().get("v.value");
        component.set("v.key", selectedValue);
       
        var currentField = component.get('v.fieldName');
        console.log('currentField is : '+currentField);
        var action = component.get("c.getObjectFields");
        action.setParams({ selectedObj : selectedValue });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var arrayOfMapKeys = component.get("v.values");     
                var StoreResponse = response.getReturnValue();   
                for (var singlekey in StoreResponse) {
                    console.log(singlekey)
                    component.set("v.fields",  StoreResponse[singlekey]);
                    arrayOfMapKeys.push({
                        'key' : singlekey,
                        'values': StoreResponse[singlekey]
                    });
                    
                }
                
                component.set("v.values", arrayOfMapKeys);
                console.log(component.get("v.values"));
            }
            else if(state === 'ERROR'){
                alert('ERROR OCCURED.');
            }
        })        
        $A.enqueueAction(action);
        
    },
    
    fetchSelectedFields : function(component, event, helper) {
        var selectedObject = component.get('v.key');
        //Case
        var currentField = component.get('v.fieldName');
        //CMA_CALLAttemptNumber
        var fieldName = component.find('fieldName1');
        //Reason
        console.log('field are : '+fieldName);
        var fields = fieldName.get('v.value');
        component.set("v.fields",fields);
        console.log('fields are : '+fields);
        var finalString = [];
        if(finalString.length !=  0 ){
            for(var i =0; i<finalString.length ;i++){
                if(finalString[i]["fieldname"] === currentField){
                    finalString[i]["fields"] = fields;
                }
            }
        }else{
            finalString.push({
            'fieldname':currentField,
            'obj':selectedObject,
            'fields':fields
            });
            
        }
       // alert('@@@ Child :: '+JSON.stringify(finalString));
        
        
        component.set("v.selectedKeyList", finalString);
        var finalData = component.get("v.selectedKeyList");
        console.log(JSON.stringify(finalData));
        var CMA_AddDataSetLinkEvent = component.getEvent("CMA_AddDataSetLinkEvent");
        CMA_AddDataSetLinkEvent.setParams({"selectedKeyList" : finalData });
        CMA_AddDataSetLinkEvent.fire();
    }
})