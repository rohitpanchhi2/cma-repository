({
    doInit : function(component) {        
        var pickvar = component.get("c.getAllPrimarySObjects");
        pickvar.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var list = response.getReturnValue();
                component.set("v.picvalue", list);
            }
            else if(state === 'ERROR'){
                //var list = response.getReturnValue();
                //component.set("v.picvalue", list);
                alert('ERROR OCCURED.');
            }
        })
        $A.enqueueAction(pickvar);
    },
    onChangeSelect : function(component, event, helper) {
    var selectedItem = event.getSource().get("v.value");
       // alert('Selectd Genre-' + selectedItem);
       component.set("v.selectedPrimaryObj", selectedItem);

    }
})