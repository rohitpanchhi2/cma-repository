({   
    onRecordIdChange : function (component, event, helper) {   
        var Tostart = component.get("v.start");
        if(Tostart == false){
            var flowfinal = component.find("flowData");
            
            flowfinal.destroy();
            
            $A.createComponent(
                "lightning:flow",{
                    "aura:id" : "flowData",
                    "onstatuschange": component.getReference("c.handleStatusChange")
                },
                function(buttonComponent, status, errorMessage){
                    if (status === "SUCCESS") {
                        var outerDiv = component.find('outerDiv').get('v.body');
                        outerDiv.push(buttonComponent);
                        component.find('outerDiv').set('v.body', outerDiv);
                    }
                }               
            ); 
            
            var flowfinal2 = component.find("flowData");
            var flownamevar = component.get( "v.Flowname" );
            var inputVariables = [
                { name : "recordId", type : "String", value: component.get("v.recordId") }
            ];       
            if(component.get("v.recordId") != null){
            flowfinal2.startFlow(flownamevar, inputVariables);   
            }
        }
    },
    
    handleStatusChange : function (component, event) { 
        if(event.getParam("status") === "STARTED") {
            var outputVariables = event.getParam("outputVariables");
            var outputVar;
            console.log(outputVariables);
            if(outputVariables != null){
            for(var i = 0; i < outputVariables.length; i++) {
                outputVar = outputVariables[i];
                if(outputVar.name === "OutputCount") {
                    var outVar = outputVar.value;               
                }
                if(outVar != null){
                    component.set("v.start",true);
                }
            }
        }
        }
        if(event.getParam("status") === "FINISHED") {
            component.set("v.start",false);
            var a = component.get('c.onRecordIdChange');
        $A.enqueueAction(a);
        }
    },
})